<%@ page contentType="text/html; charset=UTF-8" language="java" pageEncoding="utf-8" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page session="true" %>
<fmt:setLocale value="${locale}" scope="session"/>
<fmt:setBundle basename="content" var="rb"/>

<html>
<head>
    <title>Bootstrap Case</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" type="text/css" href="../css/style.css">
    <link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.0/jquery.min.js"></script>
    <script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
</head>
<body>
<c:import url="./${sessionScope.role.value}/header.jsp"/>

<div class="container">

    <div class="page-header">
        <h1><fmt:message key="label.cart" bundle="${rb}"/> </h1>
    </div>

    <table class='table table-hover table-responsive table-bordered' style='margin:1em 0 0 0;'>
        <tr>
            <th class='textAlignLeft'><fmt:message key="label.name.preparation" bundle="${rb}"/> </th>
            <th><fmt:message key="label.price" bundle="${rb}"/> </th>
            <th style='width:15em;'><fmt:message key="label.quantity" bundle="${rb}"/> </th>
            <th><fmt:message key="lable.sub.total" bundle="${rb}"/> </th>
            <th><fmt:message key="label.action" bundle="${rb}"/> </th>
        </tr>
        <c:forEach items="${requestScope.preparations}" var="pr">
            <tr>
                <td>
                    <div class='product-name'><c:out value="${pr.name}"/></div>
                </td>
                <td>
                    <div class='product-price'><c:out value="${pr.price}"/></div>
                </td>
                <td>
                    <form class='update-quantity-form' action="<c:url value="/auntification"/>" method="post">
                        <input type="hidden" name="idProduct" value="${pr.lotNumber}">
                        <input type="hidden" name="command" value="update_product_command">
                        <div class='input-group'><input type='number' name='quantity' value=${pr.count} min='1'
                                                        class='form-control'
                                                        required><span class='input-group-btn'><button type='submit'
                                                                                                       class='btn btn-default update-quantity'>
                            <fmt:message key="button.update" bundle="${rb}"/>
                        </button></span></div>
                    </form>
                </td>
                <td><ctg:sub-total price="${pr.price}" count="${pr.count}"/></td>
                <td><c:if test="${pr.requireRecipe && role.value eq 'client' && pr.recipe eq 0 }">
                    <form action="<c:url value="/auntification"/>" method="post">
                        <input type="hidden" name="idProduct" value="${pr.lotNumber}">
                        <input type="hidden" name="idOrder" value="0">
                        <input type="hidden" name="command" value="attach_recipe">
                        <ctg:recipe-list namePreparation="${pr.name}"/>
                        <button type='submit'
                                class='btn btn-danger'>
                           <fmt:message key="button.attach.recipe" bundle="${rb}"/>
                        </button>
                    </form>
                    </c:if>
                </td>
                <td>
                    <form action="<c:url value="/auntification"/>" method="post">
                        <input type="hidden" name="idProduct" value="${pr.lotNumber}">
                        <input type="hidden" name="command" value="remove_product_cart">
                        <button type='submit'
                                class='btn btn-danger'>
                            <fmt:message key="button.remove" bundle="${rb}"/>
                        </button>
                    </form>
                </td>
            </tr>
        </c:forEach>

        <tr>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td><c:if test="${role.value eq 'client'}">
                <a href='/auntification?command=check_out' class='btn btn-success'><span
                        class='glyphicon glyphicon-shopping-cart'></span> <fmt:message key="button.checkout" bundle="${rb}"/> </a>
            </c:if>
            </td>
        </tr>
    </table>
    ${requestScope.errorMessage}
</div>
</body>
</html>