<%@ page contentType="text/html; charset=UTF-8" language="java" pageEncoding="utf-8" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<fmt:setLocale value="${locale}" scope="session"/>
<fmt:setBundle basename="content" var="rb"/>


<html>
<head>
    <title>Bootstrap Case</title>
    <meta charset="utf-8">
</head>
<body>

<nav class="navbar navbar-default">
    <div class="container-fluid">
        <!-- Brand and toggle get grouped for better mobile display -->
        <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse"
                    data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="../mainDoctor.jsp">Pharmacy</a>
        </div>

        <!-- Collect the nav links, forms, and other content for toggling -->
        <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
            <ul class="nav navbar-nav">
                <li class="active"><a
                        href="<c:url value="/auntification?command=open_page&page=new.recipe"/>"><fmt:message
                        key="label.add_recipe" bundle="${ rb }"/> <span
                        class="sr-only">(current)</span></a></li>
                <li><a href="<c:url value="/auntification?command=show_request"/>"><fmt:message key="label.showRequest"
                                                                                                bundle="${ rb }"/></a>
                </li>
            </ul>
            <form class="navbar-form navbar-right" name="Localization" method="get" action="<c:url value="/auntification"/>">
                <input type="hidden" name="command" value="logout">
                <button type="submit" class="btn btn-default navbar-btn">
                    <fmt:message key="label.logout" bundle="${rb}"/>
                </button>
            </form>
        </div><!-- /.navbar-collapse -->
    </div><!-- /.container-fluid -->
</nav>

</body>
</html>
