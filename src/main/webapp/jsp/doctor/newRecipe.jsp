<%@ page contentType="text/html; charset=UTF-8" language="java" pageEncoding="utf-8" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<fmt:setLocale value="${locale}" scope="session"/>
<fmt:setBundle basename="content" var="rb"/>
<html>
<head>
    <title>Bootstrap Case</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" type="text/css" href="../css/style.css">
    <link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.0/jquery.min.js"></script>
    <script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
</head>
<body>
<c:import url="header.jsp"/>
<form method="post" action="<c:url value="/auntification"/>" style="
    margin-left: 40%;
">
    <input type="hidden" name="command" value="register_recipe">
    <div>
        <h4><fmt:message key="label.id.recipe" bundle="${rb}"/></h4>
        <label>
            <input name="idRecipe" type="number">
        </label>
        ${requestScope.invalid_id}
    </div>
    <div>
        <h4><fmt:message key="label.client.login" bundle="${rb}"/></h4>
        <label>
            <input name="clientLogin" type="text">
        </label>
        ${requestScope.invalid_client_login}
    </div>


    <div>
        <h4><fmt:message key="label.name.preparation" bundle="${rb}"/></h4>
        <label>
            <input name="namePreparation" type="text">
        </label>
        ${requestScope.invalid_name_preparation}
    </div>


    <div>
        <h4><fmt:message key="label.dosage" bundle="${rb}"/></h4>
        <label>
            <input name="dosage" type="text">
        </label>
        ${requestScope.invalid_dosage}
    </div>


    <div>
        <h4><fmt:message key="label.discount" bundle="${rb}"/></h4>
        <label>
            <input name="discount" type="number" max="100" min="0">
        </label>
        ${requestScope.invalid_discount}
    </div>

    <div>
        <h4><fmt:message key="label.amount" bundle="${rb}"/></h4>
        <label>
            <input name="amount">
        </label>
        ${requestScope.invalid_amount}
    </div>

    <div>
        <h4><fmt:message key="label.finish.date" bundle="${rb}"/></h4>
        <label>
            <input type="date" name="finishDate">
        </label>
        ${requestScope.invalid_finish_date}
    </div>

    <input type="submit" value=" <fmt:message key="button.register.recipe" bundle="${rb}"/>">
</form>
${errorMessage}
${successMessage}
</body>
</html>
