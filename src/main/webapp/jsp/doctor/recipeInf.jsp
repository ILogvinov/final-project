<%--
  Created by IntelliJ IDEA.
  User: Иван
  Date: 05.06.2016
  Time: 10:34
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html; charset=UTF-8" language="java" pageEncoding="utf-8" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<fmt:setLocale value="${locale}" scope="session"/>
<fmt:setBundle basename="content" var="rb"/>
<html>
<head>
    <title>Bootstrap Case</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" type="text/css" href="../css/style.css">
    <link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.0/jquery.min.js"></script>
    <script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
</head>
<body>

<c:import url="header.jsp"/>
<div style="margin-left: 40%;">
    <div>
        <h4><fmt:message key="label.client.login" bundle="${rb}"/></h4>
        <label>
            <c:out value="${recipe.clientLogin}"/>
        </label>
    </div>


    <div>
        <h4><fmt:message key="label.name.preparation" bundle="${rb}"/></h4>
        <label>
            <c:out value="${recipe.nameOfPreparation}"/>
        </label>
    </div>


    <div>
        <h4><fmt:message key="label.dosage" bundle="${rb}"/></h4>
        <label>
            <c:out value="${recipe.dosage}"/>
        </label>
    </div>


    <div>
        <h4><fmt:message key="label.amount" bundle="${rb}"/></h4>
        <label>
            <c:out value="${recipe.amount}"/>
        </label>
    </div>

    <div>
        <h4><fmt:message key="label.start.date" bundle="${rb}"/></h4>
        <label>
            <c:out value="${recipe.startDate}"/>
        </label>
    </div>

    <div>
        <h4><fmt:message key="label.finish.date" bundle="${rb}"/></h4>
        <label>
            <c:out value="${recipe.finishDate}"/>
        </label>
    </div>


    <form method="post" action="<c:url value="/auntification"/>">
        <input type="hidden" name="command" value="change_request_status">
        <input type="hidden" name="status" value="APPROVED">
        <input type="hidden" name="idRequest" value="${recipe.id}">
        <h4><fmt:message key="label.finish.date" bundle="${rb}"/></h4>
        <label>
            <input type="date" name="newFinishDate">
        </label>
        <input type="submit" value="<fmt:message key="button.approved" bundle="${rb}"/>"/>
    </form>

    <form method="post" action="<c:url value="/auntification"/>">
        <input type="hidden" name="command" value="change_request_status">
        <input type="hidden" name="status" value="REJECTED">
        <input type="submit" value="<fmt:message key="button.reject" bundle="${rb}"/>">
    </form>

</div>


</body>
</html>
