<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib prefix="x" uri="http://java.sun.com/jsp/jstl/xml" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<fmt:setLocale value="${locale}" scope="session"/>
<fmt:setBundle basename="content" var="rb"/>
<html>
<head>
    <title>Title</title>
    <title>Bootstrap Case</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css">
    <link rel="stylesheet"type="text/css" href="../css/style.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.0/jquery.min.js"></script>
    <script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
</head>
<body>

<h2>${pageContext.request.contextPath}</h2>
<c:import url="http://www.vitasite.ru/rss/articles" var="newsXML"/>
<x:parse var="doc" doc="${newsXML}"/>
<!--using tags x:forEach and x:out-->
<x:forEach select="$doc/rss/channel/item" var="news">
    <div class="news-item-style">
        <div class="news-image-style">
            <img src="<x:out select="$news/enclosure/@url"/>" alt="Mountain View">
        </div>
        <div class="news-article-style">
            <a href="<x:out select="$news/link"/>"><x:out select="$news/title"/></a>
            <x:out select="$news/description"/><br/>
        </div>
    </div>
</x:forEach>
</body>
</html>

