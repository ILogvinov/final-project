<%@ page contentType="text/html; charset=UTF-8" language="java" pageEncoding="utf-8" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<fmt:setLocale value="${locale}" scope="session"/>
<fmt:setBundle basename="content" var="rb"/>


<html>
<head>
    <title>Bootstrap Case</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" type="text/css" href="../css/style.css">
    <link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.0/jquery.min.js"></script>
    <script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
</head>
<body>
<c:import url="./${sessionScope.role.value}/header.jsp"/>


<div class="product-col">
    <c:forEach items="${requestScope.preparations}" var="pr">
        <div class="row1">

            <div class="thumbnail">
                <img src="/img/preparation/${pr.imageName}" alt="">
                <div class="caption">
                    <h5>${pr.id}</h5>
                    <h3><a href="<c:url value="/auntification?command=show_preparation_inf&id_preparation=${pr.id}"/>">${pr.name}</a>
                    </h3>
                    <h4>${pr.price}</h4>
                    <c:if test="${role.value=='guest'|| role.value=='client'}">
                        <form action="<c:url value="/auntification"/>" method="post">
                            <input type="hidden" name="command" value="add_to_cart">
                            <input type="hidden" name="id" value="${pr.lotNumber}">
                            <div class="input-group">
                            <span class="input-group-btn">
                                <button type="submit" class="btn btn-primary add-to-cart"><fmt:message key="button.add.cart" bundle="${rb}"/></button></span></div>
                        </form>
                    </c:if>
                </div>
            </div>

        </div>
    </c:forEach>
</div>
</body>
</html>