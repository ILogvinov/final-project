<%--
  Created by IntelliJ IDEA.
  User: Иван
  Date: 27.03.2016
  Time: 17:59
  To change this template use File | Settings | File Templates.
--%>

<%@ page contentType="text/html; charset=UTF-8" language="java" pageEncoding="utf-8" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<fmt:setBundle basename="content" var="rb"/>
<fmt:setBundle basename="test" var="trb"/>

<html>
<head>
    <title>Register</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.0/jquery.min.js"></script>
    <script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
    <fmt:setBundle basename="content" var="rb"/>

</head>


<body>


<form class="form-horizontal" action='<c:url value="/auntification"/>' method="POST"  >
    <input type="hidden" name="command" value="registrate"/>
    <fieldset style=" padding-left: 508px">
        <div id="legend" style="margin-left: -508px">
            <legend class=""><fmt:message key="label.registrate" bundle="${rb}"/> </legend>
        </div>
        <div class="control-group">
            <label class="control-label"  for="login"><fmt:message key="label.login" bundle="${ rb }"/></label>
            <div class="controls">
                <input type="text" id="login" name="login" placeholder="" class="input-xlarge"  pattern="[A-Za-zА-Яа-яЁё\_]+$">
            </div>
            ${requestScope.invalid_login}
        </div>

        <div class="control-group">
            <label class="control-label" for="password"><fmt:message key="label.password" bundle="${ rb }"/></label>
            <div class="controls">
                <input type="password" id="password" name="password" placeholder="" class="input-xlarge" pattern=".{7,20}">
            </div>
            ${requestScope.invalid_password}
        </div>

        <div class="control-group">
            <label class="control-label" for="repeat_password"><fmt:message key="label.password" bundle="${ rb }"/></label>
            <div class="controls">
                <input type="password" id="repeat_password" name="repeat_password" placeholder="" class="input-xlarge" pattern=".{7,20}">
            </div>
            ${requestScope.invalid_repeat_password}
        </div>

        <div class="control-group">
            <label class="control-label" for="name"><fmt:message key="label.name" bundle="${ trb }"/><br/></label>
            <div class="controls">
                <input type="text" id="name" name="name" placeholder="" class="input-xlarge"  pattern="^[A-Za-zА-Яа-яЁё]+$">
            </div>
            ${requestScope.invalid_name}
        </div>

        <div class="control-group">
            <label class="control-label"  for="surname"><fmt:message key="label.surname" bundle="${ trb }"/></label>
            <div class="controls">
                <input type="text" id="surname" name="surname" placeholder="" class="input-xlarge"  pattern="^[A-Za-zА-Яа-яЁё]+$">
            </div>
            ${requestScope.invalid_surname}
        </div>

        <div class="control-group">
            <label class="control-label"  for="phone"><fmt:message key="label.phone" bundle="${ trb }"/></label>
            <div class="controls">
                <input type="text" id="phone" name="phone_number" placeholder="" class="input-xlarge"  pattern="+?\d+$">
            </div>
            ${requestScope.invalid_phone}
        </div>

        <div class="control-group">
            <label class="control-label"  for="email"><fmt:message key="label.email" bundle="${ trb }"/></label>
            <div class="controls">
                <input type="text" id="email" name="email" placeholder="" class="input-xlarge"  pattern="[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,4}$">
            </div>
            ${requestScope.invalid_email}
        </div>
        <br>
        <div class="control-group">
            <!-- Button -->
            <div class="controls">
                <button class="btn btn-success"><fmt:message key="button.registry" bundle="${rb}"/> </button>
            </div>
        </div>
    </fieldset>
    ${requestScope.errorMessage}
</form>

</body>
</html>
