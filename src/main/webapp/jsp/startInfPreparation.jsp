<%--
  Created by IntelliJ IDEA.
  User: Иван
  Date: 25.04.2016
  Time: 1:11
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html; charset=UTF-8" language="java" pageEncoding="utf-8" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<fmt:setLocale value="${locale}" scope="session"/>
<jsp:useBean id="preparation" scope="request" type="by.bsuir.pharma.entity.Preparation"/>
<fmt:setBundle basename="content" var="rb"/>


<html>
<head>
    <title>Bootstrap Case</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" type="text/css" href="../css/style.css">
    <link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.0/jquery.min.js"></script>
    <script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
</head>
<body>
<c:import url="./${role.value}/header.jsp"/>
<div class="preparation-inf">
    <h2><fmt:message key="label.name.preparation" bundle="${rb}"/></h2>
    <p>
        <c:out value="${preparation.name}"/>
    </p>
    <h3><fmt:message key="label.preparation.group" bundle="${rb}"/></h3>
    <p>
        <c:out value="${preparation.group}"/>
    </p>
    <h3><fmt:message key="label.manufacture" bundle="${rb}"/></h3>
    <p>
        <c:out value="${preparation.manufacturer}"/>
    </p>
    <h3><fmt:message key="label.dosage" bundle="${rb}"/></h3>
    <p>
        <c:out value="${preparation.dosage}"/>
    </p>
    <h3><fmt:message key="label.count.units" bundle="${rb}"/></h3>
    <p>

        <c:out value="${preparation.countUnits}"/>
    </p>
    <h3><fmt:message key="label.testimony" bundle="${rb}"/></h3>
    <p>
        <c:out value="${preparation.testimony}"/>
    </p>
    <h3><fmt:message key="label.modeOfApplication" bundle="${rb}"/></h3>
    <p>
        <c:out value="${preparation.modeOfApplication}"/>
    </p>
</div>


</body>
</html>
