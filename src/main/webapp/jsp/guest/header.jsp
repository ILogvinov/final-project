<%@ page contentType="text/html; charset=UTF-8" language="java" pageEncoding="utf-8" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<fmt:setLocale value="${locale}" scope="session"/>
<fmt:setBundle basename="content" var="rb"/>
<fmt:setBundle basename="test" var="trb"/>
<html>
<head>
    <title>Bootstrap Case</title>
    <meta charset="utf-8">
</head>
<body>

<nav class="navbar navbar-default">
    <div class="container-fluid">
        <!-- Brand and toggle get grouped for better mobile display -->
        <div class="navbar-header">
            <a class="navbar-brand" href="#">Pharmacy</a>
        </div>

        <!-- Collect the nav links, forms, and other content for toggling -->
        <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
            <ul class="nav navbar-nav">
                <li class="active"><a href="/auntification?command=show_catalog"><fmt:message key="label.catalog" bundle="${ rb }"/> <span
                        class="sr-only">(current)</span></a></li>
                <li><a href="/auntification?command=open_page&page=news"><fmt:message key="label.news"
                                                                                      bundle="${ rb }"/></a></li>
                <li class='active'>
                    <a href="/auntification?command=show_cart">
                        <fmt:message key="label.cart"
                                     bundle="${ rb }"/> <span class="badge" id="comparison-count">${countProductCart}</span>
                    </a>
                </li>
            </ul>


            <!-- HTML-код модального окна-->
            <button type="submit" class="btn btn-success"><fmt:message key="buttonInput.label"
                                                                       bundle="${ rb }"/></button>
            <div class="modal fade <fmt:message key="buttonRussian.label" bundle="${ rb }"/>">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <!-- Заголовок модального окна -->
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                            <h4 class="modal-title">Вход</h4>
                        </div>
                        <!-- Основное содержимое модального окна -->

                        <form class="form-horizontal" method="post" action="auntification">
                            <input type="hidden" name="command" value="login">
                            <div class="form-group">
                                <label for="inputEmail3" class="col-sm-2 control-label"><fmt:message key="label.login"
                                                                                                     bundle="${ rb }"/>
                                    <br/></label>
                                <div class="col-sm-10">
                                    <input type="text" class="form-control" id="inputEmail3" placeholder="Email"
                                           name="login">
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="inputPassword3" class="col-sm-2 control-label"><fmt:message
                                        key="label.password" bundle="${ rb }"/></label>
                                <div class="col-sm-10">
                                    <input type="password" class="form-control" id="inputPassword3"
                                           placeholder="Password" name="password">
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="col-sm-offset-2 col-sm-10">
                                    <button type="submit" class="btn btn-default"><fmt:message key="buttonInput.label"
                                                                                               bundle="${ rb }"/></button>
                                </div>
                            </div>
                            <!-- Футер модального окна -->
                            <div class="modal-footer">
                                <a href="<c:url value="/auntification?command=start&type=registrate"/>"
                                   class="link-register"><fmt:message key="label.registrate" bundle="${ trb }"/></a>
                            </div>
                        </form>

                    </div>
                </div>
            </div>


            <form class="navbar-form navbar-right" name="Localization" method="post" action="auntification">
                <input type="hidden" name="command" value="localization">
                <button type="submit" value="Russian" name="language" class="btn btn-default navbar-btn">
                    <fmt:message key="buttonRussian.label" bundle="${ rb }"/>
                </button>
                <button type="submit" name="language" value="English" class="btn btn-default navbar-btn">
                    <fmt:message key="buttonEnglis.label" bundle="${ rb }"/>
                </button>
            </form>
        </div><!-- /.navbar-collapse -->
    </div><!-- /.container-fluid -->
</nav>


<script>
    $(document).ready(function () {
        $(".btn-danger").click(function () {
            $(".remove").modal('show');
        });
    });
    $(document).ready(function () {
        $(".btn-success").click(function () {
            $(".<fmt:message key="buttonRussian.label" bundle="${ rb }"/>").modal('show');
        });
    });
    $(document).ready(function () {
        $(".btn-primary").click(function () {
            $(".update").modal('show');
        });
    });
</script>


</body>
</html>
