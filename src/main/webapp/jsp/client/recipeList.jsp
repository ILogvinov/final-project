<%@ page contentType="text/html; charset=UTF-8" language="java" pageEncoding="utf-8" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<fmt:setLocale value="${locale}" scope="session"/>
<fmt:setBundle basename="content" var="rb"/>


<html>
<head>
    <title>Bootstrap Case</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" type="text/css" href="../css/style.css">
    <link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.0/jquery.min.js"></script>
    <script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
</head>
<body>
<c:import url="header.jsp"/>

<div class="container">

    <div class="page-header">
        <h1><fmt:message key="label.recipe.list" bundle="${rb}"/></h1>
    </div>
    <table class='table table-hover table-responsive table-bordered' style='margin:1em 0 0 0;'>
        <tr>
            <th><fmt:message key="label.name.preparation" bundle="${rb}"/> </th>
            <th><fmt:message key="label.start.date" bundle="${rb}"/></th>
            <th><fmt:message key="label.finish.date" bundle="${rb}"/></th>
            <th><fmt:message key="label.doctor.name" bundle="${rb}"/></th>
            <th><fmt:message key="label.status" bundle="${rb}"/> </th>
        </tr>
        <c:forEach items="${requestScope.recipes}" var="recipe">
            <tr>
                <td>
                    <div><c:out value="${recipe.nameOfPreparation}"/></div>
                </td>
                <td>
                    <div><c:out value="${recipe.startDate}"/></div>
                </td>
                <td>
                    <div><c:out value="${recipe.finishDate}"/></div>
                </td>
                <td>
                    <div><c:out value="${recipe.doctorName}"/></div>
                </td>
                <td>
                    <div>$<c:out value="{recipe.status}"/></div>
                </td>
                <td>
                    <form action="<c:url value="/auntification"/>" method="get">
                        <input type="hidden" name="idRecipe" value="${recipe.id}">
                        <input type="hidden" name="command" value="show_recipe_inf">
                        <button type='submit'
                                class='btn btn-danger'>
                            <fmt:message key="button.detail" bundle="${rb}"/>
                        </button>
                    </form>
                </td>
                <td>
                    <c:if test="${recipe.status eq 'FINISH'}">
                        <form action="<c:url value="/auntification"/>" method="post">
                            <input type="hidden" name="idRecipe" value="${recipe.id}">
                            <input type="hidden" name="command" value="register_request_recipe">
                            <button type='submit'
                                    class='btn btn-danger'>
                               <fmt:message key="button.extend" bundle="${rb}"/>
                            </button>
                        </form>
                    </c:if>
                </td>
            </tr>
        </c:forEach>
    </table>
</div>
</body>
</html>