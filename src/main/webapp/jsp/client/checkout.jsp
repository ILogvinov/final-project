<%--
  Created by IntelliJ IDEA.
  User: Иван
  Date: 25.04.2016
  Time: 1:11
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html; charset=UTF-8" language="java" pageEncoding="utf-8" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="ctg" uri="customtags" %>
<fmt:setLocale value="${locale}" scope="session"/>
<fmt:setBundle basename="content" var="rb"/>
<fmt:setBundle basename="test" var="trb"/>

<html>
<head>
    <title>Bootstrap Case</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" type="text/css" href="../../css/style.css">
    <link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.0/jquery.min.js"></script>
    <script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
</head>
<body>
<c:import url="header.jsp"/>
<div class="container wrapper">
    <ul class="list-group">
        <c:forEach items="${requestScope.preparations}" var="pr">
            <li class="list-group-item"><span class="badge"><ctg:sub-total price="${pr.price}" count="${pr.count}"/></span> ${pr.name}</li>
        </c:forEach>
        <li class="list-group-item list-group-item-success"><span class="badge">${requestScope.totalValue}</span>Total</li>
    </ul>

    <div class="row2 cart-body">
        <form class="form-horizontal" method="post" action="/auntification">
            <input type="hidden" name="command" value="form_order">
            <div class="col-md-pull-6 col-sm-pull-6">
                <!--SHIPPING METHOD-->
                <div class="panel panel-info">
                    <div class="panel-heading"><fmt:message key="label.address" bundle="${rb}"/> </div>
                    <div class="panel-body">
                        <div class="form-group">
                            <div class="col-md-12">
                                <h4><fmt:message key="label.shipping.address" bundle="${rb}"/> </h4>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-md-12"><strong><fmt:message key="label.city" bundle="${rb}"/> </strong></div>
                            <div class="col-md-12">
                                <input type="text" class="form-control" name="city" />
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-md-6 col-xs-12">
                                <strong><fmt:message key="label.street" bundle="${rb}"/></strong>
                                <input type="text" name="street" class="form-control" />
                            </div>
                            <div class="span1"></div>
                            <div class="col-md-6 col-xs-12">
                                <strong><fmt:message key="label.number.house" bundle="${rb}"/></strong>
                                <input type="text" name="numberHouse" class="form-control" />
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="sel1"><fmt:message key="label.payment" bundle="${rb}"/> </label>
                            <select class="form-control" id="sel1" name="paymentMethod">
                                <option value="BANK"><fmt:message key="label.bank" bundle="${rb}"/></option>
                                <option value="CASH"><fmt:message key="button.cancel" bundle="${rb}"/> </option>
                            </select>
                        </div>
                    </div>
                </div>
                <!--SHIPPING METHOD END-->
            </div>
            <div class="btn-group">
                <button class="btn btn-primary"><a style="color: white;  text-decoration: none;" href="<c:url value="/auntification?command=show_cart"/>"><fmt:message key="button.cancel" bundle="${rb}"/></a></button>
                <button type="submit" class="btn btn-primary" ><fmt:message key="label.confirm" bundle="${rb}"/> </button>
            </div>
        </form>
    </div>
    <div class="row cart-footer">

    </div>
</div>
</body>
</html>
