<%@ page contentType="text/html; charset=UTF-8" language="java" pageEncoding="utf-8" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<fmt:setLocale value="${locale}" scope="session"/>
<fmt:setBundle basename="content" var="rb"/>
<fmt:setBundle basename="test" var="trb"/>


<html>
<head>
    <title>Bootstrap Case</title>
    <meta charset="utf-8">
</head>
<body>

<nav class="navbar navbar-default">
    <div class="container-fluid">
        <!-- Brand and toggle get grouped for better mobile display -->
        <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse"
                    data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="#">Pharmacy</a>
        </div>

        <!-- Collect the nav links, forms, and other content for toggling -->
        <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
            <ul class="nav navbar-nav">
                <li class="active"><a href="<c:url value="/auntification?command=show_catalog"/>"><fmt:message key="label.catalog" bundle="${ trb }"/> <span
                        class="sr-only">(current)</span></a></li>
                <li><a href="<c:url value="/auntification?command=open_page&page=news"/>"><fmt:message key="label.news"
                                                                                      bundle="${ trb }"/></a></li>
                <li class='active'>
                    <a href="<c:url value="/auntification?command=show_cart"/>">
                        <fmt:message key="label.cart" bundle="${rb}"/> <span class="badge" id="comparison-count">${countProductCart}</span>
                    </a>
                </li>
                <li class='active'>
                    <a href="<c:url value="/auntification?command=show_order"/>">
                        <fmt:message key="button.show.history.orders" bundle="${rb}"/>
                    </a>
                </li>
                <li class='active'>
                    <a href="<c:url value="/auntification?command=show_recipe_list"/>"><fmt:message key="button.show.recipes" bundle="${rb}"/></a>
                </li>
            </ul>
            <form class="navbar-form navbar-right" name="Localization" method="post" action="<c:url value="/auntification"/>">
                <input type="hidden" name="command" value="logout">
                <button type="submit" class="btn btn-default navbar-btn">
                    <fmt:message key="label.logout" bundle="${rb}"/>
                </button>
            </form>
        </div><!-- /.navbar-collapse -->
    </div><!-- /.container-fluid -->
</nav>

</body>
</html>
