<%@ page contentType="text/html; charset=UTF-8" language="java" pageEncoding="utf-8" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<fmt:setLocale value="${locale}" scope="session"/>
<fmt:setBundle basename="content" var="rb"/>
<fmt:setBundle basename="test" var="trb"/>

<html>
<head>
    <title>Bootstrap Case</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" type="text/css" href="../css/style.css">
    <%@ taglib prefix="ctg" uri="customtags" %>
    <link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.0/jquery.min.js"></script>
    <script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
</head>
<body>
<c:import url="header.jsp"/>

<div class="container">

    <div class="page-header">
        <h1><fmt:message key="label.orders" bundle="${rb}"/></h1>
    </div>
    <table class='table table-hover table-responsive table-bordered' style='margin:1em 0 0 0;'>
        <tr>
            <th class='textAlignLeft'><fmt:message key="label.id.order" bundle="${rb}"/></th>
            <th><fmt:message key="label.date.order" bundle="${rb}"/></th>
            <th><fmt:message key="label.payment" bundle="${rb}"/></th>
            <th><fmt:message key="label.address" bundle="${rb}"/></th>
            <th><fmt:message key="label.status" bundle="${rb}"/></th>
        </tr>
        <c:forEach items="${requestScope.orders}" var="odr">
            <tr>
                <td>
                    <c:out value="${odr.id}"/>
                </td>
                <td>
                    <c:out value="${odr.date}"/>
                </td>
                <td>
                    <c:out value="${odr.paymentMethod}"/>
                </td>
                <td>
                    <c:out value="${odr.address}"/></td>
                <td>
                      <c:out value="${odr.status}"/>
                </td>
            </tr>
        </c:forEach>
    </table>
</div>
</body>
</html>