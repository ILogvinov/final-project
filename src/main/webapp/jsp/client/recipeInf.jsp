<%--
  Created by IntelliJ IDEA.
  User: Иван
  Date: 27.03.2016
  Time: 17:59
  To change this template use File | Settings | File Templates.
--%>

<%@ page contentType="text/html; charset=UTF-8" language="java" pageEncoding="utf-8" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<fmt:setBundle basename="content" var="rb"/>
<fmt:setBundle basename="test" var="trb"/>

<html>
<head>
    <title>Recipe inf</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.0/jquery.min.js"></script>
    <script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
    <fmt:setBundle basename="content" var="rb"/>
    <c:set scope="request" var="recipeInf" value="${requestScope.recipe}" target="${requestScope.recipe}" />
</head>
<body>
<c:import url="header.jsp"/>
<fieldset style=" padding-left: 508px">
    <div class="control-group">
        <h4><fmt:message key="label.name.preparation" bundle="${rb}"/></h4>
        <label class="control-label"><c:out value="${recipeInf.nameOfPreparation}"/></label>
    </div>

    <div class="control-group">
        <h4><fmt:message key="label.dosage" bundle="${rb}"/></h4>
        <label class="control-label"><c:out value="${recipeInf.dosage}"/></label>
    </div>

    <div class="control-group">
        <h4><fmt:message key="label.amount" bundle="${rb}"/></h4>
        <label class="control-label"><c:out value="${recipeInf.amount}"/></label>
    </div>

    <div class="control-group">
        <h4><fmt:message key="label.discount" bundle="${rb}"/></h4>
        <label class="control-label"><c:out value="${recipeInf.discount}"/></label>
    </div>

    <div class="control-group">
        <h4><fmt:message key="label.start.date" bundle="${rb}"/></h4>
        <label class="control-label"><c:out value="${recipeInf.startDate}"/></label>
    </div>

    <div class="control-group">
        <h4><fmt:message key="label.finish.date" bundle="${rb}"/></h4>
        <label class="control-label"><c:out value="${recipeInf.finishDate}"/></label>
    </div>
</fieldset>
</body>
</html>
