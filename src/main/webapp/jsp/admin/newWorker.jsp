
<%@ page contentType="text/html; charset=UTF-8" language="java" pageEncoding="utf-8" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<fmt:setLocale value="${locale}" scope="session"/>
<fmt:setBundle basename="content" var="rb"/>
<html>
<head>
    <title>Bootstrap Case</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" type="text/css" href="../css/style.css">
    <%@ taglib prefix="ctg" uri="customtags" %>
    <link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.0/jquery.min.js"></script>
    <script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
</head>
<body>
<c:import url="header.jsp"/>
<form method="post" action="<c:url value="/auntification"/>">
    <input type="hidden" name="command" value="register_worker">
    <div>
        <h4><fmt:message key="label.client.login" bundle="${rb}"/></h4>
        <label>
            <input name="login" type="text">
        </label>
    </div>
    <div>
        <h4><fmt:message key="label.password" bundle="${rb}"/></h4>
        <label>
            <input name="password" type="password">
        </label>
    </div>


    <div>
        <h4><fmt:message key="label.name" bundle="${rb}"/></h4>
        <label>
            <input name="name" type="text">
        </label>
    </div>


    <div>
        <h4><fmt:message key="label.surname" bundle="${rb}"/></h4>
        <label>
            <input name="surname" type="text">
        </label>
    </div>


    <div>
        <h4><fmt:message key="label.phone.number" bundle="${rb}"/></h4>
        <label>
            <input name="phone_number" type="tel" >
        </label>
    </div>

    <div>
        <h4><fmt:message key="label.email" bundle="${rb}"/></h4>
        <label>
            <input name="email">
        </label>
    </div>

    <div>
        <label>
            <select name="role">
                <option value="DOCTOR"><fmt:message key="label.doctor" bundle="${rb}"/> </option>
                <option value="PHARMACIST"><fmt:message key="label.pharmacist" bundle="${rb}"/></option>
            </select>
            Role
        </label>
    </div>


    <input type="submit" value=" <fmt:message key="button.add.new.user" bundle="${rb}"/>">
</form>
${errorMessage}
${successMessage}
</body>
</html>
