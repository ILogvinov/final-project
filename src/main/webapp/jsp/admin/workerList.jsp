<%@ page contentType="text/html; charset=UTF-8" language="java" pageEncoding="utf-8" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<fmt:setLocale value="${locale}" scope="session"/>
<fmt:setBundle basename="content" var="rb"/>
<fmt:setBundle basename="test" var="trb"/>

<html>
<head>
    <title>Bootstrap Case</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" type="text/css" href="../css/style.css">
    <%@ taglib prefix="ctg" uri="customtags" %>
    <link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.0/jquery.min.js"></script>
    <script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
</head>
<body>
<c:import url="header.jsp"/>

<div class="container">

    <div class="page-header">
        <h1><fmt:message key="label.worker.list" bundle="${rb}"/> </h1>
    </div>
    <table class='table table-hover table-responsive table-bordered' style='margin:1em 0 0 0;'>
        <tr>
            <th><fmt:message key="label.name" bundle="${rb}"/> </th>
            <th><fmt:message key="label.surname" bundle="${rb}"/> </th>
            <th><fmt:message key="label.email" bundle="${rb}"/> </th>
            <th><fmt:message key="label.phone.number" bundle="${rb}"/> </th>
            <th><fmt:message key="label.role" bundle="${rb}"/> </th>
        </tr>
        <c:forEach items="${requestScope.workers}" var="worker">
            <tr>
                <td>
                    <div><c:out value="${worker.name}"/></div>
                </td>
                <td>
                    <div ><c:out value="${worker.surname}"/></div>
                </td>
                <td>
                    <div ><c:out value="${worker.email}"/></div>
                </td>
                <td>
                    <div ><c:out value="${worker.phoneNumber}"/></div>
                </td>
                <td>
                    <div >${worker.role}</div>
                </td>
                <td>
                    <form  action="<c:url value="/auntification"/>" method="post">
                        <input type="hidden" name="login" value="${worker.login}">
                        <input type="hidden" name="command" value="delete_user">
                        <button type='submit'
                                class='btn btn-danger'>
                            <fmt:message key="button.remove" bundle="${rb}"/>
                        </button>
                    </form>
                </td>
            </tr>
        </c:forEach>
    </table>
</div>
</body>
</html>