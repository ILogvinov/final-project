<%@ page contentType="text/html; charset=UTF-8" language="java" pageEncoding="utf-8" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<fmt:setLocale value="${locale}" scope="session"/>
<fmt:setBundle basename="content" var="rb"/>


<html>
<head>
    <title>Bootstrap Case</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" type="text/css" href="../css/style.css">
    <link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.0/jquery.min.js"></script>
    <script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
</head>
<body>
<c:import url="header.jsp"/>

<div class="container">

    <div class="page-header">
        <h1><fmt:message key="label.list.preparation" bundle="${rb}"/></h1>
    </div>
    <table class='table table-hover table-responsive table-bordered' style='margin:1em 0 0 0;'>
        <tr>
            <th class='textAlignLeft'><fmt:message key="label.name.preparation" bundle="${rb}"/></th>
            <th><fmt:message key="label.price" bundle="${rb}"/></th>
            <th><fmt:message key="label.finish.date" bundle="${rb}"/></th>
            <th style='width:15em;'><fmt:message key="label.quantity" bundle="${rb}"/></th>
            <th><fmt:message key="label.action" bundle="${rb}"/></th>
        </tr>
        <c:forEach items="${requestScope.preparations}" var="pr">
            <tr>
                <td>
                    <c:out value="${pr.name}"/>
                </td>
                <td>
                    <c:out value="${pr.price}"/>
                </td>
                <td>
                    <c:out value="${pr.endShelfLife}"/>
                </td>

                <td>
                    <form class='update-quantity-form' action="<c:url value="/auntification"/>" method="post">
                        <input type="hidden" name="idProduct" value="${pr.lotNumber}">
                        <input type="hidden" name="command" value="change_count_product">
                        <div class='input-group'><input type='number' name='quantity' value=${pr.count} min='1'
                                                        class='form-control'
                                                        required><span class='input-group-btn'><button type='submit'
                                                                                                       class='btn btn-default update-quantity'>
                            <fmt:message key="button.update" bundle="${rb}"/>
                        </button></span></div>
                    </form>
                </td>


                <td>
                    <form action="<c:url value="/auntification"/>" method="post">
                        <input type="hidden" name="idProduct" value="${pr.lotNumber}">
                        <input type="hidden" name="command" value="delete_product">
                        <button type='submit'
                                class='btn btn-danger'>
                            <fmt:message key="button.remove" bundle="${rb}"/>
                        </button>
                    </form>
                </td>
            </tr>
        </c:forEach>
    </table>
</div>
${requestScope.errorMessage}
</body>
</html>