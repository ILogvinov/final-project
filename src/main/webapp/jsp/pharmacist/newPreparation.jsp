<%@ page contentType="text/html; charset=UTF-8" language="java" pageEncoding="utf-8" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<fmt:setLocale value="${locale}" scope="session"/>
<fmt:setBundle basename="content" var="rb"/>


<html>
<head>
    <title>Bootstrap Case</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" type="text/css" href="../css/style.css">
    <%@ taglib prefix="ctg" uri="customtags" %>
    <link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.0/jquery.min.js"></script>
    <script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
</head>
<body>
<c:import url="header.jsp"/>

<div class="container">
<form method="post" action="<c:url value="/auntification"/>" >
    <input type="hidden" name="command" value="add_new_preparation">
    <div class="form-group">
        <label for="i1"><fmt:message key="label.preparation.id" bundle="${rb}"/> </label>
        <input type="text" class="form-control" id="i1" name="id" pattern="^[\w\d]{9,10}$">
        ${requestScope.invalid_id}
    </div>
    <div class="form-group">
        <label for="group"><fmt:message key="label.preparation.group" bundle="${rb}"/> </label>
        <input type="text" class="form-control" id="group" name="group">
        ${requestScope.invalid_group}
    </div>
    <div class="form-group">
        <label for="name"><fmt:message key="label.name.preparation" bundle="${rb}"/>  </label>
        <input type="text" class="form-control" id="name" name="name" pattern="^[\w\s-]+$">
        ${requestScope.invalid_name}
    </div>
    <div class="form-group">
        <label for="manufacture"><fmt:message key="label.manufacture" bundle="${rb}"/></label>
        <input type="text" class="form-control" id="manufacture" name="manufacture" pattern="^[\d]{2,7}$">>
        ${requestScope.invalid_manufacture}
    </div>
    <div class="form-group">
        <label for="dosage"><fmt:message key="label.dosage" bundle="${rb}"/></label>
        <input type="text" class="form-control" id="dosage" name="dosage">
        ${requestScope.invalid_dosage}
    </div>
    <div class="form-group">
        <label for="units"><fmt:message key="label.count.units" bundle="${rb}"/></label>
        <input type="text" class="form-control" id="units" name="countUnits">
        ${requestScope.invalid_count_units}
    </div>
    <div class="form-group">
        <label for="testimony"><fmt:message key="label.testimony" bundle="${rb}"/></label>
        <textarea class="form-control" rows="5" id="testimony" name="testimony"></textarea>
        ${requestScope.invalid_testimony}
    </div>
    <div class="form-group">
        <label for="effect"><fmt:message key="label.pharm.effect" bundle="${rb}"/></label>
        <textarea class="form-control" rows="5" id="effect" name="effect"></textarea>
        ${requestScope.invalid_effect}
    </div>

    <div class="form-group">
        <label for="contr"><fmt:message key="label.contraindications" bundle="${rb}"/></label>
        <textarea class="form-control" rows="5" id="contr" name="contraindications"></textarea>
        ${requestScope.invalid_contraindication}
    </div>
    <div class="form-group">
        <label for="mode"><fmt:message key="label.modeOfApplication" bundle="${rb}"/></label>
        <textarea class="form-control" rows="5" id="mode" name="mode"></textarea>
        ${requestScope.invalid_mode}
    </div>
<input type="submit" value="Add new preparation">
</form>
</div>
</body>
</html>