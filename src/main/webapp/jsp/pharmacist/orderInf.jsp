<%@ page contentType="text/html; charset=UTF-8" language="java" pageEncoding="utf-8" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<fmt:setLocale value="${locale}" scope="session"/>
<fmt:setBundle basename="content" var="rb"/>


<html>
<head>
    <title>Bootstrap Case</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" type="text/css" href="../css/style.css">
    <%@ taglib prefix="ctg" uri="customtags" %>
    <link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.0/jquery.min.js"></script>
    <script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
</head>
<body>
<c:import url="header.jsp"/>

<div class="container">

    <div class="page-header">
        <h1><fmt:message key="label.order" bundle="${rb}"/></h1>
    </div>

    <table class='table table-hover table-responsive table-bordered' style='margin:1em 0 0 0;'>
        <tr>
            <th class='textAlignLeft'><fmt:message key="label.name.preparation" bundle="${rb}"/></th>
            <th><fmt:message key="label.quantity" bundle="${rb}"/></th>
            <th style='width:15em;'><fmt:message key="label.total" bundle="${rb}"/></th>
            <th>ID</th>
        </tr>
        <c:forEach items="${requestScope.preparations}" var="pr">
            <tr>
                <td>
                    <div class='product-name'><c:out value="${pr.name}"/></div>
                </td>
                <td>
                    <div class='product-price'><ctg:sub-total price="${pr.price}" count="${pr.count}"/></div>
                </td>
                <td><c:out value="${pr.count}"/></td>
                <td>
                    <a href="${pageContext.request.contextPath}/auntification?command=show_recipe_inf&idRecipe=${pr.recipe}"
                       class='btn btn-success'><span class='glyphicon '></span> ${pr.recipe}</a></td>
            </tr>
        </c:forEach>

        <tr>
            <td></td>
            <td></td>
            <td></td>
            <td>
                <form action="<c:url value="/auntification"/>" method="post">
                    <input type="hidden" name="command" value="update_order_status">
                    <input type="hidden" name="idOrder" value="${requestScope.idOrder}">
                    <input type="hidden" name="status" value="REJECTED">
                    <input type="submit" value="<fmt:message key="button.cancel" bundle="${rb}"/>">
                </form>
            </td>
            <td>
                <form action="<c:url value="/auntification"/>" method="post">
                    <input type="hidden" name="command" value="update_order_status">
                    <input type="hidden" name="idOrder" value="${requestScope.idOrder}">
                    <input type="hidden" name="status" value="DELIVERED">
                    <input type="submit" value="<fmt:message key="label.confirm" bundle="${rb}"/>">
                </form>
            </td>
        </tr>
    </table>
</div>
${requestScope.errorMessage}
</body>
</html>