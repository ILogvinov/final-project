<%@ page contentType="text/html; charset=UTF-8" language="java" pageEncoding="utf-8" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<fmt:setLocale value="${locale}" scope="session"/>
<fmt:setBundle basename="content" var="rb"/>


<html>
<head>
    <title>Bootstrap Case</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" type="text/css" href="../css/style.css">
    <link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.0/jquery.min.js"></script>
    <script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
</head>
<body>
<c:import url="header.jsp"/>

<div class="container">

    <div class="page-header">
        <h1><fmt:message key="label.list.preparation" bundle="${rb}"/></h1>
    </div>
    <table class='table table-hover table-responsive table-bordered' style='margin:1em 0 0 0;'>
        <tr>
            <th class='textAlignLeft'><fmt:message key="label.name.preparation" bundle="${rb}"/></th>
            <th><fmt:message key="label.preparation.group" bundle="${rb}"/></th>
            <th style='width:15em;'><fmt:message key="label.dosage" bundle="${rb}"/></th>
            <th><fmt:message key="label.count.units" bundle="${rb}"/></th>
            <th><fmt:message key="label.manufacture" bundle="${rb}"/></th>
            <th><fmt:message key="label.country" bundle="${rb}"/></th>
        </tr>
        <c:forEach items="${requestScope.preparations}" var="pr">
            <tr>
                <td>
                    <c:out value="${pr.name}"/>
                </td>
                <td>
                    <c:out value="${pr.group}"/>
                </td>
                <td>
                    <c:out value="${pr.dosage}"/>
                </td>
                <td>
                    <c:out value="${pr.countUnits}"/>
                </td>
                <td>
                    <c:out value="${pr.manufacturer}"/>
                </td>
                <td>
                    <c:out value="${pr.country}"/>
                </td>

            </tr>
        </c:forEach>
    </table>
</div>
${requestScope.errorMessage}
</body>
</html>