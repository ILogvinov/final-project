<%@ page contentType="text/html; charset=UTF-8" language="java" pageEncoding="utf-8" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<fmt:setLocale value="${locale}" scope="session"/>
<fmt:setBundle basename="content" var="rb"/>


<html>
<head>
    <title>Bootstrap Case</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" type="text/css" href="../css/style.css">
    <%@ taglib prefix="ctg" uri="customtags" %>
    <link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.0/jquery.min.js"></script>
    <script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
</head>
<body>
<c:import url="header.jsp"/>

<div class="container">
    <form method="post" action="<c:url value="/auntification"/>">
        <input type="hidden" name="command" value="add_new_product">
        <div class="form-group">
            <label for="i1"><fmt:message key="label.lot.number" bundle="${rb}"/> </label>
            <input type="text" class="form-control" id="i1" name="lotNumber">
            ${requestScope.invalid_lot}
        </div>
        <div class="form-group">
            <label for="group"><fmt:message key="label.count" bundle="${rb}"/> </label>
            <input type="text" class="form-control" id="group" name="count">
            ${requestScope.invalid_count}
        </div>
        <div class="form-group">
            <label for="manufacture"><fmt:message key="label.finish.date" bundle="${rb}"/> </label>
            <input type="text" class="form-control" id="manufacture" name="endShelfLife">
            ${requestScope.invalid_finish_date}
        </div>
        <div class="form-group">
            <label for="dosage"><fmt:message key="label.price" bundle="${rb}"/> </label>
            <input type="text" class="form-control" id="dosage" name="price">
            ${requestScope.invalid_price}
        </div>
        <div class="form-group">
            <label for="units"><fmt:message key="label.preparation.id" bundle="${rb}"/> </label>
            <input type="text" class="form-control" id="units" name="id">
            ${requestScope.invalid_id}
        </div>
        <input type="submit" value="<fmt:message key="button.add.new.product" bundle="${rb}"/>">
    </form>
</div>
</body>
</html>