<%--
  Created by IntelliJ IDEA.
  User: Иван
  Date: 27.03.2016
  Time: 17:59
  To change this template use File | Settings | File Templates.
--%>

<%@ page contentType="text/html; charset=UTF-8" language="java" pageEncoding="utf-8" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<fmt:setLocale value="${locale}" scope="session"/>
<fmt:setBundle basename="content" var="rb"/>


<html>
<head>
    <title>Bootstrap Case</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.0/jquery.min.js"></script>
    <script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
</head>
<body>

<nav class="navbar navbar-default">
    <div class="container-fluid">
        <!-- Brand and toggle get grouped for better mobile display -->
        <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse"
                    data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="#">Pharmacy</a>
        </div>

        <!-- Collect the nav links, forms, and other content for toggling -->
        <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
            <ul class="nav navbar-nav">
                <li><a href="<c:url value="/auntification?command=open_page&page=new.product"/>"><fmt:message
                        key="button.add.new.product"
                        bundle="${ rb }"/></a></li>
                <li><a href="<c:url value="/auntification?command=show_list_product"/>"><fmt:message
                        key="button.show.list.product"
                        bundle="${ rb }"/></a></li>
                <li><a href="<c:url value="/auntification?command=open_page&page=new.preparation"/>"><fmt:message
                        key="button.add.new.preparation"
                        bundle="${ rb }"/></a></li>
                <li><a href="<c:url value="/auntification?command=show_pharmacist_order"/>"><fmt:message
                        key="button.form.order"
                        bundle="${ rb }"/></a></li>
                <li><a href="<c:url value="/auntification?command=show_list_preparation"/>"><fmt:message
                        key="button.show.list.preparation"
                        bundle="${ rb }"/></a></li>
                <li><a href="<c:url value="/auntification?command=show_closed_orders"/>"><fmt:message
                        key="label.show.closed.orders"
                        bundle="${ rb }"/></a></li>
            </ul>
            <form class="navbar-form navbar-right" name="Localization" method="post"
                  action="<c:url value="/auntification"/>">
                <input type="hidden" name="command" value="logout">
                <button type="submit" class="btn btn-default navbar-btn">
                    <fmt:message key="label.logout" bundle="${rb}"/>
                </button>
            </form>
        </div><!-- /.navbar-collapse -->
    </div><!-- /.container-fluid -->
</nav>


</body>
</html>
