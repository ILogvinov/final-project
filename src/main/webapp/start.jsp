<%--
  Created by IntelliJ IDEA.
  User: Иван
  Date: 27.03.2016
  Time: 17:59
  To change this template use File | Settings | File Templates.
--%>

<%@ page contentType="text/html; charset=UTF-8" language="java" pageEncoding="utf-8" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib prefix="ctg" uri="customtags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<fmt:setLocale value="${locale}" scope="session"/>
<fmt:setBundle basename="content" var="rb"/>
<fmt:setBundle basename="test" var="trb"/>

<html>
<head>
    <title>Bootstrap Case</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/css/style.css">
    <link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.0/jquery.min.js"></script>
    <script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
</head>
<body>
<c:import url="jsp/guest/header.jsp"/>
<div class="container">
    <div class="row">
        <div class="col-sm-6 col-sm-offset-3">
            <div id="imaginary_container">
                <form method="get" action="/auntification">
                    <div class="input-group stylish-input-group">

                        <input hidden name="command" value="search_preparation">
                        <input type="text" class="form-control" placeholder="Search" name="preparationName">
                        <span class="input-group-addon">
                            <button type="submit">
                                <span class="glyphicon glyphicon-search"></span>
                            </button>
                        </span>


                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<div class="container">
    ${requestScope.errorMessage}
</div>


<script>
    $(document).ready(function () {
        $(".btn-danger").click(function () {
            $(".remove").modal('show');
        });
    });
    $(document).ready(function () {
        $(".btn-success").click(function () {
            $(".<fmt:message key="buttonRussian.label" bundle="${ rb }"/>").modal('show');
        });
    });
    $(document).ready(function () {
        $(".btn-primary").click(function () {
            $(".update").modal('show');
        });
    });
</script>


</body>
</html>
