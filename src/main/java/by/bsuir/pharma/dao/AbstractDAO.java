package by.bsuir.pharma.dao;

import by.bsuir.pharma.dao.exception.DAOException;
import by.bsuir.pharma.dao.implementation.Identified;
import by.bsuir.pharma.pool.ConnectionPool;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.Serializable;
import java.sql.*;
import java.util.List;

/**
 * Created by Иван on 25.03.2016.
 */
public abstract class AbstractDAO<T extends Identified<PK>, PK extends Serializable> {

    private static Logger logger = LogManager.getLogger(AbstractDAO.class);


    public abstract String getSelectALLQuery();

    public abstract String getCreateQuery();

    public abstract String getSelectQuery();

    public abstract String getUpdateQuery();

    public abstract String getDeleteQuery();

    protected abstract void prepareStatementForInsert(PreparedStatement statement, T object) throws DAOException;

    protected abstract void prepareStatementForUpdate(PreparedStatement statement, T object) throws DAOException;

    protected abstract void prepareStatementForDelete(PreparedStatement statement, T object) throws DAOException;




    protected abstract List<T> parseResultSet(ResultSet rs) throws DAOException;


    public T findByPK(String key) throws DAOException {
        List<T> list;
        String sql = getSelectQuery();
        ConnectionPool pool = ConnectionPool.getInstance();
        try (Connection connection = pool.getConnection();
             PreparedStatement statement = connection.prepareStatement(sql)
        ) {
            statement.setString(1, key);
            ResultSet rs = statement.executeQuery();
            list = parseResultSet(rs);
        } catch (SQLException e) {
            throw new DAOException(e);
        }
        if (list == null || list.isEmpty()) {
            return null;
        }
        return list.iterator().next();
    }


    public void update(T object) throws DAOException {
        String sql = getUpdateQuery();
        ConnectionPool pool = ConnectionPool.getInstance();
        try (Connection connection = pool.getConnection();
             PreparedStatement statement = connection.prepareStatement(sql)
        ) {
            prepareStatementForUpdate(statement, object);
            statement.executeUpdate();
        } catch (SQLException e) {
            throw new DAOException(e);
        }
    }


    public void delete(T object) throws DAOException {
        String sql = getDeleteQuery();
        ConnectionPool pool = ConnectionPool.getInstance();
        try (Connection connection = pool.getConnection();
             PreparedStatement statement = connection.prepareStatement(sql)
        ) {
            prepareStatementForDelete(statement,object);
            statement.executeUpdate();

        } catch (SQLException e) {
            throw new DAOException(e);
        }
    }
}
