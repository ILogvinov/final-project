package by.bsuir.pharma.dao.implementation;

import by.bsuir.pharma.dao.AbstractDAO;
import by.bsuir.pharma.dao.exception.DAOException;
import by.bsuir.pharma.entity.Basket;
import by.bsuir.pharma.entity.Preparation;
import by.bsuir.pharma.pool.ConnectionPool;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Иван on 10.05.2016.
 */
public class BasketDAO extends AbstractDAO<Basket, String> {
    private final static String SELECT_QUERY = "SELECT `count`,";
    private final static String INSERT_QUERY = "INSERT INTO pharmacy.`basket` (`Order_idOrder`, `count`, `Product_lot_number`,`Order_User_login`)\n" +
            "SELECT * FROM (SELECT '0' as `odr`, ? as `countLot`, ? as `lot`, ? as `user_login`) AS `tmp`\n" +
            "WHERE NOT EXISTS (\n" +
            "    SELECT `Order_idOrder`,`Product_lot_number`,`Order_User_login` FROM pharmacy.`basket` where `Order_User_login` = `user_login` and `order_idOrder` = `odr` and `Product_lot_number` = `lot` \n" +
            ") LIMIT 1;";

    private final static String FIND_COUNT_QUERY = "SELECT count(`Product_lot_number`) AS `count_product_cart` FROM pharmacy.`basket` WHERE `order_User_login` = ? AND `Order_idOrder` = 0;";


    private final static String FIND_PRODUCT_BY_ID_ORDER = "SELECT `name`,`lot_number`, `price`, `count`,`recipe_recipeId` FROM pharmacy.`product` JOIN pharmacy.`preparation` ON idNumber = Preparation_idNumber \n" +
            "JOIN pharmacy.`basket`  ON `lot_number` = `Product_lot_number` WHERE `order_idOrder` = ?";


    private final static String FIND_PRODUCT_BY_ID_QUERY = "SELECT `name`, `price` FROM pharmacy.`product` JOIN pharmacy.`preparation` ON idNumber = Preparation_idNumber WHERE `lot_number` = ?;";

    private final static String FIND_PRODUCT_BY_ID_LOGIN = "SELECT `isRequiereRecipe`,`recipe_recipeId`,`name`,`lot_number`, `price`, `count`, `discount` FROM pharmacy.`product` JOIN pharmacy.`preparation` ON idNumber = Preparation_idNumber \n" +
            "JOIN pharmacy.`basket`  ON `lot_number` = `Product_lot_number` LEFT JOIN recipe ON basket.recipe_recipeId = recipe.recipeId = recipeId WHERE `order_User_login` = ? AND `order_idOrder` = 0";

    private final static String UPDATE_QUERY = "UPDATE pharmacy.`basket` SET `count` = ? where `order_User_login` = ? and `Order_idOrder` = ? and `Product_lot_number` = ?;";

    private final static String UPDATE_ORDER_QUERY = "UPDATE pharmacy.`basket` SET `order_idOrder` = ? WHERE `order_User_login` = ? AND `order_idOrder` = 0;";

    private final static String DELETE_QUERY = "DELETE FROM pharmacy.`basket` where `Product_lot_number` = ? and  `Order_idOrder` = 0 and `order_User_login` = ?;";

    private final static String FIND_PRODUCT_WITHOUT_RECIPE = "SELECT " +
            "    COUNT(*) AS `not_register_preparation`" +
            "FROM pharmacy.`basket`" +
            "        JOIN" +
            "    pharmacy.`product` ON basket.`Product_lot_number` = product.`lot_number`" +
            "        JOIN" +
            "    pharmacy.`preparation` ON product.`Preparation_idNumber` = preparation.`idNumber`" +
            "WHERE" +
            "    basket.`order_idOrder` = 0" +
            "        AND `order_User_login` = ?" +
            "        AND `isRequiereRecipe` = 1" +
            "        AND `recipe_recipeId` IS NULL;";

    @Override
    public String getSelectALLQuery() {
        throw new UnsupportedOperationException();
    }

    @Override
    public String getCreateQuery() {
        return INSERT_QUERY;
    }

    @Override
    public String getSelectQuery() {
        throw new UnsupportedOperationException();
    }

    @Override
    public String getUpdateQuery() {
        return UPDATE_QUERY;
    }

    @Override
    public String getDeleteQuery() {
        return DELETE_QUERY;
    }

    @Override
    protected void prepareStatementForInsert(PreparedStatement statement, Basket object) throws DAOException {
        try {
            statement.setFloat(1, object.getCount());
            statement.setInt(2, object.getProductNumber());
            statement.setString(3, object.getLoginUser());
        } catch (SQLException e) {
            throw new DAOException(e);
        }
    }

    @Override
    protected void prepareStatementForUpdate(PreparedStatement statement, Basket object) throws DAOException {
        try {
            statement.setFloat(1, object.getCount());
            statement.setString(2, object.getLoginUser());
            statement.setInt(3, object.getOrder());
            statement.setInt(4, object.getProductNumber());
        } catch (SQLException e) {
            throw new DAOException(e);
        }
    }

    @Override
    protected void prepareStatementForDelete(PreparedStatement statement, Basket object) throws DAOException {
        try {
            statement.setInt(1, object.getProductNumber());
            statement.setString(2, object.getLoginUser());
        } catch (SQLException e) {
            throw new DAOException(e);
        }
    }

    public static boolean update(String login, Integer idOrder) throws DAOException {
        String sql = UPDATE_ORDER_QUERY;
        ConnectionPool pool = ConnectionPool.getInstance();
        try (Connection connection = pool.getConnection();
             PreparedStatement statement = connection.prepareStatement(sql)
        ) {
            statement.setInt(1, idOrder);
            statement.setString(2, login);
            statement.executeUpdate();

        } catch (SQLException e) {
            throw new DAOException(e);
        }
        return true;
    }


    public static List<Preparation> findProductInBasket(String login) throws DAOException {
        ConnectionPool connectionPool = ConnectionPool.getInstance();
        List<Preparation> preparations = new ArrayList<>();
        try (Connection connection = connectionPool.getConnection();
             PreparedStatement statement = connection.prepareStatement(FIND_PRODUCT_BY_ID_LOGIN)
        ) {
            statement.setString(1, login);
            ResultSet resultSet = statement.executeQuery();
            while (resultSet.next()) {
                Preparation preparation = new Preparation();
                preparation.setRequireRecipe(resultSet.getBoolean("isRequiereRecipe"));
                preparation.setLotNumber(resultSet.getInt("lot_number"));
                preparation.setName(resultSet.getString("name"));
                preparation.setCount(resultSet.getFloat("count"));
                preparation.setPrice(resultSet.getFloat("price"));
                preparation.setRecipe(resultSet.getInt("recipe_recipeId"));
                preparation.setDiscount(resultSet.getInt("discount"));
                preparations.add(preparation);
            }
        } catch (SQLException e) {
            throw new DAOException(e);
        }
        return preparations;
    }

    public static Preparation findProductInBasket(Integer idProduct) throws DAOException {
        ConnectionPool connectionPool = ConnectionPool.getInstance();
        Preparation preparation = new Preparation();
        try (Connection connection = connectionPool.getConnection();
             PreparedStatement statement = connection.prepareStatement(FIND_PRODUCT_BY_ID_QUERY)
        ) {
            statement.setInt(1, idProduct);
            ResultSet resultSet = statement.executeQuery();
            while (resultSet.next()) {
                preparation.setLotNumber(idProduct);
                preparation.setName(resultSet.getString("name"));
                preparation.setPrice(resultSet.getFloat("price"));
            }
        } catch (SQLException e) {
            throw new DAOException(e);
        }
        return preparation;
    }

    public static List<Preparation> findProductOfOrder(Integer idOrder) throws DAOException {
        List<Preparation> preparations = new ArrayList<>();
        ConnectionPool connectionPool = ConnectionPool.getInstance();
        try (Connection connection = connectionPool.getConnection();
             PreparedStatement statement = connection.prepareStatement(FIND_PRODUCT_BY_ID_ORDER)
        ) {
            statement.setInt(1, idOrder);
            ResultSet resultSet = statement.executeQuery();
            while (resultSet.next()) {
                Preparation preparation = new Preparation();
                preparation.setLotNumber(resultSet.getInt("lot_number"));
                preparation.setName(resultSet.getString("name"));
                preparation.setCount(resultSet.getFloat("count"));
                preparation.setPrice(resultSet.getFloat("price"));
                preparation.setRecipe(resultSet.getInt("recipe_recipeId"));
                preparations.add(preparation);
            }
        } catch (SQLException e) {
            throw new DAOException(e);
        }
        return preparations;
    }


    public static Integer findCountOfProduct(String login) throws DAOException {
        String sql = FIND_COUNT_QUERY;
        ConnectionPool connectionPool = ConnectionPool.getInstance();
        Integer countProduct = 0;
        try (Connection connection = connectionPool.getConnection();
             PreparedStatement statement = connection.prepareStatement(sql)
        ) {
            statement.setString(1, login);
            ResultSet resultSet = statement.executeQuery();
            if (resultSet.next()) {
                countProduct = resultSet.getInt("count_product_cart");
            } else {
                throw new DAOException("Error of find of count product");
            }
        } catch (SQLException e) {
            throw new DAOException(e);
        }
        return countProduct;
    }


    public boolean add(Basket object) throws DAOException {
        String sql = getCreateQuery();
        boolean result;
        ConnectionPool connectionPool = ConnectionPool.getInstance();
        try (Connection connection = connectionPool.getConnection();
             PreparedStatement statement = connection.prepareStatement(sql)
        ) {
            prepareStatementForInsert(statement, object);
            int count = statement.executeUpdate();
            result = count != 0;
        } catch (SQLException e) {
            throw new DAOException(e);
        }
        return result;
    }


    public static Integer findCountProductWithoutRecipe(String clientLogin) throws DAOException {
        Integer result = 0;
        ConnectionPool connectionPool = ConnectionPool.getInstance();
        try (Connection connection = connectionPool.getConnection();
             PreparedStatement statement = connection.prepareStatement(FIND_PRODUCT_WITHOUT_RECIPE)
        ) {
            statement.setString(1, clientLogin);
            ResultSet rs = statement.executeQuery();
            if (rs.next()) {
                result = rs.getInt("not_register_preparation");
            }
        } catch (SQLException e) {
            throw new DAOException(e);
        }
        return result;
    }


    @Override
    protected List<Basket> parseResultSet(ResultSet rs) throws DAOException {
        throw new UnsupportedOperationException();
    }
}
