package by.bsuir.pharma.dao.implementation;

import by.bsuir.pharma.dao.AbstractDAO;
import by.bsuir.pharma.dao.exception.DAOException;
import by.bsuir.pharma.entity.RequestToRecipe;
import by.bsuir.pharma.pool.ConnectionPool;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Иван on 26.05.2016.
 */
public class RequestToRecipeDAO extends AbstractDAO<RequestToRecipe, Integer> {
    private final static String INSERT_QUERY = "INSERT INTO pharmacy.`requesttorecipe`(`dateOfRecipe`, `state`, `Recipe_recipeId`)\n" +
            "SELECT * FROM (SELECT curdate(), 'PROCESSING', ?) as tmp  \n" +
            "WHERE NOT EXISTS (\n" +
            "SELECT `Recipe_recipeId` FROM pharmacy.`requesttorecipe` WHERE `state` = 'PROCESSING' AND `Recipe_recipeId` = ?)";
    private final static String DELETE_QUERY = "DELETE FROM `pharmacy`.`requesttorecipe` WHERE id = ? ";
    private final static String FIND_PROCESSING_REQUEST_DOCTOR = "SELECT `id`,`recipeId`, `name_preparation`, concat(`name`,' ',`surname`) as clientData, `dateOfRecipe`  FROM pharmacy.`recipe` join pharmacy.`requesttorecipe` on `recipeId` = `Recipe_recipeId` join pharmacy.`user` on `login` = `client_login` WHERE `doctor_login` = ? AND `state` = ?";
    private final static String REJECTED_EXTENSION_RECIPE_QUERY = "UPDATE pharmacy.`requesttorecipe` SET requesttorecipe.`state` = 'REJECTED' WHERE requesttorecipe.`id` = ?;";
    private final static String EXTENSION_RECIPE_QUERY = "UPDATE pharmacy.`requesttorecipe` \n" +
            "JOIN pharmacy.`recipe` on requesttorecipe.`Recipe_recipeId` = recipe.`recipeId`\n" +
            "SET recipe.`status` = 'ACTIVE'," +
            "requesttorecipe.`state` = 'REJECTED'," +
            "recipe.`finish_date` = ?" +
            "WHERE requesttorecipe.`Recipe_recipeId` = ? AND `state` = 'PROCESSING';";
    private static final String UPDATE_STATUS_RECIPE_QUERY = "UPDATE pharmacy.`recipe` SET `status` = ? WHERE recipeId = ?;";


    @Override
    public String getSelectALLQuery() {
        throw new UnsupportedOperationException();
    }

    @Override
    public String getCreateQuery() {
        return INSERT_QUERY;
    }

    @Override
    public String getSelectQuery() {
        throw new UnsupportedOperationException();
    }

    @Override
    public String getUpdateQuery() {
        throw new UnsupportedOperationException();
    }

    @Override
    public String getDeleteQuery() {
        return DELETE_QUERY;
    }

    @Override
    protected void prepareStatementForInsert(PreparedStatement statement, RequestToRecipe object) throws DAOException {
        try {
            statement.setDate(1, object.getDate());
            statement.setString(2, object.getStatus());
            statement.setInt(3, object.getIdRecipe());
        } catch (SQLException e) {
            throw new DAOException(e);
        }

    }

    @Override
    protected void prepareStatementForUpdate(PreparedStatement statement, RequestToRecipe object) throws DAOException {
        try {
            statement.setString(1, object.getStatus());
            statement.setInt(2, object.getId());
        } catch (SQLException e) {
            throw new DAOException(e);
        }

    }

    @Override
    protected void prepareStatementForDelete(PreparedStatement statement, RequestToRecipe object) throws DAOException {
        try {
            statement.setInt(1, object.getId());
        } catch (SQLException e) {
            throw new DAOException(e);
        }
    }

    @Override
    protected List<RequestToRecipe> parseResultSet(ResultSet rs) throws DAOException {
        throw new UnsupportedOperationException();
    }

    public static boolean rejectRequest(Integer idRequest) throws DAOException {
        ConnectionPool connectionPool = ConnectionPool.getInstance();
        try (Connection connection = connectionPool.getConnection();
             PreparedStatement statement = connection.prepareStatement(REJECTED_EXTENSION_RECIPE_QUERY)
        ) {
            statement.setInt(1, idRequest);
            statement.executeUpdate();
        } catch (SQLException e) {
            throw new DAOException(e);
        }
        return true;
    }

    public static boolean extendRecipe(Integer idRequest, Date newFinishDate) throws DAOException {
        ConnectionPool connectionPool = ConnectionPool.getInstance();
        try (Connection connection = connectionPool.getConnection();
             PreparedStatement statement = connection.prepareStatement(EXTENSION_RECIPE_QUERY)
        ) {
            statement.setDate(1, newFinishDate);
            statement.setInt(2, idRequest);
            statement.executeUpdate();
        } catch (SQLException e) {
            throw new DAOException(e);
        }
        return true;
    }


    public boolean add(Integer idRecipe) throws DAOException {
        ConnectionPool connectionPool = ConnectionPool.getInstance();
        boolean result = true;
        try (Connection connection = connectionPool.getConnection()) {
            try (
                    PreparedStatement statement = connection.prepareStatement(INSERT_QUERY);
                    PreparedStatement updateStatement = connection.prepareStatement(UPDATE_STATUS_RECIPE_QUERY);
            ) {
                connection.setAutoCommit(false);
                statement.setInt(1, idRecipe);
                statement.setInt(2, idRecipe);
                updateStatement.setString(1, "PROCESSING");
                updateStatement.setInt(2, idRecipe);
                statement.executeUpdate();
                updateStatement.executeUpdate();
                connection.commit();
                connection.setAutoCommit(true);
            } catch (SQLException e) {
                connection.rollback();
                connection.setAutoCommit(true);
                throw new DAOException(e);
            }

        } catch (SQLException e) {
            throw new DAOException(e);
        }
        return result;
    }

    public static List<RequestToRecipe> findRequestForDoctor(String login, String status) throws DAOException {
        ConnectionPool connectionPool = ConnectionPool.getInstance();
        List<RequestToRecipe> requestToRecipeList = new ArrayList<>();
        try (Connection connection = connectionPool.getConnection();
             PreparedStatement statement = connection.prepareStatement(FIND_PROCESSING_REQUEST_DOCTOR)
        ) {
            statement.setString(1, login);
            statement.setString(2, status);
            ResultSet rs = statement.executeQuery();

            while (rs.next()) {
                RequestToRecipe requestToRecipe = new RequestToRecipe();
                requestToRecipe.setId(rs.getInt("id"));
                requestToRecipe.setIdRecipe(rs.getInt("recipeId"));
                requestToRecipe.setDate(rs.getDate("dateOfRecipe"));
                requestToRecipe.setNamePreparation(rs.getString("name_preparation"));
                requestToRecipe.setNameOfClient(rs.getString("clientData"));
                requestToRecipeList.add(requestToRecipe);
            }

        } catch (SQLException e) {
            throw new DAOException(e);
        }
        return requestToRecipeList;
    }


}
