package by.bsuir.pharma.dao.implementation;

import by.bsuir.pharma.dao.AbstractDAO;
import by.bsuir.pharma.dao.exception.DAOException;
import by.bsuir.pharma.entity.Role;
import by.bsuir.pharma.entity.User;
import by.bsuir.pharma.pool.ConnectionPool;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Иван on 08.04.2016.
 */
public class UserDAO extends AbstractDAO<User, String> {
    private final static String SELECT_ALL_QUERY = "SELECT \n" +
            "    `login`,\n" +
            "    `password`,\n" +
            "    `name`,\n" +
            "    `surname`,\n" +
            "    `role`,\n" +
            "    `phone_number`,\n" +
            "    `email`\n" +
            "FROM\n" +
            "    pharmacy.`user`\n" +
            "WHERE\n" +
            "    `role` = 'DOCTOR'\n" +
            "        OR `role` = 'PHARMACIST';";
    private final static String INSERT_QUERY = "INSERT INTO pharmacy.user (login, password, phone_number, email, name, surname) \n" +
            "VALUES (?, ?, ?, ?, ?, ?);";

    private final static String SELECT_QUERY = "SELECT login, password, name, surname, phone_number, email, role FROM pharmacy.user WHERE login = ? AND `role`<> 'EMPTY'";
    private final static String UPDATE_QUERY = "UPDATE pharmacy.user SET password = ?, name = ?, surname = ?, phone_number = ?, email = ?  WHERE login= ?;";
    private final static String DELETE_QUERY = "UPDATE  pharmacy.`user` SET `role` = 'EMPTY' WHERE `login` = ?;";
    private final static String INSERT_WORKER = "INSERT INTO pharmacy.user (login, password, phone_number, email, name, surname, role) \n" +
            "VALUES (?, ?, ?, ?, ?, ?,?);";
    private final static String ADD_ORDER_FOR_BASKET = "INSERT INTO pharmacy.`order` (`idOrder`, `User_login`) VALUES ('0',?)";


    @Override
    public String getSelectALLQuery() {

        return SELECT_ALL_QUERY;
    }

    @Override
    public String getCreateQuery() {
        return INSERT_QUERY;
    }

    @Override
    public String getSelectQuery() {
        return SELECT_QUERY;
    }

    @Override
    public String getUpdateQuery() {
        return UPDATE_QUERY;
    }

    @Override
    public String getDeleteQuery() {

        return DELETE_QUERY;
    }

    @Override
    protected void prepareStatementForInsert(PreparedStatement statement, User object) throws DAOException {
        try {
            statement.setString(1, object.getLogin());
            statement.setString(2, object.getPassword());
            statement.setString(3, object.getPhoneNumber());
            statement.setString(4, object.getEmail());
            statement.setString(5, object.getName());
            statement.setString(6, object.getSurname());
        } catch (SQLException e) {
            throw new DAOException(e);
        }
    }

    @Override
    protected void prepareStatementForUpdate(PreparedStatement statement, User object) throws DAOException {
        try {
            statement.setString(1, object.getPassword());
            statement.setString(2, object.getName());
            statement.setString(3, object.getSurname());
            statement.setString(4, object.getPhoneNumber());
            statement.setString(5, object.getEmail());
            statement.setString(6, object.getLogin());
        } catch (SQLException e) {
            throw new DAOException(e);
        }
    }

    @Override
    protected void prepareStatementForDelete(PreparedStatement statement, User object) throws DAOException {
        try {
            statement.setString(1, object.getLogin());
        } catch (SQLException e) {
            throw new DAOException(e);
        }
    }

    //@Override
    public User add(User user) throws DAOException {
        String sql = getCreateQuery();
        ConnectionPool connectionPool = ConnectionPool.getInstance();
        try (Connection connection = connectionPool.getConnection();) {
            try (
                    PreparedStatement statement = connection.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);
                    PreparedStatement basketStatement = connection.prepareStatement(ADD_ORDER_FOR_BASKET)
            ) {
                connection.setAutoCommit(false);
                statement.setString(1, user.getLogin());
                statement.setString(2, user.getPassword());
                statement.setString(3, user.getPhoneNumber());
                statement.setString(4, user.getEmail());
                statement.setString(5, user.getName());
                statement.setString(6, user.getSurname());
                basketStatement.setString(1, user.getLogin());

                statement.executeUpdate();
                basketStatement.executeUpdate();

                connection.commit();
                connection.setAutoCommit(true);
            } catch (SQLException e) {
                connection.rollback();
                connection.setAutoCommit(true);
                throw new DAOException(e);
            }
        } catch (SQLException e) {
            throw new DAOException(e);
        }

        return user;

    }

    public void addWorker(User user) throws DAOException {

        ConnectionPool connectionPool = ConnectionPool.getInstance();
        try (Connection connection = connectionPool.getConnection();
             PreparedStatement statement = connection.prepareStatement(INSERT_WORKER, Statement.RETURN_GENERATED_KEYS);
        ) {
            statement.setString(1, user.getLogin());
            statement.setString(2, user.getPassword());
            statement.setString(3, user.getPhoneNumber());
            statement.setString(4, user.getEmail());
            statement.setString(5, user.getName());
            statement.setString(6, user.getSurname());
            statement.setString(7, user.getRole().getValue().toUpperCase());
            statement.executeUpdate();
        } catch (SQLException e) {
            throw new DAOException(e);
        }
    }

    @Override
    protected List<User> parseResultSet(ResultSet rs) throws DAOException {
        List<User> users = new ArrayList<User>();
        try {
            while (rs.next()) {
                User user = new User();
                user.setLogin(rs.getString("login"));
                user.setPassword(rs.getString("password"));
                user.setName(rs.getString("name"));
                user.setSurname(rs.getString("surname"));
                user.setRole(Role.valueOf(rs.getString("role")));
                user.setEmail(rs.getString("email"));
                user.setPhoneNumber(rs.getString("phone_number"));
                users.add(user);
            }
        } catch (SQLException e) {
            throw new DAOException(e);
        }
        return users;
    }


    public User addUser(User user, Role role) throws DAOException {
        String sql = getCreateQuery();
        ConnectionPool connectionPool = ConnectionPool.getInstance();
        try (Connection connection = connectionPool.getConnection();
             PreparedStatement statement = connection.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS)
        ) {
            statement.setString(1, user.getLogin());
            statement.setString(2, user.getPassword());
            statement.setString(3, user.getPhoneNumber());
            statement.setString(4, user.getEmail());
            statement.setString(5, user.getName());
            statement.setString(6, user.getSurname());
            statement.setString(7, role.getValue().toUpperCase());
            int count = statement.executeUpdate();
            if (count != 1) {
                throw new DAOException("On add modify more then 1 record: " + count);
            }
        } catch (SQLException e) {
            throw new DAOException(e);
        }
        return user;
    }

    public static List<User> findWorkers() throws DAOException {
        ConnectionPool connectionPool = ConnectionPool.getInstance();
        List<User> workers = new ArrayList<>();
        try (Connection connection = connectionPool.getConnection();
             PreparedStatement statement = connection.prepareStatement(SELECT_ALL_QUERY, Statement.RETURN_GENERATED_KEYS)
        ) {
            ResultSet resultSet = statement.executeQuery();
            while (resultSet.next()) {
                User user = new User();
                user.setLogin(resultSet.getString("login"));
                user.setPassword(resultSet.getString("password"));
                user.setName(resultSet.getString("name"));
                user.setSurname(resultSet.getString("surname"));
                user.setRole(Role.valueOf(resultSet.getString("role")));
                user.setPhoneNumber(resultSet.getString("phone_number"));
                user.setEmail(resultSet.getString("email"));
                workers.add(user);
            }

        } catch (SQLException e) {
            throw new DAOException(e);
        }
        return workers;
    }

}

