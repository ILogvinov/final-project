package by.bsuir.pharma.dao.implementation;

import by.bsuir.pharma.dao.AbstractDAO;
import by.bsuir.pharma.dao.exception.DAOException;
import by.bsuir.pharma.entity.Basket;
import by.bsuir.pharma.entity.Recipe;
import by.bsuir.pharma.pool.ConnectionPool;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Иван on 26.05.2016.
 */
public class RecipeDAO extends AbstractDAO<Recipe, Integer> {
    private final static String INSERT_QUERY = "INSERT INTO pharmacy.`recipe` (`recipeId`,`client_login`, `doctor_login`, `start_date`, `finish_date`, `name_preparation`, `dosage`, `amount_preparation`,`discount`,`status`) VALUES (?, ?, ?, CURDATE(), ?, ?, ?, ?, ?,'ACTIVE');";
    private final static String UPDATE_STATUS_QUERY = "UPDATE `pharmacy`.`recipe` SET `status`=? WHERE `recipeId`=?;";
    private final static String CONTINUE_RECIPE_QUERY = "UPDATE `pharmacy`.`recipe` SET `status`=?, `finish_date`=? WHERE `recipeId`=?;";
    private final static String FIND_RECIPE_BU_ID_QUERY = "SELECT `recipeId`, `client_login`, `doctor_login`, `start_date`, `finish_date`, `name_preparation`, `dosage`, `amount_preparation`, `status`, `discount` FROM pharmacy.`recipe` WHERE `recipeId` = ?";
    private final static String FIND_LIST_RECIPE_CLIENT_QUERY = "SELECT `recipeId`,`discount`, `start_date`, `finish_date`, `status`, CONCAT(`name`,' ', `surname`) as doctorName, `name_preparation` \n" +
            "FROM pharmacy.`recipe` join pharmacy.`user` on `doctor_login` = `login` WHERE `client_login` = ?;";
    private final static String ATTACH_RECIPE_TO_PRODUCT = "UPDATE basket \n" +
            "SET \n" +
            "    basket.`recipe_recipeId` = (SELECT CASE WHEN `pr1` = `pr2` THEN ? ELSE basket.recipe_recipeId END" +
            "    FROM (" +
            "  SELECT `pr1`, `pr2` FROM" +
            "    (SELECT " +
            "                preparation.`name` as `pr1`" +
            "            FROM" +
            "                pharmacy.`product`" +
            "                    JOIN" +
            "                pharmacy.`preparation` ON preparation.idNumber = product.Preparation_idNumber" +
            "                    JOIN" +
            "                pharmacy.`basket` ON product.lot_number = basket.Product_lot_number" +
            "            WHERE" +
            "                basket.`order_idOrder` = ?" +
            "                    AND basket.Product_lot_number = ?" +
            "                    AND basket.order_User_login = ?) AS tmp1," +
            "    (SELECT " +
            "                `name_preparation` as `pr2`" +
            "            FROM" +
            "                pharmacy.`recipe`" +
            "            WHERE" +
            "                recipe.recipeId = ?) as tmp2" +
            ") AS tmp3" +
            "  ) where basket.Product_lot_number = ? AND basket.order_idOrder = ? AND basket.order_User_login = ?;\n";



    @Override
    public String getSelectALLQuery() {
        throw new UnsupportedOperationException();
    }

    @Override
    public String getCreateQuery() {
        return INSERT_QUERY;
    }

    @Override
    public String getSelectQuery() {
        throw new UnsupportedOperationException();
    }

    @Override
    public String getUpdateQuery() {
        throw new UnsupportedOperationException();
    }

    @Override
    public String getDeleteQuery() {
        throw new UnsupportedOperationException();
    }

    @Override
    protected void prepareStatementForInsert(PreparedStatement statement, Recipe object) throws DAOException {
        try {
            statement.setInt(1, object.getId());
            statement.setString(2, object.getClientLogin());
            statement.setString(3, object.getDoctorLogin());
            statement.setDate(4, object.getFinishDate());
            statement.setString(5, object.getNameOfPreparation());
            statement.setFloat(6, object.getDosage());
            statement.setFloat(7, object.getAmount());
            statement.setFloat(8, object.getDiscount());
        } catch (SQLException e) {
            throw new DAOException(e);
        }


    }

    @Override
    protected void prepareStatementForUpdate(PreparedStatement statement, Recipe object) throws DAOException {
        throw new UnsupportedOperationException();
    }

    @Override
    protected void prepareStatementForDelete(PreparedStatement statement, Recipe object) throws DAOException {
        throw new UnsupportedOperationException();
    }

    @Override
    protected List<Recipe> parseResultSet(ResultSet rs) throws DAOException {
        throw new UnsupportedOperationException();
    }

    public static boolean updateStatusRecipe(String status, Integer id) throws DAOException {
        ConnectionPool connectionPool = ConnectionPool.getInstance();

        try (Connection connection = connectionPool.getConnection();
             PreparedStatement statement = connection.prepareStatement(UPDATE_STATUS_QUERY)
        ) {
            statement.setString(1, status);
            statement.setInt(2, id);
            int countOfUpdateRow = statement.executeUpdate();
            if (countOfUpdateRow != 1) {
                throw new DAOException("error of update status");
            }
        } catch (SQLException e) {
            throw new DAOException(e);
        }
        return true;
    }

    public static boolean continueOfRecipe(Date date, Integer id) throws DAOException {
        ConnectionPool connectionPool = ConnectionPool.getInstance();

        try (Connection connection = connectionPool.getConnection();
             PreparedStatement statement = connection.prepareStatement(CONTINUE_RECIPE_QUERY)
        ) {
            statement.setDate(1, date);
            statement.setInt(2, id);
            int countOfUpdateRow = statement.executeUpdate();
            if (countOfUpdateRow != 1) {
                throw new DAOException("error of update status");
            }
        } catch (SQLException e) {
            throw new DAOException(e);
        }
        return true;
    }

    public void add(Recipe recipe) throws DAOException {
        ConnectionPool connectionPool = ConnectionPool.getInstance();
        try (Connection connection = connectionPool.getConnection();
             PreparedStatement statement = connection.prepareStatement(INSERT_QUERY)
        ) {
            prepareStatementForInsert(statement, recipe);
            int count = statement.executeUpdate();
            if (count != 1) {
                throw new DAOException("On add modify more then 1 record: " + count);
            }

        } catch (SQLException e) {
            throw new DAOException(e);
        }
    }

    public static Recipe findRecipe(Integer id) throws DAOException {
        ConnectionPool pool = ConnectionPool.getInstance();
        Recipe recipe;
        try (Connection connection = pool.getConnection();
             PreparedStatement statement = connection.prepareStatement(FIND_RECIPE_BU_ID_QUERY)
        ) {
            statement.setInt(1, id);
            ResultSet resultSet = statement.executeQuery();
            if (resultSet.next()) {
                recipe = new Recipe();
                recipe.setId(resultSet.getInt("recipeId"));
                recipe.setDoctorLogin(resultSet.getString("doctor_login"));
                recipe.setDosage(resultSet.getFloat("dosage"));
                recipe.setStatus(resultSet.getString("status"));
                recipe.setNameOfPreparation(resultSet.getString("name_preparation"));
                recipe.setFinishDate(resultSet.getDate("finish_date"));
                recipe.setStartDate(resultSet.getDate("start_date"));
                recipe.setClientLogin(resultSet.getString("client_login"));
                recipe.setDiscount(resultSet.getFloat("discount"));
                recipe.setAmount(resultSet.getFloat("amount_preparation"));
            } else {
                throw new DAOException("Error recipe not found");
            }

        } catch (SQLException e) {
            throw new DAOException(e);
        }
        return recipe;
    }

    public static List<Recipe> findClientRecipe(String login) throws DAOException {
        ConnectionPool pool = ConnectionPool.getInstance();
        List<Recipe> recipes = new ArrayList<>();
        try (Connection connection = pool.getConnection();
             PreparedStatement statement = connection.prepareStatement(FIND_LIST_RECIPE_CLIENT_QUERY)
        ) {
            statement.setString(1,login);
            ResultSet resultSet = statement.executeQuery();
            while (resultSet.next()){
                Recipe recipe = new Recipe();
                recipe.setId(resultSet.getInt("recipeId"));
                recipe.setStartDate(resultSet.getDate("start_date"));
                recipe.setFinishDate(resultSet.getDate("finish_date"));
                recipe.setDiscount(resultSet.getFloat("discount"));
                recipe.setDoctorName(resultSet.getString("doctorName"));
                recipe.setNameOfPreparation(resultSet.getString("name_preparation"));
                recipe.setStatus(resultSet.getString("status"));
                recipes.add(recipe);
            }

        } catch (SQLException e) {
            throw new DAOException(e);
        }
        return recipes;
    }

    public static boolean attachRecipeToProduct(Basket basket) throws DAOException {
        boolean result = false;
        ConnectionPool pool = ConnectionPool.getInstance();
        try (Connection connection = pool.getConnection();
             PreparedStatement statement = connection.prepareStatement(ATTACH_RECIPE_TO_PRODUCT)
        ) {
            statement.setInt(1,basket.getIdRecipe());
            statement.setInt(2,basket.getOrder());
            statement.setInt(3,basket.getProductNumber());
            statement.setString(4,basket.getLoginUser());
            statement.setInt(5,basket.getIdRecipe());
            statement.setInt(6,basket.getProductNumber());
            statement.setInt(7,basket.getOrder());
            statement.setString(8,basket.getLoginUser());
            int count = statement.executeUpdate();
            if(count==1){
                result = true;
            }
        } catch (SQLException e) {
            throw new DAOException(e);
        }
        return result;
    }
}
