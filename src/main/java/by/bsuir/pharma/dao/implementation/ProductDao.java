package by.bsuir.pharma.dao.implementation;

import by.bsuir.pharma.dao.AbstractDAO;
import by.bsuir.pharma.dao.exception.DAOException;
import by.bsuir.pharma.entity.Preparation;
import by.bsuir.pharma.entity.Product;
import by.bsuir.pharma.pool.ConnectionPool;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Иван on 13.05.2016.
 */
public class ProductDao extends AbstractDAO<Product,Integer> {
    private final static String INSERT_PRODUCT_QUERY = "INSERT INTO `pharmacy`.`product` (`lot_number`, `countOnSparePart`, `dateConsigment`, `end_shelf_life`, `price`, `Preparation_idNumber`) VALUES (?, ?, CURDATE(), ?, ?, ?);";

    private final static String UPDATE_COUNT_PRODUCT_QUERY = "UPDATE pharmacy.`product` set countOnSparePart = (countOnSparePart - ?) where lot_number = ?;";

    private final static String CHANGE_COUNT_PRODUCT = "UPDATE pharmacy.`product` set countOnSparePart = ? where lot_number = ?;";

    private final static String SELECT_QUERY_BY_KEY = "SELECT `lot_number`, `countOnSparePart`, `dateConsigment`, `end_shelf_life`, `price`, `Preparation_idNumber` FROM pharmacy.`product` WHERE `lot_number` = ?;";

    private static final String DELETE_QUERY = "UPDATE  `pharmacy`.`product` SET `countOnSparePart` = 0 WHERE `lot_number`=?;";

    private static final String SELECT_ALL_QUERY = "SELECT `name`, `dosage`, `idNumber`, `imageName`, `price`, `countOnSparePart`, `count_units`, `lot_number`,`end_shelf_life` FROM pharmacy.preparation join pharmacy.product " +
            "on `idNumber` = `Preparation_idNumber` WHERE `countOnSparePart`>0";
    @Override
    public String getSelectALLQuery() {
        throw new UnsupportedOperationException();
    }

    @Override
    public String getCreateQuery() {
        throw new UnsupportedOperationException();
    }

    @Override
    public String getSelectQuery() {
        return SELECT_QUERY_BY_KEY;
    }

    @Override
    public String getUpdateQuery() {
        throw new UnsupportedOperationException();
    }

    @Override
    public String getDeleteQuery() {
        return DELETE_QUERY;
    }

    @Override
    protected void prepareStatementForInsert(PreparedStatement statement, Product object) throws DAOException {
        try {
            statement.setInt(1,object.getLotNumber());
            statement.setInt(2,object.getCountOnSparePart());
            statement.setDate(3,object.getEndShelfLife());
            statement.setFloat(4,object.getPrice());
            statement.setString(5,object.getPreparationId());
        } catch (SQLException e) {
            throw new DAOException("Error of prepare statement for insert product",e);
        }

    }

    @Override
    protected void prepareStatementForUpdate(PreparedStatement statement, Product object) throws DAOException {
        throw new UnsupportedOperationException();
    }

    @Override
    protected void prepareStatementForDelete(PreparedStatement statement, Product object) throws DAOException {
        try {
            statement.setInt(1,object.getLotNumber());
        } catch (SQLException e) {
            throw new DAOException(e);
        }
    }

    @Override
    protected List<Product> parseResultSet(ResultSet rs) throws DAOException {
        List<Product> products = new ArrayList<>();
        try {
            while(rs.next()){
                Product product = new Product();
                product.setLotNumber(rs.getInt("lot_number"));
                product.setCountOnSparePart(rs.getInt("countOnSparepart"));
                product.setDateConsigment(rs.getDate("dateConsigment"));
                product.setEndShelfLife(rs.getDate("end_shelf_life"));
                product.setPrice(rs.getFloat("price"));
                product.setPreparationId(rs.getString("Preparation_idNumber"));
                products.add(product);

            }
        } catch (SQLException e) {
            throw new DAOException(e);
        }
    return products;
    }

    public  boolean addProduct(Product product) throws DAOException {
        ConnectionPool connectionPool = ConnectionPool.getInstance();

        try (Connection connection = connectionPool.getConnection();
             PreparedStatement statement = connection.prepareStatement(INSERT_PRODUCT_QUERY)
        ) {
            prepareStatementForInsert(statement,product);
            statement.executeUpdate();
        } catch (SQLException e) {
            throw new DAOException(e);
        }
        return true;
    }


    public static boolean changeCountOfProduct(Integer lotNumber, Float count) throws  DAOException{
        ConnectionPool connectionPool = ConnectionPool.getInstance();

        try (Connection connection = connectionPool.getConnection();
             PreparedStatement statement = connection.prepareStatement(CHANGE_COUNT_PRODUCT)
        ) {
            statement.setFloat(1,count);
            statement.setInt(2,lotNumber);
            statement.executeUpdate();
        } catch (SQLException e) {
            throw new DAOException(e);
        }
        return true;
    }

    public static List<Preparation> findAll() throws DAOException {
        ConnectionPool connectionPool = ConnectionPool.getInstance();
        List<Preparation> preparations = new ArrayList<>();
        try (Connection connection = connectionPool.getConnection();
             PreparedStatement statement = connection.prepareStatement(SELECT_ALL_QUERY)
        ) {
            ResultSet rs = statement.executeQuery();

            while (rs.next()){
                Preparation preparation = new Preparation();
                preparation.setName(rs.getString("name"));
                preparation.setDosage(rs.getFloat("dosage"));
                preparation.setId(rs.getString("idNumber"));
                preparation.setImageName(rs.getString("imageName"));
                preparation.setPrice(rs.getFloat("price"));
                preparation.setCount(rs.getFloat("countOnSparePart"));
                preparation.setCountUnits(rs.getFloat("count_units"));
                preparation.setLotNumber(rs.getInt("lot_number"));
                preparation.setEndShelfLife(rs.getDate("end_shelf_life"));
                preparations.add(preparation);
            }
        } catch (SQLException e) {
            throw new DAOException(e);
        }
        return preparations;
    }

}
