package by.bsuir.pharma.dao.implementation;

import by.bsuir.pharma.dao.AbstractDAO;
import by.bsuir.pharma.dao.exception.DAOException;
import by.bsuir.pharma.entity.Order;
import by.bsuir.pharma.pool.ConnectionPool;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Иван on 10.05.2016.
 */
public class OrderDAO extends AbstractDAO<Order, Integer> {

    private final static String ADD_ORDER_FOR_BASKET = "INSERT INTO pharmacy.`order` (`idOrder`, `User_login`) VALUES ('0',?)";
    private final static String INSERT_ORDER_QUERY =
            "INSERT INTO pharmacy.`order` (`dateOrder`, `paymentMethod`, `User_login`, `address`, `status`) VALUES (curdate(), ?, ?, ?, 'PROCESSING');";

    private final static String UPDATE_BASKET_ORDER_QUERY = "UPDATE pharmacy.`basket`,\n" +
            "    pharmacy.`recipe` \n" +
            "SET \n" +
            "    basket.`order_idOrder` = LAST_INSERT_ID(),\n" +
            "    recipe.`status` = 'FINISH'\n" +
            "WHERE\n" +
            "    `recipeId` = `recipe_recipeId`\n" +
            "\t AND `order_User_login` = ?\n" +
            "\t AND `order_idOrder` = 0;";

    private final static String UPDATE_SPARE_PART_AND_STATUS = "UPDATE pharmacy.`product`" +
            "        JOIN" +
            "    pharmacy.`basket` ON lot_number = Product_lot_number" +
            "        JOIN" +
            "    pharmacy.`order` ON idOrder = order_idOrder" +
            "        JOIN" +
            "    pharmacy.`recipe` ON recipeId = recipe_recipeId " +
            "SET " +
            "    countOnSparePart = CASE" +
            "        WHEN 'DELIVERED' = ? THEN (countOnSparePart - `count`)" +
            "        WHEN 'CANCEL' = ? THEN (countOnSparePart + `count`)" +
            "        ELSE countOnSparePart" +
            "    END," +
            "    `order`.`status` = ?," +
            "    recipe.`status` = IF('REJECTED' = ?" +
            "            OR 'CANCEL' = ?," +
            "        'ACTIVE'," +
            "        recipe.`status`)" +
            "WHERE" +
            "    idOrder = ?;";


    private final static String FIND_BY_USER_QUERY = "SELECT `idOrder`, `dateOrder`, `paymentMethod`, `address`, `status` FROM pharmacy.`order` WHERE `User_login` = ? AND idOrder <> 0;";

    private final static String FIND_BY_STATUS_QUERY = "SELECT `idOrder`, `dateOrder`, `paymentMethod`, `address`, `status`,`User_login` FROM pharmacy.`order` WHERE `status` = ?;";


    private final static String UPDATE_STATUS_QUERY = "UPDATE pharmacy.`order` SET `status` = ? WHERE `idOrder` = ?;";

    @Override
    public String getSelectALLQuery() {
        throw new UnsupportedOperationException();
    }

    @Override
    public String getCreateQuery() {
        return INSERT_ORDER_QUERY;
    }

    @Override
    public String getSelectQuery() {
        throw new UnsupportedOperationException();
    }

    @Override
    public String getUpdateQuery() {
        throw new UnsupportedOperationException();
    }

    @Override
    public String getDeleteQuery() {
        throw new UnsupportedOperationException();
    }

    @Override
    protected void prepareStatementForInsert(PreparedStatement statement, Order object) throws DAOException {

    }

    @Override
    protected void prepareStatementForUpdate(PreparedStatement statement, Order object) throws DAOException {

    }

    @Override
    protected void prepareStatementForDelete(PreparedStatement statement, Order object) throws DAOException {

    }

    public static void addOrderForBasket(String login) throws DAOException {
        ConnectionPool connectionPool = ConnectionPool.getInstance();
        try (Connection connection = connectionPool.getConnection();
             PreparedStatement statement = connection.prepareStatement(ADD_ORDER_FOR_BASKET, Statement.RETURN_GENERATED_KEYS)
        ) {
            statement.setString(1, login);
            statement.executeUpdate();

        } catch (SQLException e) {
            throw new DAOException(e);
        }
    }

    //@Override
    public void add(Order object) throws DAOException {
        ConnectionPool connectionPool = ConnectionPool.getInstance();
        try(Connection connection = connectionPool.getConnection()) {
            try (
                 PreparedStatement statement = connection.prepareStatement(INSERT_ORDER_QUERY);
                 PreparedStatement preparedStatementUpdate = connection.prepareStatement(UPDATE_BASKET_ORDER_QUERY)
            ) {
                connection.setAutoCommit(false);
                statement.setString(1, object.getPaymentMethod());
                statement.setString(2, object.getUserLogin());
                statement.setString(3, object.getAddress());
                preparedStatementUpdate.setString(1, object.getUserLogin());
                statement.executeUpdate();
                preparedStatementUpdate.executeUpdate();
                connection.commit();
                connection.setAutoCommit(true);
            } catch (SQLException e) {
                connection.rollback();
                connection.setAutoCommit(true);
            }
        }catch (SQLException e){
            throw new DAOException(e);
        }


    }


    public static List<Order> findOderByLogin(String userLogin) throws DAOException {
        ConnectionPool connectionPool = ConnectionPool.getInstance();
        List<Order> orders = new ArrayList<>();
        try (Connection connection = connectionPool.getConnection();
             PreparedStatement statement = connection.prepareStatement(FIND_BY_USER_QUERY, Statement.RETURN_GENERATED_KEYS)
        ) {
            statement.setString(1, userLogin);
            ResultSet resultSet = statement.executeQuery();
            while (resultSet.next()) {
                Order order = new Order();
                order.setId(resultSet.getInt("idOrder"));
                order.setDate(resultSet.getDate("dateOrder"));
                order.setPaymentMethod(resultSet.getString("paymentMethod"));
                order.setAddress(resultSet.getString("address"));
                order.setStatus(resultSet.getString("status"));
                orders.add(order);
            }

        } catch (SQLException e) {
            throw new DAOException(e);
        }
        return orders;
    }

    public static List<Order> findOderByStatus(String status) throws DAOException {
        ConnectionPool connectionPool = ConnectionPool.getInstance();
        List<Order> orders = new ArrayList<>();
        try (Connection connection = connectionPool.getConnection();
             PreparedStatement statement = connection.prepareStatement(FIND_BY_STATUS_QUERY)
        ) {
            statement.setString(1, status);
            ResultSet resultSet = statement.executeQuery();
            while (resultSet.next()) {
                Order order = new Order();
                order.setId(resultSet.getInt("idOrder"));
                order.setDate(resultSet.getDate("dateOrder"));
                order.setPaymentMethod(resultSet.getString("paymentMethod"));
                order.setAddress(resultSet.getString("address"));
                order.setStatus(resultSet.getString("status"));
                order.setUserLogin(resultSet.getString("User_login"));
                orders.add(order);
            }

        } catch (SQLException e) {
            throw new DAOException(e);
        }
        return orders;
    }


    public static boolean updateOrderStatus(Integer idOrder, String status) throws DAOException {
        ConnectionPool connectionPool = ConnectionPool.getInstance();

        try (Connection connection = connectionPool.getConnection();
             PreparedStatement statement = connection.prepareStatement(UPDATE_SPARE_PART_AND_STATUS)
        ) {
            statement.setString(1, status);
            statement.setString(2,status);
            statement.setString(3,status);
            statement.setString(4,status);
            statement.setString(5,status);
            statement.setInt(6, idOrder);
            statement.executeUpdate();

        } catch (SQLException e) {
            throw new DAOException(e);
        }
        return true;
    }

    @Override
    protected List<Order> parseResultSet(ResultSet rs) throws DAOException {
        throw new UnsupportedOperationException();
    }


}
