package by.bsuir.pharma.dao.implementation;

import by.bsuir.pharma.dao.AbstractDAO;
import by.bsuir.pharma.dao.exception.DAOException;
import by.bsuir.pharma.entity.CharacteristicPreparation;
import by.bsuir.pharma.entity.Preparation;
import by.bsuir.pharma.pool.ConnectionPool;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by Иван on 27.04.2016.
 */
public class PreparationDAO extends AbstractDAO<Preparation, String> {
    private static final String SELECT_ALL_QUERY = "SELECT `name`, `dosage`, `idNumber`, `imageName`, `price`, `countOnSparePart`, `count_units`, `lot_number` FROM pharmacy.preparation join pharmacy.product " +
            "on idNumber = Preparation_idNumber";
    private static final String SELECT_BY_NAME_QUERY = "SELECT `name`, `dosage`, `idNumber`, `imageName`, `price`, `totalCount`, `count_units`, `lot_number` FROM pharmacy.preparation JOIN pharmacy.product " +
            "ON idNumber = Preparation_idNumber WHERE `name` = ?";
    private static final String SELECT_QUERY = "SELECT `name`,`idNumber`,`group`,`manufacture`,`country`, `testimony`, `dosage`,`modeOfApplication`, `pharmachologicEffect`, `count_units`,`imageName`,`totalCount` FROM pharmacy.preparation " +
            "join (SELECT manufacturer.`name` as `manufacture`, `idManufacturer`, country.`name` as `country` " +
            " from pharmacy.`manufacturer` join pharmacy.`country` on `idCountry` = `Country_idCountry`) as buf " +
            "on `idManufacturer` = `Manufacturer_idManufacturer` where `idNumber`=?;";

    private static final String INSERT_QUERY = "INSERT INTO `pharmacy`.`preparation` (`idNumber`, `group`, `name`, `Manufacturer_idManufacturer`, `dosage`, `count_units`, `testimony`, `pharmachologicEffect`, `contraindications`, `modeOfApplication`) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?);";

    private static final String UPDATE_PREPARATION_IMAGE_QUERY = "UPDATE pharmacy.`preparation` SET `imageName` = ? WHERE preparation.`idNumber` = ?;";

    private static final String FIND_ALL_QUERY = "SELECT " +
            "    `idNumber`, preparation.`name`, `group`, `dosage`, `count_units`, manufacturer.`name` AS `manufacturer`, country.`name` AS `country` " +
            "FROM" +
            "    pharmacy.`preparation`" +
            "        JOIN" +
            "    pharmacy.`manufacturer` ON `Manufacturer_idManufacturer` = `idManufacturer`\n" +
            "        JOIN" +
            "    pharmacy.country ON `idCountry` = `Country_idCountry`;";
    private static final HashMap<CharacteristicPreparation, String> SQL_FILTERED_PARAMETERS = new HashMap<>();

    private static final String DELETE_PREPARATION_QUERY = "DELETE FROM pharmacy.`preparation` WHERE preparation.`idNumber` = ?;";

    static {
        SQL_FILTERED_PARAMETERS.put(CharacteristicPreparation.MAX_PRICE, "`price` < ?");
        SQL_FILTERED_PARAMETERS.put(CharacteristicPreparation.MIN_PRICE, "`price` > ?");
        SQL_FILTERED_PARAMETERS.put(CharacteristicPreparation.GROUP, "`group` = ?");
        SQL_FILTERED_PARAMETERS.put(CharacteristicPreparation.IS_REQUIRE_RECIPE, "`isRequiereRecipe` = ?");
    }

    @Override
    public String getSelectALLQuery() {
        return null;
    }

    @Override
    public String getCreateQuery() {
        throw new UnsupportedOperationException();
    }

    @Override
    public String getSelectQuery() {
        return SELECT_QUERY;
    }

    @Override
    public String getUpdateQuery() {
        throw new UnsupportedOperationException();
    }

    @Override
    public String getDeleteQuery() {
        throw new UnsupportedOperationException();
    }

    @Override
    protected void prepareStatementForInsert(PreparedStatement statement, Preparation object) throws DAOException {
        try {
            statement.setString(1, object.getId());
            statement.setString(2, object.getGroup());
            statement.setString(3, object.getName());
            statement.setInt(4, object.getManufactureID());
            statement.setFloat(5, object.getDosage());
            statement.setFloat(6, object.getCountUnits());
            statement.setString(7, object.getTestimony());
            statement.setString(8, object.getPharmacologicalEffect());
            statement.setString(9, object.getContraindications());
            statement.setString(10, object.getModeOfApplication());
        } catch (SQLException e) {
            throw new DAOException(e);
        }
    }

    @Override
    protected void prepareStatementForUpdate(PreparedStatement statement, Preparation object) throws DAOException {
        throw new UnsupportedOperationException();
    }

    @Override
    protected void prepareStatementForDelete(PreparedStatement statement, Preparation object) throws DAOException {
        throw new UnsupportedOperationException();
    }


    public Preparation add(Preparation object) throws DAOException {
        ConnectionPool pool = ConnectionPool.getInstance();
        try (Connection connection = pool.getConnection();
             PreparedStatement statement = connection.prepareStatement(INSERT_QUERY)
        ) {
            prepareStatementForInsert(statement, object);
            statement.executeUpdate();
        } catch (SQLException e) {
            throw new DAOException(e);
        }
        return object;
    }

    @Override
    protected List<Preparation> parseResultSet(ResultSet rs) throws DAOException {
        List<Preparation> preparations = new ArrayList<Preparation>();
        try {
            while (rs.next()) {
                Preparation preparation = new Preparation();
                preparation.setName(rs.getString("name"));
                preparation.setId(rs.getString("idNumber"));
                preparation.setImageName(rs.getString("imageName"));
                preparation.setDosage(rs.getFloat("dosage"));
                preparation.setManufacturer(rs.getString("manufacture"));
                preparation.setTestimony(rs.getString("testimony"));
                preparation.setModeOfApplication(rs.getString("modeOfApplication"));
                preparation.setPharmacologicalEffect(rs.getString("pharmachologicEffect"));
                preparation.setCountUnits(rs.getFloat("count_units"));
                preparations.add(preparation);
            }
        } catch (SQLException e) {
            throw new DAOException(e);
        }
        return preparations;
    }

    private List<Preparation> parseResultSetForCatalog(ResultSet rs) throws DAOException {
        List<Preparation> preparations = new ArrayList<Preparation>();
        try {
            while (rs.next()) {
                Preparation preparation = new Preparation();
                preparation.setName(rs.getString("name"));
                preparation.setId(rs.getString("idNumber"));
                preparation.setImageName(rs.getString("imageName"));
                preparation.setDosage(rs.getFloat("dosage"));
                preparation.setPrice(rs.getFloat("price"));
                preparation.setLotNumber(rs.getInt("lot_number"));
                preparations.add(preparation);
            }
        } catch (SQLException e) {
            throw new DAOException(e);
        }
        return preparations;
    }

    private String formFilteredRequest(Map<CharacteristicPreparation, String> characteristic) {
        String newSQL = SELECT_ALL_QUERY;
        if (characteristic.size() >= 1) {
            List<CharacteristicPreparation> keys = new ArrayList<>(characteristic.keySet());
            newSQL += " WHERE " + SQL_FILTERED_PARAMETERS.get(keys.get(0));
            for (int i = 1; i < keys.size(); i++) {
                newSQL += " AND " + SQL_FILTERED_PARAMETERS.get(keys.get(i));
            }
        }
        return newSQL;
    }

    private void preparedStatementForFilter(PreparedStatement statement, Map<CharacteristicPreparation, String> characteristic) throws DAOException {
        int index = 1;
        try {
            for (Map.Entry<CharacteristicPreparation, String> entry : characteristic.entrySet()) {
                switch (entry.getKey()) {
                    case MAX_PRICE:
                        statement.setFloat(index, Float.valueOf(entry.getValue()));
                        break;
                    case MIN_PRICE:
                        statement.setFloat(index, Float.valueOf(entry.getValue()));
                        break;
                    case IS_REQUIRE_RECIPE:
                        statement.setBoolean(index, Boolean.parseBoolean(entry.getValue()));
                        break;
                    default:
                        statement.setString(index, entry.getValue());
                        break;
                }
                index++;
            }
        } catch (SQLException e) {
            throw new DAOException(e);
        }
    }

    public List<Preparation> findConcretePreparations(Map<CharacteristicPreparation, String> characteristic) throws DAOException {
        List<Preparation> preparations = null;
        String sql = formFilteredRequest(characteristic);
        ConnectionPool pool = ConnectionPool.getInstance();
        try (Connection connection = pool.getConnection();
             PreparedStatement statement = connection.prepareStatement(sql)
        ) {
            preparedStatementForFilter(statement, characteristic);
            ResultSet rs = statement.executeQuery();
            preparations = parseResultSetForCatalog(rs);
        } catch (SQLException e) {
            throw new DAOException(e);
        }
        return preparations;

    }

    public List<Preparation> findByName(String namePreparation) throws DAOException {
        List<Preparation> list = null;
        String sql = SELECT_BY_NAME_QUERY;
        ConnectionPool pool = ConnectionPool.getInstance();
        try (Connection connection = pool.getConnection();
             PreparedStatement statement = connection.prepareStatement(sql)
        ) {
            statement.setString(1, namePreparation);
            ResultSet rs = statement.executeQuery();
            list = parseResultSetForCatalog(rs);
        } catch (SQLException e) {
            throw new DAOException(e);
        }
        return list;
    }

    public static boolean addImageName(String id, String namePreparation) throws DAOException {
        boolean result = false;
        ConnectionPool pool = ConnectionPool.getInstance();
        try (Connection connection = pool.getConnection();
             PreparedStatement statement = connection.prepareStatement(UPDATE_PREPARATION_IMAGE_QUERY)
        ) {
            statement.setString(1, namePreparation);
            statement.setString(2, id);
            int count = statement.executeUpdate();
            if (count == 1) {
                result = true;
            }
        } catch (SQLException e) {
            throw new DAOException(e);
        }
        return result;
    }

    public static List<Preparation> findAll() throws DAOException {
        ConnectionPool pool = ConnectionPool.getInstance();
        List<Preparation> preparations = new ArrayList<>();
        try (Connection connection = pool.getConnection();
             PreparedStatement statement = connection.prepareStatement(FIND_ALL_QUERY)
        ) {
            ResultSet resultSet = statement.executeQuery();
            while (resultSet.next()) {
                Preparation preparation = new Preparation();
                preparation.setId(resultSet.getString("idNumber"));
                preparation.setName(resultSet.getString("name"));
                preparation.setGroup(resultSet.getString("group"));
                preparation.setDosage(resultSet.getFloat("dosage"));
                preparation.setCountUnits(resultSet.getFloat("count_units"));
                preparation.setManufacturer(resultSet.getString("manufacturer"));
                preparation.setCountry(resultSet.getString("country"));
                preparations.add(preparation);
            }
        } catch (SQLException e) {
            throw new DAOException(e);
        }
        return preparations;
    }

    public static boolean deletePreparation(String id) throws DAOException {
        boolean result = false;
        ConnectionPool pool = ConnectionPool.getInstance();
        try (Connection connection = pool.getConnection();
             PreparedStatement statement = connection.prepareStatement(DELETE_PREPARATION_QUERY)
        ) {
            statement.setString(1, id);
            int count = statement.executeUpdate();
            if (count == 1) {
                result = true;
            }
        } catch (SQLException e) {
            throw new DAOException(e);
        }
        return result;
    }

}
