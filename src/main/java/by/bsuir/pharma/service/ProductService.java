package by.bsuir.pharma.service;

import by.bsuir.pharma.dao.exception.DAOException;
import by.bsuir.pharma.dao.implementation.ProductDao;
import by.bsuir.pharma.entity.Preparation;
import by.bsuir.pharma.entity.Product;

import java.util.List;

/**
 * Created by Иван on 14.05.2016.
 */
public class ProductService {
    public static boolean addNewProduct(Product product) throws ServiceException {
        ProductDao dao = new ProductDao();

        try {
            dao.addProduct(product);
        } catch (DAOException e) {
            throw new ServiceException(e);
        }
        return true;
    }

    public static boolean  updateCountPreparation(Integer lot, Float newValue) throws ServiceException {
        try {
            ProductDao.changeCountOfProduct(lot, newValue);
        } catch (DAOException e) {
            throw new ServiceException(e);
        }
        return true;
    }

    public static boolean removeProduct(Integer lotNumber) throws ServiceException {
        ProductDao dao = new ProductDao();
        Product p = new Product();
        p.setLotNumber(lotNumber);
        try {
            dao.delete(p);
        } catch (DAOException e) {
            throw new ServiceException(e);
        }
        return true;
    }

    public static List<Preparation> giveCatalogOfPreparation() throws ServiceException {
        try {
            return ProductDao.findAll();
        } catch (DAOException e) {
            throw new ServiceException(e);
        }
    }
}
