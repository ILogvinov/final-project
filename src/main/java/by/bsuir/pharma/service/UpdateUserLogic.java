package by.bsuir.pharma.service;

import by.bsuir.pharma.dao.exception.DAOException;
import by.bsuir.pharma.dao.implementation.UserDAO;
import by.bsuir.pharma.entity.User;
import org.apache.commons.codec.digest.DigestUtils;

/**
 * Created by Иван on 25.04.2016.
 */
public class UpdateUserLogic {

    public static boolean changeUserData(User user) throws ServiceException{
        user.setPassword(DigestUtils.md5Hex(user.getPassword()));
        UserDAO dao = new UserDAO();
        try {
            dao.update(user);
            return true;
        } catch (DAOException e) {
            throw new ServiceException(e);
        }
    }
}
