package by.bsuir.pharma.service;

import by.bsuir.pharma.dao.exception.DAOException;
import by.bsuir.pharma.dao.implementation.UserDAO;
import by.bsuir.pharma.entity.User;

import java.util.List;

/**
 * Created by Иван on 13.05.2016.
 */
public class WorkerService {

    public static boolean deleteUser(String login) throws ServiceException {
        User user = new User();
        user.setLogin(login);
        UserDAO dao = new UserDAO();
        try {
            dao.delete(user);
        } catch (DAOException e) {
            throw new ServiceException(e);
        }
        return true;
    }

    public static List<User> findWorkers() throws ServiceException {
        try {
            return UserDAO.findWorkers();
        } catch (DAOException e) {
            throw new ServiceException(e);
        }
    }
}
