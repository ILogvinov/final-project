package by.bsuir.pharma.service;

import by.bsuir.pharma.dao.AbstractDAO;
import by.bsuir.pharma.dao.exception.DAOException;
import by.bsuir.pharma.dao.implementation.UserDAO;
import by.bsuir.pharma.entity.User;
import org.apache.commons.codec.digest.DigestUtils;

/**
 * Created by Иван on 27.03.2016.
 */
public class LoginService {


    public static User loginUser(User user) throws ServiceException {

        AbstractDAO dao = new UserDAO();
        User registeredUser;
        try {
            registeredUser = (User) dao.findByPK(user.getLogin());
            String hashPassword = DigestUtils.md5Hex(user.getPassword());
            if (registeredUser != null && hashPassword.equals(registeredUser.getPassword())) {
                return registeredUser;
            } else {
                throw new ServiceException("Invalid data");
            }

        } catch (DAOException e) {
            throw new ServiceException(e);
        }
    }
}
