package by.bsuir.pharma.service;

import by.bsuir.pharma.dao.exception.DAOException;
import by.bsuir.pharma.dao.implementation.UserDAO;
import by.bsuir.pharma.entity.User;
import org.apache.commons.codec.digest.DigestUtils;

/**
 * Created by Иван on 10.04.2016.
 */
public class RegistrateService {


    public static boolean checkData(User user) throws ServiceException {

        user.setPassword(DigestUtils.md5Hex(user.getPassword()));

        UserDAO dao = new UserDAO();
        try {
            dao.add(user);
            return true;
        } catch (DAOException e) {
            throw new ServiceException(e);
        }


    }


    public static boolean registreNewUser(User user) throws ServiceException {

        user.setPassword(DigestUtils.md5Hex(user.getPassword()));

        UserDAO dao = new UserDAO();
        try {
            if (user.getRole() == null) {
                dao.addUser(user, user.getRole());
            } else {
                dao.addWorker(user);
            }
            return true;
        } catch (DAOException e) {
            throw new ServiceException(e);
        }


    }
}
