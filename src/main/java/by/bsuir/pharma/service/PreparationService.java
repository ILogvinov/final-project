package by.bsuir.pharma.service;

import by.bsuir.pharma.dao.exception.DAOException;
import by.bsuir.pharma.dao.implementation.PreparationDAO;
import by.bsuir.pharma.entity.CharacteristicPreparation;
import by.bsuir.pharma.entity.Preparation;

import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

/**
 * Created by Иван on 27.04.2016.
 */
public class PreparationService {

    public List<Preparation> giveCatalogOfPreparation() throws ServiceException {
        PreparationDAO dao = new PreparationDAO();
        List<Preparation> preparations;
        try {
            preparations = dao.findAll();
        } catch (DAOException e) {
            throw new ServiceException(e);
        }
        return preparations;
    }

    public List<Preparation> giveFilteredPreparation(Map<CharacteristicPreparation, String> characteristic) throws ServiceException {
        if (PreparationCharacteristicValidator.checkParameters(characteristic)) {
            PreparationDAO dao = new PreparationDAO();
            try {
                return dao.findConcretePreparations(characteristic);
            } catch (DAOException e) {
                throw new ServiceException(e);
            }
        } else {
            throw new ServiceException("Invalidate param of search");
        }

    }

    public static boolean addNewPreparation(Preparation preparation) throws ServiceException {
        PreparationDAO dao = new PreparationDAO();
        try {
            dao.add(preparation);
        } catch (DAOException e) {
            throw new ServiceException(e);
        }
        return true;
    }

    public List<Preparation> givePreparationByName(String name) throws ServiceException {
        PreparationDAO dao = new PreparationDAO();
        try {
            return dao.findByName(name);
        } catch (DAOException e) {
            throw new ServiceException(e);
        }
    }


    private static class PreparationCharacteristicValidator {
        private static String PRICE_REGEX = "\\d+(.\\d)?";

        private static boolean checkPricesCharacteristic(String maxPriceStr, String minPriceStr) {

            Pattern pattern = Pattern.compile(PRICE_REGEX);
            Matcher matcherMinPrice = pattern.matcher(minPriceStr);
            Matcher matcherMaxPrice = pattern.matcher(maxPriceStr);
            Float minPrice;
            Float maxPrice;
            if (matcherMaxPrice.matches() && matcherMinPrice.matches()) {
                minPrice = Float.valueOf(minPriceStr);
                maxPrice = Float.valueOf(maxPriceStr);
                return ((maxPrice - minPrice) > 0.000001);
            } else {
                return false;
            }

        }

        private static boolean checkPriceCharacteristic(String priceStr) {
            Pattern pattern = Pattern.compile(PRICE_REGEX);
            Matcher matcherMinPrice = pattern.matcher(priceStr);
            return matcherMinPrice.matches();
        }


        private static boolean checkParameters(Map<CharacteristicPreparation, String> characteristicPreparation) {
            Map<CharacteristicPreparation, String> tmp = characteristicPreparation.entrySet().stream().filter(map -> !("".equals(map.getValue()) || map.getValue() == null)).collect(Collectors.toMap(p -> p.getKey(), p -> p.getValue()));
            characteristicPreparation.keySet().removeAll(characteristicPreparation.keySet());
            characteristicPreparation.putAll(tmp);

            if (characteristicPreparation.containsKey(CharacteristicPreparation.MAX_PRICE) && characteristicPreparation.containsKey(CharacteristicPreparation.MIN_PRICE)) {
                return checkPricesCharacteristic(characteristicPreparation.get(CharacteristicPreparation.MAX_PRICE), characteristicPreparation.get(CharacteristicPreparation.MIN_PRICE));
            } else {
                if (characteristicPreparation.containsKey(CharacteristicPreparation.MIN_PRICE)) {
                    return checkPriceCharacteristic(characteristicPreparation.get(CharacteristicPreparation.MIN_PRICE));
                } else if (characteristicPreparation.containsKey(CharacteristicPreparation.MAX_PRICE)) {
                    return checkPriceCharacteristic(characteristicPreparation.get(CharacteristicPreparation.MAX_PRICE));
                }
            }
            return true;
        }

    }

    public Preparation findById(String id) throws ServiceException {
        PreparationDAO dao = new PreparationDAO();
        try {
            return dao.findByPK(id);
        } catch (DAOException e) {
            throw new ServiceException(e);
        }
    }

    public static boolean addNewPreparationImage(String id, String imageName) throws ServiceException {
        try {
            return PreparationDAO.addImageName(id,imageName);
        } catch (DAOException e) {
            throw new ServiceException(e);
        }
    }

    public static List<Preparation> findAll() throws ServiceException {
        try {
            return PreparationDAO.findAll();
        } catch (DAOException e) {
            throw new ServiceException(e);
        }
    }

    public static boolean deletePreparation(String idPreparation) throws ServiceException {
        try {
            return PreparationDAO.deletePreparation(idPreparation);
        } catch (DAOException e) {
            throw new ServiceException(e);
        }
    }




}


