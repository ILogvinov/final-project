package by.bsuir.pharma.service;

import by.bsuir.pharma.dao.exception.DAOException;
import by.bsuir.pharma.dao.implementation.RequestToRecipeDAO;
import by.bsuir.pharma.entity.RequestToRecipe;

import java.sql.Date;
import java.util.List;

/**
 * Created by Иван on 31.05.2016.
 */
public class RequestToRecipeService {
    public boolean registryNewRequest(Integer idRecipe) throws ServiceException {
        RequestToRecipeDAO requestToRecipeDAO = new RequestToRecipeDAO();
        boolean result;
        try {
            result = requestToRecipeDAO.add(idRecipe);
        } catch (DAOException e) {
            throw new ServiceException(e);
        }
        return result;
    }

    public boolean rejectRequest(Integer idRequest) throws ServiceException {
        try {
            RequestToRecipeDAO.rejectRequest(idRequest);
        } catch (DAOException e) {
            throw new ServiceException(e);
        }
        return true;
    }

    public boolean approveRequest(Integer idRequest, Date newFinishDate) throws ServiceException {
        try {
            RequestToRecipeDAO.extendRecipe(idRequest,newFinishDate);
        } catch (DAOException e) {
            throw new ServiceException(e);
        }
        return true;
    }

    public boolean deleteRequest(RequestToRecipe requestToRecipe) throws ServiceException {
        RequestToRecipeDAO dao = new RequestToRecipeDAO();
        try {
            dao.delete(requestToRecipe);
        } catch (DAOException e) {
            throw new ServiceException(e);
        }
        return true;
    }

    public List<RequestToRecipe> giveDoctorRequests(String doctorLogin, String status) throws ServiceException {
        RequestToRecipeDAO dao = new RequestToRecipeDAO();
        List<RequestToRecipe> requestToRecipeList;
        try {
            requestToRecipeList = RequestToRecipeDAO.findRequestForDoctor(doctorLogin, status);
        } catch (DAOException e) {
            throw new ServiceException(e);
        }
        return requestToRecipeList;
    }


}
