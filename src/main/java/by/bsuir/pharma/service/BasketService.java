package by.bsuir.pharma.service;

import by.bsuir.pharma.dao.exception.DAOException;
import by.bsuir.pharma.dao.implementation.BasketDAO;
import by.bsuir.pharma.entity.Basket;
import by.bsuir.pharma.entity.Preparation;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import javax.servlet.http.Cookie;
import java.io.UnsupportedEncodingException;
import java.lang.reflect.Type;
import java.net.URLDecoder;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by Иван on 10.05.2016.
 */
public class BasketService {
    private final static String CART_STORE_COOKIE = "cart_store";
    private final static float START_COUNT_PRODUCT_IN_BASKET = 1;

    public Cookie addProductToBasket(Cookie[] cookies, Integer idProduct) throws ServiceException {
        Cookie cartCookie = null;
        Cookie newCartCookie = null;
        Map<Integer, Float> idCartProducts = new HashMap<>();
        Gson gson = new Gson();
        Type dataSetType = new TypeToken<HashMap<Integer, Float>>() {
        }.getType();
        String jsonCartProduct;
        try {
            if (cookies != null) {
                for (Cookie cookie : cookies) {
                    if (cookie.getName().equals(CART_STORE_COOKIE)) {
                        cartCookie = cookie;
                        break;
                    }
                }
                if (cartCookie != null) {
                    String cartCookieValue = URLDecoder.decode(cartCookie.getValue(), "UTF-8");
                    idCartProducts.putAll(gson.fromJson(cartCookieValue, dataSetType));
                }
            }
            if (!idCartProducts.containsKey(idProduct)) {
                idCartProducts.put(idProduct, START_COUNT_PRODUCT_IN_BASKET);
                jsonCartProduct = gson.toJson(idCartProducts, dataSetType);
                newCartCookie = new Cookie(CART_STORE_COOKIE, URLEncoder.encode(jsonCartProduct, "UTF-8"));
            }


        } catch (UnsupportedEncodingException e) {
            throw new ServiceException(e);
        }
        return newCartCookie;
    }

    public static Integer addProductToBasket(Cookie[] cookies, String loginUser) throws ServiceException {
        Cookie cartCookie = null;
        Integer countProduct;
        HashMap<Integer, Float> idCartProducts = new HashMap<>();
        Gson gson = new Gson();
        Type dataSetType = new TypeToken<HashMap<Integer, Float>>() {
        }.getType();
        try {
            if (cookies != null) {
                for (Cookie cookie : cookies) {
                    if (cookie.getName().equals(CART_STORE_COOKIE)) {
                        cartCookie = cookie;
                        break;
                    }
                }
                if (cartCookie != null) {
                    String cartCookieValue;

                    cartCookieValue = URLDecoder.decode(cartCookie.getValue(), "UTF-8");

                    idCartProducts.putAll(gson.fromJson(cartCookieValue, dataSetType));
                    Basket basket = new Basket();
                    basket.setLoginUser(loginUser);
                    basket.setCount((float) 1);
                    BasketDAO dao = new BasketDAO();
                    for (Integer idProduct : idCartProducts.keySet()) {
                        basket.setProductNumber(idProduct);
                        dao.add(basket);
                    }
                }
            }

            countProduct = BasketDAO.findCountOfProduct(loginUser);

        } catch (UnsupportedEncodingException | DAOException e) {
            throw new ServiceException(e);
        }
        return countProduct;
    }

    public boolean addProductToBasket(Integer idProduct, String loginUser) throws ServiceException {
        boolean isAddInBasket;
        BasketDAO dao = new BasketDAO();
        Basket basket = new Basket();
        basket.setCount((float) 1);
        basket.setLoginUser(loginUser);
        basket.setOrder(0);
        basket.setProductNumber(idProduct);


        try {
            isAddInBasket = dao.add(basket);
        } catch (DAOException e) {
            throw new ServiceException(e);
        }
        return isAddInBasket;
    }

    public static List<Preparation> findProductsOfGuest(Cookie[] cookies) throws ServiceException {
        Map<Integer, Float> idCartProducts;
        List<Preparation> preparations = new ArrayList<>();
        try {
            idCartProducts = parseCartCookie(cookies);
            for (Integer idProduct : idCartProducts.keySet()) {
                Preparation preparation = BasketDAO.findProductInBasket(idProduct);
                preparation.setCount(idCartProducts.get(idProduct));
                preparations.add(preparation);
            }
        } catch (DAOException e) {
            throw new ServiceException(e);
        }
        return preparations;
    }

    public static List<Preparation> findProductsOfClient(String login) throws ServiceException {

        List<Preparation> preparations;
        try {
            preparations = BasketDAO.findProductInBasket(login);

        } catch (DAOException e) {
            throw new ServiceException(e);
        }
        return preparations;
    }

    public static Integer findPreparationWithoutRecipe(String login) throws ServiceException {
        try {
            return BasketDAO.findCountProductWithoutRecipe(login);
        } catch (DAOException e) {
            throw new ServiceException(e);
        }
    }


    public static List<Preparation> findProductsOfClient(Integer idOrder) throws ServiceException {

        List<Preparation> preparations;
        try {
            preparations = BasketDAO.findProductOfOrder(idOrder);

        } catch (DAOException e) {
            throw new ServiceException(e);
        }
        return preparations;
    }


    public static boolean removeProduct(String login, Integer productId) throws ServiceException {
        Basket basket = new Basket();
        basket.setOrder(0);
        basket.setLoginUser(login);
        basket.setProductNumber(productId);

        BasketDAO dao = new BasketDAO();

        try {
            dao.delete(basket);
        } catch (DAOException e) {
            throw new ServiceException(e);
        }
        return true;
    }

    public static Cookie removeProduct(Cookie[] cookies, Integer idProduct) throws ServiceException {
        Cookie newCartCookie = null;
        Map<Integer, Float> idCartProducts;
        Gson gson = new Gson();
        Type dataSetType = new TypeToken<HashMap<Integer, Float>>() {
        }.getType();
        String jsonCartProduct;
        try {
            idCartProducts = parseCartCookie(cookies);
            if (idCartProducts.containsKey(idProduct)) {
                idCartProducts.remove(idProduct);
                jsonCartProduct = gson.toJson(idCartProducts, dataSetType);
                newCartCookie = new Cookie(CART_STORE_COOKIE, URLEncoder.encode(jsonCartProduct, "UTF-8"));
            }

        } catch (UnsupportedEncodingException e) {
            throw new ServiceException(e);
        }
        return newCartCookie;
    }

    public static boolean updateProduct(String login, Float count, Integer productId) throws ServiceException {
        Basket basket = new Basket();
        basket.setOrder(0);
        basket.setLoginUser(login);
        basket.setProductNumber(productId);
        basket.setCount(count);

        BasketDAO dao = new BasketDAO();

        try {
            dao.update(basket);
        } catch (DAOException e) {
            throw new ServiceException(e);
        }
        return true;
    }

    public static Cookie updateProduct(Float count, Integer idProduct, Cookie[] cookies) throws ServiceException {
        Cookie cartCookie = null;
        Cookie newCartCookie;
        Map<Integer, Float> idCartProducts = new HashMap<>();
        Gson gson = new Gson();
        Type dataSetType = new TypeToken<HashMap<Integer, Float>>() {
        }.getType();
        String jsonCartProduct;
        try {
            if (cookies != null) {
                for (Cookie cookie : cookies) {
                    if (cookie.getName().equals(CART_STORE_COOKIE)) {
                        cartCookie = cookie;
                        break;
                    }
                }
                if (cartCookie != null) {
                    String cartCookieValue = null;
                    cartCookieValue = URLDecoder.decode(cartCookie.getValue(), "UTF-8");
                    idCartProducts.putAll(gson.fromJson(cartCookieValue, dataSetType));
                    if (idCartProducts.containsKey(idProduct)) {
                        idCartProducts.put(idProduct, count);
                    }
                }
            }
            jsonCartProduct = gson.toJson(idCartProducts, dataSetType);
            newCartCookie = new Cookie(CART_STORE_COOKIE, URLEncoder.encode(jsonCartProduct, "UTF-8"));
        } catch (UnsupportedEncodingException e) {
            throw new ServiceException(e);
        }
        return newCartCookie;
    }

    private static Map<Integer, Float> parseCartCookie(Cookie[] cookies) throws ServiceException {
        Cookie cartCookie = null;
        Map<Integer, Float> idCartProducts = new HashMap<>();
        Gson gson = new Gson();
        Type dataSetType = new TypeToken<HashMap<Integer, Float>>() {
        }.getType();
        try {
            if (cookies != null) {
                for (Cookie cookie : cookies) {
                    if (cookie.getName().equals(CART_STORE_COOKIE)) {
                        cartCookie = cookie;
                        break;
                    }
                }
                if (cartCookie != null) {
                    String cartCookieValue = null;
                    cartCookieValue = URLDecoder.decode(cartCookie.getValue(), "UTF-8");
                    idCartProducts.putAll(gson.fromJson(cartCookieValue, dataSetType));
                }
            }
        } catch (UnsupportedEncodingException e) {
            throw new ServiceException(e);
        }
        return idCartProducts;
    }


}
