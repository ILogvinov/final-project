package by.bsuir.pharma.service;

import by.bsuir.pharma.dao.exception.DAOException;
import by.bsuir.pharma.dao.implementation.RecipeDAO;
import by.bsuir.pharma.entity.Basket;
import by.bsuir.pharma.entity.Recipe;

import java.sql.Date;
import java.util.List;

/**
 * Created by Иван on 31.05.2016.
 */
public class RecipeService {

    public boolean registryNewRecipe(Recipe recipe) throws ServiceException {
        RecipeDAO dao = new RecipeDAO();
        try {
            dao.add(recipe);
        } catch (DAOException e) {
            throw new ServiceException(e);
        }
        return true;
    }

    public Recipe findRecipe(Integer id) throws ServiceException {
        Recipe recipe;
        try {
            recipe = RecipeDAO.findRecipe(id);
        } catch (DAOException e) {
            throw new ServiceException(e);
        }
        return recipe;
    }

    public static List<Recipe> findRecipeClient(String loginClient) throws ServiceException {
        List<Recipe> recipes;
        try {
            recipes = RecipeDAO.findClientRecipe(loginClient);
        } catch (DAOException e) {
            throw new ServiceException();
        }
        return recipes;
    }

    public static boolean attachRecipe(Basket basket) throws ServiceException {
        boolean result;
        try {
            result = RecipeDAO.attachRecipeToProduct(basket);
        } catch (DAOException e) {
            throw new ServiceException(e);
        }
        return result;
    }
}
