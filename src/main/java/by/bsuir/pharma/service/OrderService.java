package by.bsuir.pharma.service;

import by.bsuir.pharma.dao.exception.DAOException;
import by.bsuir.pharma.dao.implementation.BasketDAO;
import by.bsuir.pharma.dao.implementation.OrderDAO;
import by.bsuir.pharma.dao.implementation.ProductDao;
import by.bsuir.pharma.entity.Order;
import by.bsuir.pharma.entity.Preparation;
import by.bsuir.pharma.entity.Product;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Иван on 13.05.2016.
 */
public class OrderService {

    public static boolean addOrder(String login, String address, String paymentMethod) throws ServiceException {
        boolean result;
        Order order = new Order();
        order.setUserLogin(login);
        order.setAddress(address);
        order.setPaymentMethod(paymentMethod);
        OrderDAO dao = new OrderDAO();
        try {
            if(BasketDAO.findCountProductWithoutRecipe(order.getUserLogin())==0){
                dao.add(order);
                result = true;
            }else {
                result = false;
            }

        } catch (DAOException e) {
            throw new ServiceException(e);
        }
        return result;
    }

    public static List<Order> findOrder(String login) throws ServiceException {
        List<Order> orders = new ArrayList<>();

        try {
            orders.addAll(OrderDAO.findOderByLogin(login));
        } catch (DAOException e) {
            throw new ServiceException(e);
        }
        return orders;
    }

    public static List<Order> findOrderByStatus(String status) throws ServiceException {
        List<Order> orders = new ArrayList<>();
        try {
            orders.addAll(OrderDAO.findOderByStatus(status));
        } catch (DAOException e) {
            throw new ServiceException(e);
        }
        return orders;
    }

    public static boolean updateStatusOrder(String status, Integer idOrder) throws ServiceException {
        try {
            if ("DELIVERED".equals(status)) {
                List<Preparation> preparations = BasketDAO.findProductOfOrder(idOrder);
                ProductDao productDao = new ProductDao();
                for (Preparation p: preparations){
                    Product product = productDao.findByPK(p.getLotNumber().toString());
                    if(product.getCountOnSparePart()<p.getCount()){
                        return false;
                    }
                }
                OrderDAO.updateOrderStatus(idOrder, status);
            } else {
                OrderDAO.updateOrderStatus(idOrder,status);
            }
        } catch (DAOException e) {
            throw new ServiceException(e);
        }
    return true;
    }


}
