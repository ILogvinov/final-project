package by.bsuir.pharma.entity;

import by.bsuir.pharma.dao.implementation.Identified;

/**
 * Created by Иван on 10.05.2016.
 */
public class Basket implements Identified<String> {
    private Integer order;
    private String loginUser;
    private Integer productNumber;
    private Float count;
    private Integer idRecipe;

    public Integer getProductNumber() {
        return productNumber;
    }

    public void setProductNumber(Integer productNumber) {
        this.productNumber = productNumber;
    }

    public String getLoginUser() {
        return loginUser;
    }

    public void setLoginUser(String loginUser) {
        this.loginUser = loginUser;
    }

    public Integer getOrder() {
        return order;
    }

    public void setOrder(Integer order) {
        this.order = order;
    }

    public Float getCount() {
        return count;
    }

    public void setCount(Float count) {
        this.count = count;
    }

    @Override
    public String getId() {
        return loginUser;
    }

    public Integer getIdRecipe() {
        return idRecipe;
    }

    public void setIdRecipe(Integer idRecipe) {
        this.idRecipe = idRecipe;
    }
}
