package by.bsuir.pharma.entity;

import by.bsuir.pharma.dao.implementation.Identified;

import java.io.Serializable;
import java.sql.Date;

/**
 * Created by Иван on 25.05.2016.
 */
public class RequestToRecipe implements Identified<Integer>,Serializable {
    private Integer id;
    private Date date;
    private String status;
    private Integer idRecipe;
    private String namePreparation;
    private String nameOfClient;
    @Override
    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public Integer getIdRecipe() {
        return idRecipe;
    }

    public void setIdRecipe(Integer idRecipe) {
        this.idRecipe = idRecipe;
    }

    public String getNamePreparation() {
        return namePreparation;
    }

    public void setNamePreparation(String namePreparation) {
        this.namePreparation = namePreparation;
    }

    public String getNameOfClient() {
        return nameOfClient;
    }

    public void setNameOfClient(String nameOfClient) {
        this.nameOfClient = nameOfClient;
    }
}
