package by.bsuir.pharma.entity;

/**
 * Created by Иван on 17.04.2016.
 */
public enum Role {

    CLIENT{
        {
            value = "client";
        }
    },PHARMACIST{
        {
            value = "pharmacist";
        }
    },DOCTOR{
        {
            value = "doctor";
        }
    },ADMIN{
        {
            value = "admin";
        }
    },GUEST{
        {
            value = "guest";
        }
    };
    String value;

    public String getValue() {
        return value;
    }
}
