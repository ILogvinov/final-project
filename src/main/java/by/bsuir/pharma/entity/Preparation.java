package by.bsuir.pharma.entity;

import by.bsuir.pharma.dao.implementation.Identified;

import java.sql.Date;

/**
 * Created by Иван on 27.04.2016.
 */
public class Preparation implements Identified<String> {
    private Integer lotNumber;
    private String id;
    private String group;
    private boolean requireRecipe;
    private String name;
    private String manufacturer;
    private Float dosage;
    private Float countUnits;
    private String testimony;
    private String pharmacologicalEffect;
    private String contraindications;
    private String modeOfApplication;
    private Integer totalCount;
    private String imageName;
    private Float price;
    private Float count;
    private Integer manufactureID;
    private Integer recipe;
    private Integer discount;
    private String country;
    private Date endShelfLife;

    public Preparation() {
    }


    @Override
    public String getId() {
        return id;
    }

    public String getGroup() {
        return group;
    }

    public void setGroup(String group) {
        this.group = group;
    }



    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getManufacturer() {
        return manufacturer;
    }

    public void setManufacturer(String manufacturer) {
        this.manufacturer = manufacturer;
    }

    public Float getDosage() {
        return dosage;
    }

    public void setDosage(Float dosage) {
        this.dosage = dosage;
    }

    public Float getCountUnits() {
        return countUnits;
    }

    public void setCountUnits(Float countUnits) {
        this.countUnits = countUnits;
    }

    public String getTestimony() {
        return testimony;
    }

    public void setTestimony(String testimony) {
        this.testimony = testimony;
    }

    public String getPharmacologicalEffect() {
        return pharmacologicalEffect;
    }

    public void setPharmacologicalEffect(String pharmacologicalEffect) {
        this.pharmacologicalEffect = pharmacologicalEffect;
    }

    public String getContraindications() {
        return contraindications;
    }

    public void setContraindications(String contraindications) {
        this.contraindications = contraindications;
    }

    public String getModeOfApplication() {
        return modeOfApplication;
    }

    public void setModeOfApplication(String modeOfApplication) {
        this.modeOfApplication = modeOfApplication;
    }

    public Integer getTotalCount() {
        return totalCount;
    }

    public void setTotalCount(Integer totalCount) {
        this.totalCount = totalCount;
    }

    public String getImageName() {
        return imageName;
    }

    public void setImageName(String imageName) {
        this.imageName = imageName;
    }

    public Float getPrice() {
        return price;
    }

    public void setPrice(Float price) {
        this.price = price;
    }

    public Integer getLotNumber() {
        return lotNumber;
    }

    public void setLotNumber(Integer lotNumber) {
        this.lotNumber = lotNumber;
    }

    public Float getCount() {
        return count;
    }

    public void setCount(Float count) {
        this.count = count;
    }

    public Integer getManufactureID() {
        return manufactureID;
    }

    public void setManufactureID(Integer manufactureID) {
        this.manufactureID = manufactureID;
    }


    public boolean isRequireRecipe() {
        return requireRecipe;
    }

    public void setRequireRecipe(boolean requireRecipe) {
        this.requireRecipe = requireRecipe;
    }

    public Integer getRecipe() {
        return recipe;
    }

    public void setRecipe(Integer recipe) {
        this.recipe = recipe;
    }

    public Integer getDiscount() {
        return discount;
    }

    public void setDiscount(Integer discount) {
        this.discount = discount;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public Date getEndShelfLife() {
        return endShelfLife;
    }

    public void setEndShelfLife(Date endShelfLife) {
        this.endShelfLife = endShelfLife;
    }
}
