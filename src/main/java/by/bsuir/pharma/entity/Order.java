package by.bsuir.pharma.entity;

import by.bsuir.pharma.dao.implementation.Identified;

import java.sql.Date;


/**
 * Created by Иван on 10.05.2016.
 */
public class Order implements Identified<Integer> {
    private Integer id;
    private Date date;
    private String userLogin;
    private String paymentMethod;
    private String address;
    private String status;
    @Override
    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public String getUserLogin() {
        return userLogin;
    }

    public void setUserLogin(String userLogin) {
        this.userLogin = userLogin;
    }

    public String getPaymentMethod() {
        return paymentMethod;
    }

    public void setPaymentMethod(String paymentMethod) {
        this.paymentMethod = paymentMethod;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }
}
