package by.bsuir.pharma.entity;

/**
 * Created by Иван on 04.05.2016.
 */
public enum CharacteristicPreparation {

    GROUP{
        {
            this.value = "type_preparation";
        }
    },
    TYPE_PACKING{
        {
            this.value = "type_packing";
        }
    },
    MIN_PRICE{
        {
            this.value = "min_price";
        }
    },
    MAX_PRICE{
        {
            this.value = "max_price";
        }
    },
    IS_REQUIRE_RECIPE{
        {
            this.value = "is_require_recipe";
        }
    };

    String value;

    public String getValue() {
        return value;
    }
}
