package by.bsuir.pharma.entity;

import by.bsuir.pharma.dao.implementation.Identified;

import java.io.Serializable;
import java.sql.Date;

/**
 * Created by Иван on 25.05.2016.
 */
public class Recipe implements Identified<Integer>, Serializable{
    private Integer id;
    private String clientLogin;
    private String doctorLogin;
    private String doctorName;
    private String clientName;
    private Date startDate;
    private Date finishDate;
    private String nameOfPreparation;
    private Float dosage;
    private Float amount;
    private String status;
    private Float discount;
    @Override
    public Integer getId() {
        return id;
    }

    public Recipe(){}

    public void setId(Integer id) {
        this.id = id;
    }

    public String getClientLogin() {
        return clientLogin;
    }

    public void setClientLogin(String clientLogin) {
        this.clientLogin = clientLogin;
    }

    public String getDoctorLogin() {
        return doctorLogin;
    }

    public void setDoctorLogin(String doctorLogin) {
        this.doctorLogin = doctorLogin;
    }

    public Date getStartDate() {
        return startDate;
    }

    public void setStartDate(Date startDate) {
        this.startDate = startDate;
    }

    public Date getFinishDate() {
        return finishDate;
    }

    public void setFinishDate(Date finishDate) {
        this.finishDate = finishDate;
    }

    public String getNameOfPreparation() {
        return nameOfPreparation;
    }

    public void setNameOfPreparation(String nameOfPreparation) {
        this.nameOfPreparation = nameOfPreparation;
    }

    public Float getDosage() {
        return dosage;
    }

    public void setDosage(Float dosage) {
        this.dosage = dosage;
    }

    public Float getAmount() {
        return amount;
    }

    public void setAmount(Float amount) {
        this.amount = amount;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public Float getDiscount() {
        return discount;
    }

    public void setDiscount(Float discount) {
        this.discount = discount;
    }

    public String getDoctorName() {
        return doctorName;
    }

    public void setDoctorName(String doctorName) {
        this.doctorName = doctorName;
    }

    public String getClientName() {
        return clientName;
    }

    public void setClientName(String clientName) {
        this.clientName = clientName;
    }
}
