package by.bsuir.pharma.entity;

import by.bsuir.pharma.dao.implementation.Identified;

import java.sql.Date;

/**
 * Created by Иван on 13.05.2016.
 */
public class Product implements Identified<Integer> {
    private Integer lotNumber;
    private Integer countOnSparePart;
    private Date dateConsigment;
    private Date endShelfLife;
    private Float price;
    private String preparationId;



    @Override
    public Integer getId() {
        return null;
    }

    public Integer getLotNumber() {
        return lotNumber;
    }

    public void setLotNumber(Integer lotNumber) {
        this.lotNumber = lotNumber;
    }

    public Integer getCountOnSparePart() {
        return countOnSparePart;
    }

    public void setCountOnSparePart(Integer countOnSparePart) {
        this.countOnSparePart = countOnSparePart;
    }

    public Date getDateConsigment() {
        return dateConsigment;
    }

    public void setDateConsigment(Date dateConsigment) {
        this.dateConsigment = dateConsigment;
    }

    public Date getEndShelfLife() {
        return endShelfLife;
    }

    public void setEndShelfLife(Date endShelfLife) {
        this.endShelfLife = endShelfLife;
    }

    public Float getPrice() {
        return price;
    }

    public void setPrice(Float price) {
        this.price = price;
    }

    public String getPreparationId() {
        return preparationId;
    }

    public void setPreparationId(String preparationId) {
        this.preparationId = preparationId;
    }
}
