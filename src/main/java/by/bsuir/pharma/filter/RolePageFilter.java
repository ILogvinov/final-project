package by.bsuir.pharma.filter;

import by.bsuir.pharma.entity.Role;
import org.apache.commons.lang3.EnumUtils;

import javax.servlet.*;
import javax.servlet.annotation.WebFilter;
import javax.servlet.annotation.WebInitParam;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;

/**
 * Created by Иван on 20.04.2016.
 */
@WebFilter(filterName = "RolePageFilter",
        urlPatterns = {"/auntification"},
        initParams = {
                @WebInitParam(name = "pharmacist", value = "jsp/mainPharmacist.jsp"),
                @WebInitParam(name = "client", value = "jsp/mainClient.jsp"),
                @WebInitParam(name = "guest", value = "/start.jsp")
        })
public class RolePageFilter implements Filter {
    private FilterConfig config = null;

    @Override
    public void init(FilterConfig filterConfig) throws ServletException {
        config = filterConfig;
    }

    @Override
    public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain filterChain) throws IOException, ServletException {
        HttpServletRequest request = (HttpServletRequest) servletRequest;
        HttpSession session = request.getSession();
        HttpServletResponse response = (HttpServletResponse) servletResponse;
        Role role = (Role) session.getAttribute("role");
        String command = request.getParameter("command");
        if (EnumUtils.isValidEnum(CommandEnum.class, command.toUpperCase())) {
            CommandEnum commandEnum = CommandEnum.valueOf(command.toUpperCase());

            if (commandEnum.getRoles().contains(role)) {
                filterChain.doFilter(servletRequest, servletResponse);
            } else {
                response.sendRedirect(config.getInitParameter(role.getValue()));
            }
        } else {
            response.sendRedirect(config.getInitParameter(role.getValue()));

        }
    }

    @Override
    public void destroy() {

    }
}
