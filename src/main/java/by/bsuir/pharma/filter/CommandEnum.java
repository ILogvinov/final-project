package by.bsuir.pharma.filter;

import by.bsuir.pharma.command.DeleteUserCommand;
import by.bsuir.pharma.command.RegistryWorkerCommand;
import by.bsuir.pharma.command.ShowListProductCommand;
import by.bsuir.pharma.command.ShowWorkerListCommand;
import by.bsuir.pharma.entity.Role;

import java.util.ArrayList;
import java.util.Collections;

/**
 * Created by Иван on 18.06.2016.
 */
public enum CommandEnum {
    LOGIN(Role.GUEST),
    LOGOUT(Role.CLIENT, Role.DOCTOR, Role.ADMIN, Role.PHARMACIST),
    LOCALIZATION(Role.GUEST, Role.DOCTOR, Role.PHARMACIST, Role.CLIENT),
    REGISTRATE(Role.GUEST),
    START(Role.GUEST),
    CHANGE_INFORMATION(Role.CLIENT),
    OPEN_PAGE(Role.CLIENT, Role.DOCTOR, Role.GUEST, Role.PHARMACIST),
    SHOW_CATALOG(Role.CLIENT, Role.GUEST),
    FILTER_CATALOG(Role.CLIENT, Role.GUEST),
    SHOW_PREPARATION_INF(Role.CLIENT, Role.DOCTOR, Role.GUEST, Role.PHARMACIST),
    ADD_TO_CART(Role.CLIENT, Role.GUEST),
    SHOW_CART(Role.CLIENT, Role.GUEST),
    REMOVE_PRODUCT_CART(Role.CLIENT, Role.GUEST),
    UPDATE_PRODUCT_COMMAND(Role.GUEST, Role.CLIENT),
    CHECK_OUT(Role.CLIENT),
    FORM_ORDER(Role.CLIENT),
    SHOW_ORDER(Role.CLIENT),
    SHOW_DETAILED_ORDER(Role.CLIENT,Role.PHARMACIST),
    SHOW_PHARMACIST_ORDER(Role.PHARMACIST),
    UPDATE_ORDER_STATUS(Role.PHARMACIST),
    ADD_NEW_PRODUCT(Role.PHARMACIST),
    ADD_NEW_PREPARATION(Role.PHARMACIST),
    DELETE_PRODUCT(Role.PHARMACIST),
    CHANGE_COUNT_PRODUCT(Role.PHARMACIST),
    SHOW_LIST_PREPARATION(Role.CLIENT, Role.PHARMACIST) ,
    REGISTER_RECIPE(Role.DOCTOR),
    SHOW_RECIPE_INF(Role.CLIENT,Role.PHARMACIST,Role.DOCTOR) ,
    SHOW_REQUEST(Role.DOCTOR) ,
    CHANGE_REQUEST_STATUS(Role.DOCTOR) ,
    SHOW_RECIPE_LIST(Role.CLIENT) ,
    REGISTER_REQUEST_RECIPE(Role.CLIENT),
    ATTACH_RECIPE(Role.CLIENT),
    SEARCH_PREPARATION(Role.CLIENT, Role.GUEST, Role.DOCTOR, Role.PHARMACIST),
    SHOW_CLOSED_ORDERS(Role.PHARMACIST),
    SHOW_LIST_PRODUCT(Role.PHARMACIST),
    REGISTER_WORKER(Role.ADMIN),
    DELETE_USER(Role.ADMIN),
    SHOW_WORKERS(Role.ADMIN);

    ArrayList<Role> roles;

    CommandEnum(Role role) {
        roles = new ArrayList<>();
        roles.add(role);
    }

    CommandEnum(Role... role) {
        roles = new ArrayList<>();
        Collections.addAll(roles, role);
    }

    public ArrayList<Role> getRoles(){
        return roles;
    }
}

