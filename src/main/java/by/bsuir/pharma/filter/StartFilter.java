package by.bsuir.pharma.filter;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import javax.servlet.*;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import java.io.IOException;
import java.lang.reflect.Type;
import java.net.URLDecoder;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Set;

/**
 * Created by Иван on 12.05.2016.
 */
@WebFilter(filterName = "StartFilter",
        urlPatterns = {"/start.jsp"},
        dispatcherTypes = {
                DispatcherType.FORWARD
        }
)
public class StartFilter implements Filter {


    @Override
    public void init(FilterConfig filterConfig) throws ServletException {

    }

    @Override
    public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain filterChain) throws IOException, ServletException {
        HttpServletRequest request = (HttpServletRequest) servletRequest;
        Cookie[] cookies = request.getCookies();
        Cookie cartCookie = null;
        HashMap<Integer,Float> idCartProducts = new HashMap<>();
        Gson gson = new Gson();
        Type dataSetType = new TypeToken<HashMap<Integer,Float>>() {
        }.getType();
        if (cookies != null) {
            for (Cookie cookie : cookies) {
                if ("cart_store".equals(cookie.getName())) {
                    cartCookie = cookie;
                    break;
                }
            }
        }
        if(cartCookie!=null){
            String cartCookieValue = null;
            cartCookieValue = URLDecoder.decode(cartCookie.getValue(), "UTF-8");
            idCartProducts.putAll(gson.fromJson(cartCookieValue, dataSetType));
            request.getSession().setAttribute("countProductCart",idCartProducts.size());
        }else {
            request.getSession().setAttribute("countProductCart",0);
        }
        filterChain.doFilter(servletRequest, servletResponse);
    }

    @Override
    public void destroy() {

    }
}


