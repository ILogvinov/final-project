package by.bsuir.pharma.resource;

import javax.servlet.http.HttpServletRequest;
import java.util.Locale;
import java.util.ResourceBundle;

/**
 * Created by Иван on 27.03.2016.
 */
public class MessageManager {

    public static String getProperty(String key, Locale locale){
        ResourceBundle resourceBundle = ResourceBundle.getBundle("message",locale);
        return  resourceBundle.getString(key);
    }

    public static String getProperty(String key){

        //String atr = (String) request.getSession().getAttribute("locale");
        Locale locale;

            locale = new Locale("ru","RU");


        ResourceBundle resourceBundle = ResourceBundle.getBundle("message",locale);
        return  resourceBundle.getString(key);
    }
}


