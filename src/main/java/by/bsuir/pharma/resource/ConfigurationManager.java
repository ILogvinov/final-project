package by.bsuir.pharma.resource;

import java.util.ResourceBundle;

/**
 * Created by Иван on 27.03.2016.
 */
public class ConfigurationManager {
    private final static ResourceBundle resourceBundle = ResourceBundle.getBundle("config");

    public static String getProperty(String key){
        return resourceBundle.getString(key);
    }
}
