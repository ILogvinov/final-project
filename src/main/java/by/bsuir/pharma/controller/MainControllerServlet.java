package by.bsuir.pharma.controller;

import by.bsuir.pharma.command.ActionCommand;
import by.bsuir.pharma.command.cookie.CartCookieCommand;
import by.bsuir.pharma.command.factory.ActionFactory;
import by.bsuir.pharma.entity.Role;
import by.bsuir.pharma.pool.ConnectionPool;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;


/**
 * Created by Иван on 27.03.2016.
 */
@WebServlet(urlPatterns = "/auntification", name = "auntification")
public class MainControllerServlet extends HttpServlet {

    @Override
    public void init() throws ServletException {
        super.init();
    }

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        processRequest(request,response);
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        processRequest(request,response);
    }

    private void processRequest(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
        String page;
        ActionFactory client = new ActionFactory();
        ActionCommand command = client.defineCommand(request);

        page = command.execute(request);
        if ((command instanceof CartCookieCommand) && (Role.GUEST.equals(request.getSession().getAttribute("role")))) {
            CartCookieCommand cartCookieCommand = (CartCookieCommand) command;
            if (cartCookieCommand.getCartCookie() != null) {
                response.addCookie(cartCookieCommand.getCartCookie());
            }
        }
        if (command.isRedirect()) {
            response.sendRedirect(page);
        } else {
            request.getRequestDispatcher(page).forward(request, response);
        }
    }

    @Override
    public void destroy() {

    }
}
