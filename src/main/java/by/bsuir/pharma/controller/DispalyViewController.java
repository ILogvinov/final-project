package by.bsuir.pharma.controller;

import by.bsuir.pharma.command.AddImagePreparationCommand;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * Created by Иван on 13.04.2016.
 */
@WebServlet(urlPatterns = "/auntification/addNewPreparation",name = "DisplayViewController")
public class DispalyViewController extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String page;
        AddImagePreparationCommand command = new AddImagePreparationCommand();
        page = command.execute(request);
        response.sendRedirect(page);
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        AddImagePreparationCommand command = new AddImagePreparationCommand();
        String page = command.execute(request);
        String error = request.getParameter("error");
        if(error!=null){
            request.setAttribute("errorMessage",error);
        }
        RequestDispatcher view = request.getRequestDispatcher(page);
        view.forward(request,response);
    }
}
