package by.bsuir.pharma.tag;


import javax.servlet.jsp.JspException;
import javax.servlet.jsp.JspWriter;
import javax.servlet.jsp.tagext.TagSupport;
import java.io.IOException;

/**
 * Created by Иван on 11.05.2016.
 */
public class SubTotalTag extends TagSupport {
    private Float price;
    private Float count;

    public void setPrice(Float price) {
        this.price = price;
    }
    public void setCount(Float count) {
        this.count = count;
    }

    @Override
    public int doStartTag() throws JspException {
        Float subTotal = price*count;
        JspWriter out = pageContext.getOut();
        try {
            out.write(subTotal.toString());
        } catch (IOException e) {
            e.printStackTrace();
        }
        return SKIP_BODY;
    }
}
