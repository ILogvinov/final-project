package by.bsuir.pharma.tag;

import by.bsuir.pharma.entity.Recipe;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.jsp.JspException;
import javax.servlet.jsp.JspWriter;
import javax.servlet.jsp.tagext.TagSupport;
import java.io.IOException;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Created by Иван on 13.06.2016.
 */
public class RecipeListTag extends TagSupport {
    private String namePreparation;

    @Override
    public int doStartTag() throws JspException {
        HttpServletRequest request = (HttpServletRequest) pageContext.getRequest();
        JspWriter out = pageContext.getOut();
        List<Recipe> recipes = (List<Recipe>) request.getAttribute("recipes");
        List<Recipe> requiredRecipes = recipes.stream().filter(recipe -> namePreparation.equals(recipe.getNameOfPreparation())&&"ACTIVE".equals(recipe.getStatus())).collect(Collectors.toList());
        try {
            out.write("<select required name=\"idRecipe\">");
            for(Recipe recipe: requiredRecipes){
                out.write("<option value=\""+recipe.getId()+"\">");
                out.write(recipe.getNameOfPreparation());
                out.write("</option>");
            }
            out.write("</select>");
        } catch (IOException e) {
            e.printStackTrace();
        }
        return SKIP_BODY;
    }


    public void setNamePreparation(String namePreparation) {
        this.namePreparation = namePreparation;
    }
}
