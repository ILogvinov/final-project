package by.bsuir.pharma.tag;


import by.bsuir.pharma.resource.MessageManager;

import javax.servlet.jsp.JspException;
import javax.servlet.jsp.JspTagException;
import javax.servlet.jsp.JspWriter;
import javax.servlet.jsp.tagext.BodyTagSupport;
import java.io.IOException;

/**
 * Created by Иван on 26.04.2016.
 */
public class SimpleTag extends BodyTagSupport {
    private String role;
    private Integer count;

    public void setRole(String role) {
        this.role = role;
    }
    public void setCount(Integer count) {
        this.count = count;
    }


    @Override
    public int doStartTag() throws JspException {
        try {
            JspWriter out = pageContext.getOut();
            out.write("<li><a href=#>");
        } catch (IOException e) {
            throw new JspTagException(e.getMessage());
        }
        return EVAL_BODY_INCLUDE;
    }


    @Override
    public int doAfterBody() throws JspException {
        if (count-- > 1) {
            try {
                pageContext.getOut().write("</a></li>" +"<li><a href=\"#\">"+ MessageManager.getProperty(role + ".opportunity_" + String.valueOf(count)));
            } catch (IOException e) {
                throw new JspTagException(e.getMessage());
            }
            return EVAL_BODY_AGAIN;
        } else {
            return SKIP_BODY;
        }
    }

    @Override
    public int doEndTag() throws JspTagException {
        try {pageContext.getOut().write("</a></li>");
        } catch (IOException e) {
            throw new JspTagException(e.getMessage());
        }
        return EVAL_PAGE;
    }


}
