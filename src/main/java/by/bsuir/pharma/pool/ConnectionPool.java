package by.bsuir.pharma.pool;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.ResourceBundle;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

/**
 * Created by Иван on 13.04.2016.
 */
public class ConnectionPool {

    private static Logger logger = LogManager.getLogger(ConnectionPool.class);
    private static AtomicBoolean instanceCreated = new AtomicBoolean(false);
    private static ConnectionPool instance;
    private static AtomicBoolean closed = new AtomicBoolean(false);
    private static final long CLOSE_TIMEOUT = 1;
    private static Lock lock = new ReentrantLock();

    private ArrayBlockingQueue<ProxyConnection> freeConnections;
    private ArrayBlockingQueue<ProxyConnection> usedConnections;
    private static final int POOL_SIZE = 10;
    private static final String DRIVER = "com.mysql.jdbc.Driver";
    private static final String BUNDLE_NAME = "database";


    private ConnectionPool() {
        ResourceBundle bundle = ResourceBundle.getBundle(BUNDLE_NAME);
        String url = bundle.getString("url");
        String user = bundle.getString("user");
        String password = bundle.getString("password");
        freeConnections = new ArrayBlockingQueue<>(POOL_SIZE);
        usedConnections = new ArrayBlockingQueue<>(POOL_SIZE);


        try {
            Class.forName(DRIVER);
        } catch (ClassNotFoundException ex) {
            logger.fatal("Can not connect to database", ex);
            throw new RuntimeException(ex);
        }

        for (int i = 0; i < POOL_SIZE; i++) {
            try {
                ProxyConnection connection = new ProxyConnection(DriverManager.getConnection(url, user, password));
                freeConnections.add(connection);
            } catch (SQLException e) {
                logger.error(e);
            }

        }

    }

    public static ConnectionPool getInstance() {
        if (!instanceCreated.get()) {
            lock.lock();
            try {
                if (!instanceCreated.get()) {
                    instance = new ConnectionPool();
                    instanceCreated.set(true);
                }
            } finally {
                lock.unlock();
            }
        }

        return instance;
    }


    public ProxyConnection getConnection() {
        ProxyConnection connection = null;
        if (!closed.get()) {
            try {
                connection = freeConnections.take();
                usedConnections.add(connection);
            } catch (InterruptedException ex) {
                logger.warn("ProxyConnection fail", ex);
            }
        }

        return connection;
    }

    void freeConnection(ProxyConnection connection) {
        usedConnections.remove(connection);
        try {
            freeConnections.put(connection);
        } catch (InterruptedException ex) {
            logger.warn("ProxyConnection fail", ex);
        }
    }


    public void releaseConnections() {
        closed.set(true);
        try {
            TimeUnit.SECONDS.sleep(CLOSE_TIMEOUT);
            for (ProxyConnection connection : freeConnections) {
                try {
                    connection.closeConnection();
                } catch (SQLException ex) {
                    logger.error("Connection can not be closed", ex);
                }
            }
            if (!usedConnections.isEmpty()) {
                for (ProxyConnection connection : usedConnections) {
                    try {
                        connection.closeConnection();
                    } catch (SQLException ex) {
                        logger.error("Connection can not be closed", ex);
                    }
                }
            }
        } catch (InterruptedException ex) {
            logger.warn("Release connection exception", ex);
        }

    }


}
