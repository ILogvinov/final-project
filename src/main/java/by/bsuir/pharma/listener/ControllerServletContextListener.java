package by.bsuir.pharma.listener;

import by.bsuir.pharma.pool.ConnectionPool;

import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;

/**
 * Created by Иван on 20.04.2016.
 */
public class ControllerServletContextListener implements ServletContextListener {
    @Override
    public void contextInitialized(ServletContextEvent servletContextEvent) {
        ConnectionPool pool = ConnectionPool.getInstance();
    }

    @Override
    public void contextDestroyed(ServletContextEvent servletContextEvent) {
        ConnectionPool pool = ConnectionPool.getInstance();
        pool.releaseConnections();
    }
}
