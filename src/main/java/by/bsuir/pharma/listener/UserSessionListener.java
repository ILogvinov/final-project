package by.bsuir.pharma.listener;

import javax.servlet.annotation.WebListener;
import javax.servlet.http.HttpSession;
import javax.servlet.http.HttpSessionEvent;
import javax.servlet.http.HttpSessionListener;

import java.util.Locale;

import static by.bsuir.pharma.entity.Role.GUEST;

/**
 * Created by Иван on 20.04.2016.
 */
@WebListener
public class UserSessionListener implements HttpSessionListener {
    private final static String NAME_ROLE_ATTRIBUTE = "role";


    @Override
    public void sessionCreated(HttpSessionEvent httpSessionEvent) {
        HttpSession session = httpSessionEvent.getSession();
        session.setAttribute(NAME_ROLE_ATTRIBUTE, GUEST);
        session.setAttribute("locale", "ru_RU");
        session.setAttribute("Locale", new Locale("ru", "RU"));
    }

    @Override
    public void sessionDestroyed(HttpSessionEvent httpSessionEvent) {

    }
}
