package by.bsuir.pharma.command;

import by.bsuir.pharma.resource.ConfigurationManager;
import by.bsuir.pharma.resource.MessageManager;
import by.bsuir.pharma.service.OrderService;
import by.bsuir.pharma.service.ServiceException;

import javax.servlet.http.HttpServletRequest;

/**
 * Created by Иван on 13.05.2016.
 */
public class UpdateStatusOrderCommand implements ActionCommand {
    private boolean isRedirect;
    private static final String ORDER_STATUS_REGEXP = "REJECTED|DELIVERED|CANCEL|FINISH";
    private static final String ORDER_ID_REGEXP = "\\d{1,11}";

    private static final String PARAM_STATUS_NAME = "status";
    private static final String PARAM_ID_ORDER_NAME = "idOrder";

    private static final String ATTRIBUTE_ERROR_MESSAGE = "errorMessage";

    @Override
    public String execute(HttpServletRequest request) {
        String status = request.getParameter(PARAM_STATUS_NAME);
        String  idOrder = request.getParameter(PARAM_ID_ORDER_NAME);
        String page;
        if(validate(status,idOrder)){
            try {

                if (OrderService.updateStatusOrder(status, Integer.valueOf(idOrder))) {
                    page = ConfigurationManager.getProperty("path.page.pharmacist.orders");
                } else {
                    page = ConfigurationManager.getProperty("path.page.pharmacist.orders");
                    request.setAttribute(ATTRIBUTE_ERROR_MESSAGE, MessageManager.getProperty("message.error.update.order", request.getLocale()));

                }
            } catch (ServiceException e) {
                page = ConfigurationManager.getProperty("path.page.pharmacist.orders");
                request.setAttribute(ATTRIBUTE_ERROR_MESSAGE, MessageManager.getProperty("message.error.update.order", request.getLocale()));
            }
        }else {
            request.setAttribute(ATTRIBUTE_ERROR_MESSAGE, MessageManager.getProperty("message.invalid.link",request.getLocale()));
            page = ConfigurationManager.getProperty("path.page.fail.result");        }

        return page;
    }

    @Override
    public boolean isRedirect() {
        return isRedirect;
    }

    private boolean validate(String status, String idOrder) {
        return !(status.isEmpty() || !checkRegexp(status, ORDER_STATUS_REGEXP)) && !(idOrder.isEmpty() || !checkRegexp(idOrder, ORDER_ID_REGEXP));
    }


}
