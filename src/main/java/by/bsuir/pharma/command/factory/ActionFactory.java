package by.bsuir.pharma.command.factory;

import by.bsuir.pharma.command.ActionCommand;
import by.bsuir.pharma.command.CommandEnum;
import by.bsuir.pharma.command.EmptyCommand;
import by.bsuir.pharma.resource.MessageManager;

import javax.servlet.http.HttpServletRequest;

/**
 * Created by Иван on 27.03.2016.
 */
public class ActionFactory {
    public ActionCommand defineCommand(HttpServletRequest request) {
        ActionCommand current = new EmptyCommand();

        String action = request.getParameter("command");
        if (action == null || action.isEmpty()) {
            return current;
        }

        try {
            CommandEnum currentEnum = CommandEnum.valueOf(action.toUpperCase());

            current = currentEnum.getCurrentCommand();
        } catch (IllegalArgumentException e) {
            request.setAttribute("wrongAction", action
                    + MessageManager.getProperty("message.wrongaction",request.getLocale()));
        }
        return current;
    }
}
