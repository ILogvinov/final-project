package by.bsuir.pharma.command;

import by.bsuir.pharma.service.RequestToRecipeService;
import by.bsuir.pharma.service.ServiceException;

import javax.servlet.http.HttpServletRequest;
import java.sql.Date;

/**
 * Created by Иван on 12.06.2016.
 */
public class ChangeStatusRequestOfRecipe implements ActionCommand {
    private boolean isRedirect;
    @Override
    public String execute(HttpServletRequest request) {
        String page = null;
        String status = request.getParameter("status");
        Integer idRequest = Integer.valueOf(request.getParameter("idRequest"));
        RequestToRecipeService service = new RequestToRecipeService();
        try {
            switch (status) {
                case "APPROVED":
                    Date newFinishDate = Date.valueOf(request.getParameter("newFinishDate"));
                    service.approveRequest(idRequest, newFinishDate);
                    break;
                case "REJECTED":
                    service.rejectRequest(idRequest);
                    break;
            }
            page = "http://localhost:8080/auntification?command=show_request";
            isRedirect = true;
        } catch (ServiceException e) {
            e.printStackTrace();
        }
        return page;
    }

    @Override
    public boolean isRedirect() {
        return isRedirect;
    }
}
