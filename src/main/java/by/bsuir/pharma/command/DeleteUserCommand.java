package by.bsuir.pharma.command;

import by.bsuir.pharma.resource.ConfigurationManager;
import by.bsuir.pharma.resource.MessageManager;
import by.bsuir.pharma.service.ServiceException;
import by.bsuir.pharma.service.WorkerService;

import javax.servlet.http.HttpServletRequest;

/**
 * Created by Иван on 13.05.2016.
 */
public class DeleteUserCommand implements ActionCommand {
    private boolean isRedirect;
    private static final String PARAM_LOGIN = "login";

    @Override
    public String execute(HttpServletRequest request) {
        String login  = request.getParameter(PARAM_LOGIN);
        String page;
        try {
            WorkerService.deleteUser(login);
            page = request.getHeader("referer");
            isRedirect =true;
        } catch (ServiceException e) {
            request.setAttribute("errorMessage", MessageManager.getProperty("message.error.delete.user",request.getLocale()));
            page = ConfigurationManager.getProperty("path.page.fail.result");
        }
        return page;
    }

    @Override
    public boolean isRedirect() {
        return isRedirect;
    }
}
