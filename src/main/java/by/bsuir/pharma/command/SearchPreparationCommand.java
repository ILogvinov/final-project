package by.bsuir.pharma.command;

import by.bsuir.pharma.entity.Preparation;
import by.bsuir.pharma.entity.User;
import by.bsuir.pharma.resource.ConfigurationManager;
import by.bsuir.pharma.resource.MessageManager;
import by.bsuir.pharma.service.PreparationService;
import by.bsuir.pharma.service.ServiceException;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

/**
 * Created by Иван on 13.05.2016.
 */
public class SearchPreparationCommand implements ActionCommand {
    private final static String PARAM_NAME_PREPARATION = "preparationName";

    private final static String ATTRIBUTE_PREPARATIONS = "preparations";
    private final static String ATTRIBUTE_WARN_MESSAGE = "warnMessage";

    @Override
    public String execute(HttpServletRequest request) {
        String namePreparation = request.getParameter(PARAM_NAME_PREPARATION);
        PreparationService service = new PreparationService();
        String page;

        if (!namePreparation.isEmpty()) {
            try {
                List<Preparation> preparations = service.givePreparationByName(namePreparation);
                if (preparations.size() > 0) {
                    request.setAttribute(ATTRIBUTE_PREPARATIONS, preparations);
                } else {
                    request.setAttribute(ATTRIBUTE_WARN_MESSAGE, MessageManager.getProperty("message.preparation.not.found", request.getLocale()));
                }
                page = ConfigurationManager.getProperty("path.result.search.page");
            } catch (ServiceException e) {
                request.setAttribute(ATTRIBUTE_WARN_MESSAGE, MessageManager.getProperty("message.preparation.not.found", request.getLocale()));
                page = ConfigurationManager.getProperty("path.result.search.page");
            }
        } else {
            User user = (User) request.getSession().getAttribute("user");
            page = ConfigurationManager.getProperty("path.page.main" + "_" + user.getRole().toString().toLowerCase());
        }

        return page;
    }

    @Override
    public boolean isRedirect() {
        return false;
    }

}
