package by.bsuir.pharma.command;

import by.bsuir.pharma.entity.Order;
import by.bsuir.pharma.entity.User;
import by.bsuir.pharma.resource.ConfigurationManager;
import by.bsuir.pharma.resource.MessageManager;
import by.bsuir.pharma.service.OrderService;
import by.bsuir.pharma.service.ServiceException;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

/**
 * Created by Иван on 13.05.2016.
 */
public class ShowOrderCommand implements ActionCommand {
    private static final String ATTRIBUTE_ORDERS = "orders";
    private static final String ATTRIBUTE_ERROR_MESSAGE = "errorMessage";
    private static final String ATTRIBUTE_USER = "user";

    @Override
    public String execute(HttpServletRequest request) {
        String page;

        User user = (User) request.getSession().getAttribute(ATTRIBUTE_USER);
        String login = user.getLogin();
        try {
            List<Order> orders = OrderService.findOrder(login);
            request.setAttribute(ATTRIBUTE_ORDERS, orders);
            page = ConfigurationManager.getProperty("path.page.orders");
        } catch (ServiceException e) {
            request.setAttribute(ATTRIBUTE_ERROR_MESSAGE, MessageManager.getProperty("message.error.show.order", request.getLocale()));
            page = ConfigurationManager.getProperty("path.page.fail.result");
        }


        return page;
    }

    @Override
    public boolean isRedirect() {
        return false;
    }
}
