package by.bsuir.pharma.command;

import by.bsuir.pharma.entity.Basket;
import by.bsuir.pharma.entity.User;
import by.bsuir.pharma.resource.ConfigurationManager;
import by.bsuir.pharma.resource.MessageManager;
import by.bsuir.pharma.service.RecipeService;
import by.bsuir.pharma.service.ServiceException;

import javax.servlet.http.HttpServletRequest;

/**
 * Created by Иван on 14.06.2016.
 */
public class AttachRecipeOrderCommand implements ActionCommand {

    private static final String PRODUCT_LOT_NUMBER_REGEXP = "^[\\d]{9}$";
    private static final String RECIPE_ID_REGEXP = "^\\d{8,10}$";
    private static final String ORDER_ID_REGEXP = "^\\d{1,10}$";

    private static final String PARAMETER_ID_RECIPE = "idRecipe";
    private static final String PARAMETER_ID_PRODUCT = "idProduct";
    private static final String PARAMETER_ID_ORDER = "idOrder";

    private static final String ATTRIBUTE_USER = "user";
    private static final String ATTRIBUTE_ERROR_MESSAGE = "errorMessage";

    private boolean isRedirect;

    @Override
    public String execute(HttpServletRequest request) {
        String page;
        Basket basket = new Basket();
        if (validate(request, basket)) {
            try {
                RecipeService.attachRecipe(basket);
                page = ConfigurationManager.getProperty("path.link.show.cart");
                isRedirect = true;
            } catch (ServiceException e) {
                request.setAttribute(ATTRIBUTE_ERROR_MESSAGE, MessageManager.getProperty("message.error.attach.recipe", request.getLocale()));
                page = ConfigurationManager.getProperty("path.page.fail.result");
            }
        } else {
            request.setAttribute(ATTRIBUTE_ERROR_MESSAGE, MessageManager.getProperty("message.error.attach.recipe", request.getLocale()));
            page = ConfigurationManager.getProperty("path.page.fail.result");
        }


        return page;
    }

    @Override
    public boolean isRedirect() {
        return isRedirect;
    }

    private boolean validate(HttpServletRequest request, Basket basket) {
        boolean result = false;
        String paramIdRecipe = request.getParameter(PARAMETER_ID_RECIPE);
        String paramIdProduct = request.getParameter(PARAMETER_ID_PRODUCT);
        String paramIdOrder = request.getParameter(PARAMETER_ID_ORDER);

        if (!(paramIdOrder.isEmpty() && paramIdProduct.isEmpty() && paramIdRecipe.isEmpty())) {
            if (checkRegexp(paramIdOrder, ORDER_ID_REGEXP) && checkRegexp(paramIdProduct, PRODUCT_LOT_NUMBER_REGEXP) && checkRegexp(paramIdRecipe, RECIPE_ID_REGEXP)) {
                basket.setIdRecipe(Integer.valueOf(paramIdRecipe));
                basket.setProductNumber(Integer.valueOf(paramIdProduct));
                basket.setOrder(Integer.valueOf(paramIdOrder));
                User user = (User) request.getSession().getAttribute(ATTRIBUTE_USER);
                basket.setLoginUser(user.getLogin());
                result = true;
            }
        }

        return result;
    }
}
