package by.bsuir.pharma.command;

import by.bsuir.pharma.command.cookie.CartCookieCommand;
import by.bsuir.pharma.entity.Preparation;
import by.bsuir.pharma.entity.Role;
import by.bsuir.pharma.entity.User;
import by.bsuir.pharma.resource.ConfigurationManager;
import by.bsuir.pharma.resource.MessageManager;
import by.bsuir.pharma.service.BasketService;
import by.bsuir.pharma.service.ServiceException;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import java.util.List;

/**
 * Created by Иван on 12.05.2016.
 */
public class RemoveProductCartCommand extends CartCookieCommand {
    private boolean isRedirect;
    private final String COUNT_PRODUCT_CART_NAME = "countProductCart";
    private static final String PRODUCT_LOT_NUMBER_REGEXP = "^[\\d]{9}$";
    private static final String PARAMETER_ID_PRODUCT = "idProduct";

    private static final String ATTRIBUTE_ROLE = "role";
    private static final String ATTRIBUTE_USER = "user";
    private static final String ATTRIBUTE_PREPARATIONS = "preparations";
    private static final String ATTRIBUTE_ERROR_MESSAGE = "errorMessage";

    @Override
    public String execute(HttpServletRequest request) {
        String page = null;
        Cookie[] cookies = request.getCookies();
        String paramIdProduct = request.getParameter(PARAMETER_ID_PRODUCT);
        if(!paramIdProduct.isEmpty()&&checkRegexp(paramIdProduct,PRODUCT_LOT_NUMBER_REGEXP)){
            Integer idProduct = Integer.valueOf(request.getParameter(PARAMETER_ID_PRODUCT));
            try {
                switch ( (Role)request.getSession().getAttribute(ATTRIBUTE_ROLE)) {
                    case GUEST:
                        cookie = BasketService.removeProduct(cookies, idProduct);
                        if (cookie != null) {
                            changeAttributeCountCart(request);
                            page = request.getHeader("referer");
                            isRedirect = true;
                        }else {
                            List<Preparation> preparations = BasketService.findProductsOfGuest(request.getCookies());
                            request.setAttribute(ATTRIBUTE_PREPARATIONS,preparations);
                            page = ConfigurationManager.getProperty("path.page.cart");
                        }
                        break;
                    case CLIENT:
                        User user = (User) request.getSession().getAttribute(ATTRIBUTE_USER);
                        String login = user.getLogin();
                        if (BasketService.removeProduct(login, idProduct)) {
                            changeAttributeCountCart(request);
                            page = request.getHeader("referer");
                            isRedirect = true;
                        }else {
                            List<Preparation> preparations = BasketService.findProductsOfClient(login);
                            request.setAttribute(ATTRIBUTE_PREPARATIONS,preparations);
                            page = ConfigurationManager.getProperty("path.page.cart");
                        }
                        break;
                }


            } catch (ServiceException e) {
                request.setAttribute(ATTRIBUTE_ERROR_MESSAGE, MessageManager.getProperty("message.error.remove.product.cart", request.getLocale()));
                page = ConfigurationManager.getProperty("path.page.fail.result");
            }
        }else {
            request.setAttribute(ATTRIBUTE_ERROR_MESSAGE, MessageManager.getProperty("message.invalid.link",request.getLocale()));
            page = ConfigurationManager.getProperty("path.page.fail.result");
        }

        return page;
    }

    @Override
    public boolean isRedirect() {
        return isRedirect;
    }

    private void changeAttributeCountCart(HttpServletRequest request){
        Integer countProduct;
        countProduct = (Integer) request.getSession().getAttribute(COUNT_PRODUCT_CART_NAME);
        countProduct--;
        request.getSession().setAttribute(COUNT_PRODUCT_CART_NAME, countProduct);
    }
}
