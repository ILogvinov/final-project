package by.bsuir.pharma.command.cookie;

import by.bsuir.pharma.command.ActionCommand;

import javax.servlet.http.Cookie;

/**
 * Created by Иван on 12.06.2016.
 */
public abstract class CartCookieCommand implements ActionCommand {
    protected Cookie cookie;
    public Cookie getCartCookie(){
        return cookie;
    }
}
