package by.bsuir.pharma.command;

import by.bsuir.pharma.entity.Role;
import by.bsuir.pharma.entity.User;
import by.bsuir.pharma.service.BasketService;
import by.bsuir.pharma.service.ServiceException;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import by.bsuir.pharma.resource.ConfigurationManager;
import by.bsuir.pharma.service.LoginService;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

/**
 * Created by Иван on 27.03.2016.
 */
public class LoginCommand implements ActionCommand{
    private static final String PARAM_NAME_LOGIN = "login";
    private static final String PARAM_NAME_PASSWORD = "password";
    private static Logger logger = LogManager.getLogger(LoginCommand.class);
    @Override
    public String execute(HttpServletRequest request) {
        String page;

        User checkUser = new User();
        checkUser.setLogin(request.getParameter(PARAM_NAME_LOGIN));
        checkUser.setPassword(request.getParameter(PARAM_NAME_PASSWORD));


        try {
            User user = LoginService.loginUser(checkUser);
            if(user.getRole()== Role.CLIENT){
                Integer countProducts = BasketService.addProductToBasket(request.getCookies(),user.getLogin());
                request.getSession().setAttribute("countProductCart",countProducts);
            }else if(user.getRole()==Role.DOCTOR){

            }
            logger.info("Password and login are right");
            HttpSession session = request.getSession();
            session.setAttribute("user",user);
            session.setAttribute("role",user.getRole());
          page = ConfigurationManager.getProperty("path.page.main"+"_"+user.getRole().toString().toLowerCase());
        } catch (ServiceException e) {
            logger.warn("Invalidate password and login",e);
            page = ConfigurationManager.getProperty("path.page.start");
        }


    return page;
    }

    @Override
    public boolean isRedirect() {
        return false;
    }

}


