package by.bsuir.pharma.command;

import by.bsuir.pharma.entity.Recipe;
import by.bsuir.pharma.entity.User;
import by.bsuir.pharma.resource.ConfigurationManager;
import by.bsuir.pharma.resource.MessageManager;
import by.bsuir.pharma.service.RecipeService;
import by.bsuir.pharma.service.ServiceException;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

/**
 * Created by Иван on 13.06.2016.
 */
public class ShowRecipeClient implements ActionCommand {
    private static final String ATTRIBUTE_ERROR_MESSAGE = "errorMessage";
    private static final String ATTRIBUTE_RECIPES = "recipes";
    @Override
    public String execute(HttpServletRequest request) {
        String page;
        User user = (User) request.getSession().getAttribute("user");
        try {
            List<Recipe> recipes = RecipeService.findRecipeClient(user.getLogin());
            request.setAttribute(ATTRIBUTE_RECIPES, recipes);
            page = ConfigurationManager.getProperty("path.page.client.list.recipe");
        } catch (ServiceException e) {
            request.setAttribute(ATTRIBUTE_ERROR_MESSAGE, MessageManager.getProperty("message.error.show.recipes.client", request.getLocale()));
            page = ConfigurationManager.getProperty("path.page.fail.result");
        }
        return page;
    }

    @Override
    public boolean isRedirect() {
        return false;
    }
}
