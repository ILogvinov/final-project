package by.bsuir.pharma.command;

import by.bsuir.pharma.resource.ConfigurationManager;
import by.bsuir.pharma.resource.MessageManager;
import by.bsuir.pharma.service.PreparationService;
import by.bsuir.pharma.service.ServiceException;

import javax.servlet.http.HttpServletRequest;

/**
 * Created by Иван on 20.06.2016.
 */
public class DeletePreparationCommand implements ActionCommand {
    private boolean isRedirect = false;
    private static final String ID_PREPARATION_REGEXP = "^[\\w\\d]{9,10}$";
    private static final String PARAMETER_ID_NUMBER = "idNumber";


    @Override
    public String execute(HttpServletRequest request) {
        String page;
        String idNumber = request.getParameter(PARAMETER_ID_NUMBER);
        if(!idNumber.isEmpty()||checkRegexp(idNumber,ID_PREPARATION_REGEXP)){
            try {
                if(PreparationService.deletePreparation(idNumber)){
                    isRedirect = true;
                }
                page = ConfigurationManager.getProperty("path.link.show.list.preparation");
            } catch (ServiceException e) {
                page = ConfigurationManager.getProperty("path.link.show.list.preparation");
                request.setAttribute("errorMessage", MessageManager.getProperty("message.error.delete.preparation", request.getLocale()));
            }
        }else {
            request.setAttribute("errorMessage", MessageManager.getProperty("message.invalidate.data.preparation.delete", request.getLocale()));
            page = ConfigurationManager.getProperty("path.page.fail.result");
        }
        return page;
    }

    @Override
    public boolean isRedirect() {
        return isRedirect;
    }
}
