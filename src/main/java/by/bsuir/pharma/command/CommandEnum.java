package by.bsuir.pharma.command;

/**
 * Created by Иван on 27.03.2016.
 */
public enum CommandEnum {

    LOGIN {
        {
            this.command = new LoginCommand();
        }
    }, LOGOUT {

        {
            this.command = new LogoutCommand();
        }
    },
    LOCALIZATION {
        {
            this.command = new LocaleCommand();
        }
    },

    RESULT_OUT {
        {
            this.command = new ResultOutCommand();
        }
    },
    REGISTRATE {
        {
            this.command = new RegistryCommand();
        }
    },
    START {
        {
            this.command = new StartCommand();
        }
    },
    CHANGE_INFORMATION {
        {
            this.command = new ChangeInformationCommand();
        }
    },
    OPEN_PAGE {
        {
            this.command = new OpenPageCommand();
        }
    },
    SHOW_CATALOG {
        {
            this.command = new ShowCatalogCommand();
        }
    },
    FILTER_CATALOG {
        {
            this.command = new FilterCatalogCommand();
        }
    },
    SHOW_PREPARATION_INF {
        {
            this.command = new ViewInfPreparationCommand();
        }
    },
    ADD_TO_CART {
        {
            this.command = new AddProductToBasket();
        }
    },
    SHOW_CART {
        {
            this.command = new ShowCartCommand();
        }
    },
    REMOVE_PRODUCT_CART {
        {
            this.command = new RemoveProductCartCommand();
        }
    },
    UPDATE_PRODUCT_COMMAND{
        {
            this.command = new UpdateProductCartCommand();
        }
    },
    CHECK_OUT{
        {
            this.command = new CheckOutCommand();
        }
    },
    FORM_ORDER{
        {
            this.command = new FormOrderCommand();
        }
    },
    SHOW_ORDER{
        {
            this.command = new ShowOrderCommand();
        }
    },
    SHOW_DETAILED_ORDER{
        {
            this.command = new ShowDetailedOrderCommand();
        }
    },
    SHOW_PHARMACIST_ORDER{
        {
            this.command = new ShowOrderPharmacistCommand();
        }
    },
    UPDATE_ORDER_STATUS{
        {
            this.command = new UpdateStatusOrderCommand();
        }
    },
    ADD_NEW_PRODUCT{
        {
            this.command = new AddNewProductCommand();
        }
    },
    ADD_NEW_PREPARATION{
        {
            this.command = new AddNewPreparationCommand();
        }
    },
    DELETE_PRODUCT{
        {
            this.command = new DeleteProductCommand();
        }
    },
    CHANGE_COUNT_PRODUCT{
        {
            this.command = new ChangeCountPreparationCommand();
        }
    },
    SHOW_LIST_PREPARATION{
        {
            this.command = new ShowListPreparationCommand();
        }
    },
    REGISTER_RECIPE{
        {
            this.command = new AddNewRecipeCommand();
        }
    },
    SHOW_RECIPE_INF{
        {
            this.command = new ShowInfOfRecipe();
        }
    },
    SHOW_REQUEST{
        {
            this.command = new ShowRequestCommand();
        }
    },
    CHANGE_REQUEST_STATUS{
        {
            this.command = new ChangeStatusRequestOfRecipe();
        }
    },
    SHOW_RECIPE_LIST{
        {
            this.command = new ShowRecipeClient();
        }
    },
    REGISTER_REQUEST_RECIPE{
        {
            this.command = new RegisterRequestToRecipe();
        }
    },
    ATTACH_RECIPE{
        {
            this.command = new AttachRecipeOrderCommand();
        }
    },
    SEARCH_PREPARATION{
        {
            this.command = new SearchPreparationCommand();
        }
    },
    SHOW_LIST_PRODUCT{
        {
            this.command = new ShowListProductCommand();
        }
    },
    REGISTER_WORKER{
        {
            this.command = new RegistryWorkerCommand();
        }
    },
    DELETE_USER{
        {
            this.command = new DeleteUserCommand();
        }
    },
    SHOW_WORKERS{
        {
            this.command = new ShowWorkerListCommand();
        }
    },
    SHOW_CLOSED_ORDERS{
        {
            this.command = new ShowClosedOrdersCommand();
        }
    }
    ;

    ActionCommand command;

    public ActionCommand getCurrentCommand() {
        return command;
    }
}
