package by.bsuir.pharma.command;

import by.bsuir.pharma.command.cookie.CartCookieCommand;
import by.bsuir.pharma.entity.Role;
import by.bsuir.pharma.resource.ConfigurationManager;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;

/**
 * Created by Иван on 27.03.2016.
 */
public class LogoutCommand extends CartCookieCommand {
    private static final String ATTRIBUTE_ROLE = "role";
    private static final String ATTRIBUTE_CART_COOKIE = "cart_store";

    @Override
    public String execute(HttpServletRequest request) {
        String page = ConfigurationManager.getProperty("path.page.index");
        Role role = (Role) request.getSession().getAttribute(ATTRIBUTE_ROLE);

        for(Cookie oldCookie:request.getCookies()){
            if(ATTRIBUTE_CART_COOKIE.equals(oldCookie.getName())){
                cookie=oldCookie;
            }
        }
        if(role==Role.CLIENT && cookie!=null){
            cookie.setMaxAge(0);
        }
        request.getSession().setAttribute(ATTRIBUTE_ROLE,Role.GUEST);
        return page;
    }

    @Override
    public boolean isRedirect() {
        return true;
    }
}
