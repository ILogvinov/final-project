package by.bsuir.pharma.command;

import by.bsuir.pharma.entity.CharacteristicPreparation;
import by.bsuir.pharma.entity.Preparation;
import by.bsuir.pharma.resource.ConfigurationManager;
import by.bsuir.pharma.resource.MessageManager;
import by.bsuir.pharma.service.PreparationService;
import by.bsuir.pharma.service.ServiceException;

import javax.servlet.http.HttpServletRequest;
import java.util.HashMap;
import java.util.List;

/**
 * Created by Иван on 04.05.2016.
 */
public class FilterCatalogCommand implements ActionCommand {
    private final String TYPE_PREPARATION = "type_preparation";
    private final String TYPE_PACKAGING = "type_packing";
    private final String MIN_PRICE = "min_price";
    private final String MAX_PRICE = "max_price";
    private final String IS_REQUIRE_RECIPE = "is_require_recipe";

    @Override
    public String execute(HttpServletRequest request) {
        String page = null;
        HashMap<CharacteristicPreparation,String> characteristic = new HashMap<>();

        characteristic.put(CharacteristicPreparation.GROUP,request.getParameter(CharacteristicPreparation.GROUP.getValue()));
        characteristic.put(CharacteristicPreparation.IS_REQUIRE_RECIPE,request.getParameter(CharacteristicPreparation.IS_REQUIRE_RECIPE.getValue()));
        characteristic.put(CharacteristicPreparation.MIN_PRICE,request.getParameter(CharacteristicPreparation.MIN_PRICE.getValue()));
        characteristic.put(CharacteristicPreparation.MAX_PRICE,request.getParameter(CharacteristicPreparation.MAX_PRICE.getValue()));

        try {
            PreparationService service = new PreparationService();
            List<Preparation> preparations = service.giveFilteredPreparation(characteristic);
            request.setAttribute("preparations",preparations);
            page = ConfigurationManager.getProperty("path.page.catalog");
        } catch (ServiceException e) {
            //logger.warn("Invalidate password and login",e);
            request.setAttribute("errorMessage", MessageManager.getProperty("message.loginerror",request.getLocale()));
            page = ConfigurationManager.getProperty("path.page.login");
        }


        return page;

    }

    @Override
    public boolean isRedirect() {
        return false;
    }
}
