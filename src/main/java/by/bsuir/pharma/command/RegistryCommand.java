package by.bsuir.pharma.command;

import by.bsuir.pharma.entity.Role;
import by.bsuir.pharma.entity.User;
import by.bsuir.pharma.resource.ConfigurationManager;
import by.bsuir.pharma.resource.MessageManager;
import by.bsuir.pharma.service.RegistrateService;
import by.bsuir.pharma.service.ServiceException;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.util.Locale;

/**
 * Created by Иван on 10.04.2016.
 */
public class RegistryCommand implements ActionCommand {
    private boolean isRedirect;
    private static final String PARAM_NAME_LOGIN = "login";
    private static final String PARAM_NAME_PASSWORD = "password";
    private static final String PARAM_REPEAT_PASSWORD = "repeat_password";
    private static final String PARAM_EMAIL = "email";
    private static final String PARAM_PHONE_NUMBER = "phone_number";
    private static final String PARAM_NAME = "name";
    private static final String PARAM_SURNAME = "surname";

    private static final String ATTRIBUTE_ROLE = "role";
    private static final String ATTRIBUTE_ERROR_MESSAGE = "errorMessage";
    private static final String ATTRIBUTE_USER = "user";

    private static final String NAME_REGEXP = "^[A-Za-zА-Яа-я]{2,15}$";
    private static final String SURNAME_REGEXP = "^[A-Za-zА-Яа-я\\-]{2,25}$";
    private static final String PASSWORD_REGEXP = "^[\\w\\d]{7,20}";
    private static final String ROLE_REGEXP = "^[DOCTOR|PHARMACIST]$";
    private static final String LOGIN_REGEXP = "^[\\w\\d\\_]{7,20}$";
    private static final String EMAIL_REGEXP = "^[A-Za-z0-9._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,6}$";
    private static final String PHONE_REGEXP = "\\+?[\\d]{10,14}";

    private static Logger logger = LogManager.getLogger(RegistryCommand.class);

    @Override
    public String execute(HttpServletRequest request) {
        String page;

        User checkUser = new User();
        if(validate(request,checkUser)){
            try {
                if(RegistrateService.checkData(checkUser)){
                    logger.info("Password and login are right");

                    checkUser.setRole(Role.CLIENT);
                    HttpSession session = request.getSession();
                    session.setAttribute(ATTRIBUTE_USER,checkUser);
                    session.setAttribute(ATTRIBUTE_ROLE,Role.CLIENT);
                    page = ConfigurationManager.getProperty("path.page.main"+"_"+checkUser.getRole().toString().toLowerCase());
                }
                else {
                    logger.warn("Invalidate password and login");
                    request.getSession().setAttribute(ATTRIBUTE_ERROR_MESSAGE, MessageManager.getProperty("message.wrong.registerdate", request.getLocale()));
                    page = ConfigurationManager.getProperty("path.page.registrate");
                }

            } catch (ServiceException e) {
                logger.warn("Invalidate password and login",e);
                request.setAttribute(ATTRIBUTE_ERROR_MESSAGE, MessageManager.getProperty("wrong.registerlogin", request.getLocale()));
                page = ConfigurationManager.getProperty("path.page.registrate");
            }


        }else {
            page = ConfigurationManager.getProperty("path.page.registrate");
        }

        return page;
    }

    @Override
    public boolean isRedirect() {
        return false;
    }

    private boolean validate(HttpServletRequest request, User user){
        boolean result = true;
        Locale locale = (Locale) request.getSession().getAttribute("Locale");
        String param = request.getParameter(PARAM_NAME_LOGIN);
        if(!param.isEmpty()&&checkRegexp(param,LOGIN_REGEXP)){
            user.setLogin(param);
        }else {
            request.setAttribute("invalid_login",MessageManager.getProperty("message.wrong.format.login",locale));
            result = false;
        }

        param = request.getParameter(PARAM_NAME_PASSWORD);
        if(!param.isEmpty()&&checkRegexp(param,PASSWORD_REGEXP)){
            user.setPassword(param);
        }else {
            request.setAttribute("invalid_password",MessageManager.getProperty("message.wrong.format.password",locale));
            result = false;
        }

        param = request.getParameter(PARAM_REPEAT_PASSWORD);
        if(param.isEmpty()){
            request.setAttribute("invalid_repeat_password",MessageManager.getProperty("message.wrong.format.password",locale));
        }else {
            if(!param.equals(user.getPassword())){
                request.setAttribute("invalid_repeat_password",MessageManager.getProperty("message.wrong.repeat.password",locale));
            }
        }

        param = request.getParameter(PARAM_PHONE_NUMBER);
        if(!param.isEmpty()&&checkRegexp(param,PHONE_REGEXP)){
            user.setPhoneNumber(param);
        }else {
            request.setAttribute("invalid_phone",MessageManager.getProperty("message.wrong.format.phone",locale));
            result = false;
        }

        param = request.getParameter(PARAM_EMAIL);
        if(!param.isEmpty()&&checkRegexp(param,EMAIL_REGEXP)){
            user.setEmail(param);
        }else {
            request.setAttribute("invalid_email",MessageManager.getProperty("message.wrong.format.email",locale));
            result = false;
        }

        param = request.getParameter(PARAM_NAME);
        if(!param.isEmpty()&&checkRegexp(param,NAME_REGEXP)){
            user.setName(param);
        }else {
            request.setAttribute("invalid_name",MessageManager.getProperty("message.wrong.format.name",locale));
            result = false;
        }

        param = request.getParameter(PARAM_SURNAME);
        if(!param.isEmpty()&&checkRegexp(param,SURNAME_REGEXP)){
            user.setSurname(param);
        }else {
            request.setAttribute("invalid_surname",MessageManager.getProperty("message.wrong.format.surname",locale));
            result = false;
        }

        return result;
    }
}
