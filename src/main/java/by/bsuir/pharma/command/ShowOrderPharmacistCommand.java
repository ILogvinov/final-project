package by.bsuir.pharma.command;

import by.bsuir.pharma.entity.Order;
import by.bsuir.pharma.resource.ConfigurationManager;
import by.bsuir.pharma.resource.MessageManager;
import by.bsuir.pharma.service.OrderService;
import by.bsuir.pharma.service.ServiceException;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

/**
 * Created by Иван on 13.05.2016.
 */
public class ShowOrderPharmacistCommand implements ActionCommand {
    private static final String ATTRIBUTE_ORDERS = "orders";
    private static final String ATTRIBUTE_ERROR_MESSAGE = "errorMessage";

    private static final String ORDER_STATUS_PROCESSING = "PROCESSING";
    @Override
    public String execute(HttpServletRequest request) {
        String page;
        try {
            List<Order> orders = OrderService.findOrderByStatus(ORDER_STATUS_PROCESSING);
            request.setAttribute(ATTRIBUTE_ORDERS, orders);
            page = ConfigurationManager.getProperty("path.page.pharmacist.orders");
        } catch (ServiceException e) {
            request.setAttribute(ATTRIBUTE_ERROR_MESSAGE, MessageManager.getProperty("message.error.show.order", request.getLocale()));
            page = ConfigurationManager.getProperty("path.page.fail.result");
        }
        return page;
    }

    @Override
    public boolean isRedirect() {
        return false;
    }
}
