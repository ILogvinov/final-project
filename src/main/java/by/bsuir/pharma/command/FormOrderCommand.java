package by.bsuir.pharma.command;

import by.bsuir.pharma.entity.User;
import by.bsuir.pharma.resource.ConfigurationManager;
import by.bsuir.pharma.resource.MessageManager;
import by.bsuir.pharma.service.OrderService;
import by.bsuir.pharma.service.ServiceException;

import javax.servlet.http.HttpServletRequest;

/**
 * Created by Иван on 13.05.2016.
 */
public class FormOrderCommand implements ActionCommand {
    private boolean isRedirect;
    private static final String PAYMENT_METHOD_REGEX = "(BANK|CASH)";
    @Override
    public String execute(HttpServletRequest request) {
        String page = null;
        String address = request.getParameter("city")+","+request.getParameter("street")+","+request.getParameter("numberHouse");
        String paymentMethod = request.getParameter("paymentMethod");
        if(validate(address,paymentMethod)){
            try {
                User user = (User) request.getSession().getAttribute("user");
                String login = user.getLogin();

                if(OrderService.addOrder(login,address,paymentMethod)){
                    page = ConfigurationManager.getProperty("path.page.success.order");
                    isRedirect = true;
                    request.getSession().setAttribute("countProductCart",0);
                }else {
                    page = request.getHeader("referer");
                    isRedirect = false;
                }
            } catch (ServiceException e) {
                request.setAttribute("errorMessage", MessageManager.getProperty("message.loginerror", request.getLocale()));
                page = ConfigurationManager.getProperty("path.page.registrate");
            }
        }else {
            page = request.getHeader("referer");
        }
        return page;

    }

    @Override
    public boolean isRedirect() {
        return isRedirect;
    }

    private boolean validate(String address, String paymentMethod){
        return !address.isEmpty()&&!paymentMethod.isEmpty()&&checkRegexp(paymentMethod,PAYMENT_METHOD_REGEX);
    }
}
