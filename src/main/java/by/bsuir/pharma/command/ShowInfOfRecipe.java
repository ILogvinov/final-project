package by.bsuir.pharma.command;

import by.bsuir.pharma.entity.Recipe;
import by.bsuir.pharma.entity.Role;
import by.bsuir.pharma.resource.ConfigurationManager;
import by.bsuir.pharma.resource.MessageManager;
import by.bsuir.pharma.service.RecipeService;
import by.bsuir.pharma.service.ServiceException;

import javax.servlet.http.HttpServletRequest;

/**
 * Created by Иван on 04.06.2016.
 */
public class ShowInfOfRecipe implements ActionCommand {
    private static final String RECIPE_ID_REGEXP = "^\\d{10}$";

    private static final String PARAMETER_ID_RECIPE = "idRecipe";

    private static final String ATTRIBUTE_ROLE = "role";
    private static final String ATTRIBUTE_RECIPE = "recipe";
    private static final String ATTRIBUTE_ERROR_MESSAGE = "errorMessage";


    @Override
    public String execute(HttpServletRequest request) {
        String page;
        String paramIdRecipe = request.getParameter(PARAMETER_ID_RECIPE);

        if (!paramIdRecipe.isEmpty() && checkRegexp(paramIdRecipe, RECIPE_ID_REGEXP)) {
            RecipeService recipeService = new RecipeService();
            Integer idRecipe = Integer.valueOf(paramIdRecipe);
            Role role = (Role) request.getSession().getAttribute(ATTRIBUTE_ROLE);
            try {
                Recipe recipe = recipeService.findRecipe(idRecipe);
                request.setAttribute(ATTRIBUTE_RECIPE, recipe);
                if (role == Role.DOCTOR) {
                    page = ConfigurationManager.getProperty("path.doctor.recipe.inf");
                } else {
                    page = ConfigurationManager.getProperty("path.page.client.recipe.inf");
                }

            } catch (ServiceException e) {
                page = ConfigurationManager.getProperty("path.page.main_doctor");
                request.setAttribute(ATTRIBUTE_ERROR_MESSAGE, MessageManager.getProperty("message.error.show.recipe"));
            }
        } else {
            request.setAttribute(ATTRIBUTE_ERROR_MESSAGE, MessageManager.getProperty("message.invalid.link",request.getLocale()));
            page = ConfigurationManager.getProperty("path.page.fail.result");
        }

        return page;
    }

    @Override
    public boolean isRedirect() {
        return false;
    }
}
