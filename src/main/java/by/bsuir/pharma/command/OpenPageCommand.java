package by.bsuir.pharma.command;

import by.bsuir.pharma.entity.Role;
import by.bsuir.pharma.resource.ConfigurationManager;

import javax.servlet.http.HttpServletRequest;

/**
 * Created by Иван on 25.04.2016.
 */
public class OpenPageCommand implements ActionCommand {
    private final static String PAGE = "page";
    private final static String ROLE = "role";
    @Override
    public String execute(HttpServletRequest request) {
        String page;
        Role role = (Role) request.getSession().getAttribute(ROLE);
        page = request.getParameter(PAGE);
        page = ConfigurationManager.getProperty("path.page."+role.getValue()+"."+page);
        return page;
    }

    @Override
    public boolean isRedirect() {
        return false;
    }
}
