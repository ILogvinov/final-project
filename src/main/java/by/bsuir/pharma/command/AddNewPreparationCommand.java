package by.bsuir.pharma.command;

import by.bsuir.pharma.entity.Preparation;
import by.bsuir.pharma.resource.ConfigurationManager;
import by.bsuir.pharma.resource.MessageManager;
import by.bsuir.pharma.service.PreparationService;
import by.bsuir.pharma.service.ServiceException;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.servlet.http.HttpServletRequest;

/**
 * Created by Иван on 13.05.2016.
 */
public class AddNewPreparationCommand implements ActionCommand {

    private static final String ID_PREPARATION_REGEXP = "^[\\w\\d]{9,10}$";
    private static final String PREPARATION_NAME_REGEXP = "^[\\w\\s-]+$";
    private static final String PREPARATION_CHARACTERISTIC_REGEX = ".{10,1000}";
    private static final String PREPARATION_MANUFACTURE_REGEXP = "^[\\d]{2,7}$";
    private static final String PREPARATION_NUMERIC_REGEXP = "^[\\d]{1,2}(\\.[\\d]{1,3})?$";

    private static final String PARAM_ID = "id";
    private static final String PARAM_PREPARATION_NAME = "name";
    private static final String PARAM_PREPARATION_GROUP = "group";
    private static final String PARAM_PREPARATION_MANUFACTURE = "manufacture";
    private static final String PARAM_PREPARATION_DOSAGE = "dosage";
    private static final String PARAM_PREPARATION_COUNT_UNITS = "countUnits";
    private static final String PARAM_PREPARATION_TESTIMONY = "testimony";
    private static final String PARAM_PREPARATION_EFFECT = "effect";
    private static final String PARAM_PREPARATION_CONTRAINDICATION = "contraindications";
    private static final String PARAM_PREPARATION_MODE = "mode";

    private static final String ATTRIBUTE_ID_PREPARATION = "idPreparation";
    private static final String ATTRIBUTE_ERROR_MESSAGE = "errorMessage";

    private boolean isRedirect;
    private static Logger logger = LogManager.getLogger(AddNewPreparationCommand.class);


    /**
     * Returns a string which contains relative path new preparation image page. Validates preparation
     * information which contains in HTTP request object ({@link HttpServletRequest}). If validation failed returns a
     * string which contains relative path to new preparation page file. Else the Preparation object filled and send to PreparationService.
     * If save image failed created message page with
     * message about fail.
     *
     * @param request the HTTP request object which contains information about order
     * @return a string which contains relative path to message page or to new order page
     * @see Preparation
     * @see PreparationService
     */


    @Override
    public String execute(HttpServletRequest request) {
        Preparation preparation = new Preparation();
        String page;
        try {
            if (validate(request, preparation)) {
                PreparationService.addNewPreparation(preparation);
                request.getSession().setAttribute(ATTRIBUTE_ID_PREPARATION, preparation.getId());
                page = ConfigurationManager.getProperty("path.page.preparation.image");
                isRedirect = true;
            } else {
                page = ConfigurationManager.getProperty("path.page.pharmacist.new.preparation");
                logger.warn("Invalidate parameters of new preparation");
            }
        } catch (ServiceException e) {
            logger.error("Error of adding new preparation",e);
            request.setAttribute(ATTRIBUTE_ERROR_MESSAGE, MessageManager.getProperty("message.fail.add.preparation", request.getLocale()));
            page = ConfigurationManager.getProperty("path.page.pharmacist.fail");
        }
        return page;
    }

    @Override
    public boolean isRedirect() {
        return isRedirect;
    }


    private boolean validate(HttpServletRequest request, Preparation preparation) {
        boolean result = true;
        String param = request.getParameter(PARAM_ID);
        if (param.isEmpty() || !checkRegexp(param, ID_PREPARATION_REGEXP)) {
            result = false;
            request.setAttribute("invalid_id", MessageManager.getProperty("message.wrong.preparation.id", request.getLocale()));
        } else {
            preparation.setId(param);
        }

        param = request.getParameter(PARAM_PREPARATION_GROUP);
        if (param.isEmpty()) {
            result = false;
            request.setAttribute("invalid_group", MessageManager.getProperty("message.wrong.group.preparation", request.getLocale()));
        } else {
            preparation.setGroup(param);

        }


        param = request.getParameter(PARAM_PREPARATION_NAME);
        if (param.isEmpty() || !checkRegexp(param, PREPARATION_NAME_REGEXP)) {
            result = false;
            request.setAttribute("invalid_name", MessageManager.getProperty("message.wrong.parametr.name", request.getLocale()));
        } else {
            preparation.setName(param);
        }

        param = request.getParameter(PARAM_PREPARATION_MANUFACTURE);
        if (param.isEmpty() || !checkRegexp(param, PREPARATION_MANUFACTURE_REGEXP)) {
            result = false;
            request.setAttribute("invalid_manufacture", MessageManager.getProperty("message.wrong.manufacture", request.getLocale()));

        } else {
            preparation.setManufactureID(Integer.valueOf(param));
        }

        param = request.getParameter(PARAM_PREPARATION_TESTIMONY);
        if (param.isEmpty() || !checkRegexp(param, PREPARATION_CHARACTERISTIC_REGEX)) {
            result = false;
            request.setAttribute("invalid_testimony", MessageManager.getProperty("message.wrong.testimony", request.getLocale()));
        } else {
            preparation.setTestimony(param);
        }

        param = request.getParameter(PARAM_PREPARATION_EFFECT);
        if (param.isEmpty() || !checkRegexp(param, PREPARATION_CHARACTERISTIC_REGEX)) {
            result = false;
            request.setAttribute("invalid_effect", MessageManager.getProperty("message.wrong.param.effect", request.getLocale()));
        } else {
            preparation.setPharmacologicalEffect(param);
        }

        param = request.getParameter(PARAM_PREPARATION_CONTRAINDICATION);
        if (param.isEmpty() || !checkRegexp(param, PREPARATION_CHARACTERISTIC_REGEX)) {
            request.setAttribute("invalid_contraindication", MessageManager.getProperty("message.wrong.param.contraindication", request.getLocale()));
            result = false;
        } else {
            preparation.setContraindications(param);
        }

        param = request.getParameter(PARAM_PREPARATION_MODE);
        if (param.isEmpty() || !checkRegexp(param, PREPARATION_CHARACTERISTIC_REGEX)) {
            result = false;
            request.setAttribute("invalid_mode", MessageManager.getProperty("message.wrong.parametr.mode", request.getLocale()));
        } else {
            preparation.setModeOfApplication(param);
        }

        param = request.getParameter(PARAM_PREPARATION_DOSAGE);
        if (param.isEmpty() || !checkRegexp(param, PREPARATION_NUMERIC_REGEXP)) {
            result = false;
            request.setAttribute("invalid_dosage", MessageManager.getProperty("message.wrong.fomat.dosage", request.getLocale()));
        } else {
            preparation.setDosage(Float.valueOf(param));
        }

        param = request.getParameter(PARAM_PREPARATION_COUNT_UNITS);
        if (param.isEmpty() || !checkRegexp(param, PREPARATION_NUMERIC_REGEXP)) {
            result = false;
            request.setAttribute("invalid_count_units", MessageManager.getProperty("message.wrong.format.count.units", request.getLocale()));
        } else {
            preparation.setCountUnits(Float.valueOf(param));
        }
        return result;
    }
}
