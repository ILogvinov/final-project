package by.bsuir.pharma.command;

import by.bsuir.pharma.entity.RequestToRecipe;
import by.bsuir.pharma.entity.User;
import by.bsuir.pharma.resource.ConfigurationManager;
import by.bsuir.pharma.resource.MessageManager;
import by.bsuir.pharma.service.RequestToRecipeService;
import by.bsuir.pharma.service.ServiceException;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

/**
 * Created by Иван on 04.06.2016.
 */
public class ShowRequestCommand implements ActionCommand {
    @Override
    public String execute(HttpServletRequest request) {
        String page;
        User user = (User) request.getSession().getAttribute("user");
        String status = request.getParameter("status");
        if(status==null){
            status = "PROCESSING";
        }
        RequestToRecipeService service = new RequestToRecipeService();
        try {
            List<RequestToRecipe> requestToRecipeList = service.giveDoctorRequests(user.getLogin(), status);
            request.setAttribute("requests", requestToRecipeList);
            page = ConfigurationManager.getProperty("path.page.doctor.list.request");
        } catch (ServiceException e) {
            page = ConfigurationManager.getProperty("path.page.fail.result");
            request.setAttribute("errorMessage", MessageManager.getProperty("message.error.show.requests"));
        }
        return page;
    }

    @Override
    public boolean isRedirect() {
        return false;
    }
}
