package by.bsuir.pharma.command;

import by.bsuir.pharma.resource.ConfigurationManager;
import by.bsuir.pharma.service.PreparationService;
import by.bsuir.pharma.service.ServiceException;
import org.apache.commons.fileupload.FileItem;
import org.apache.commons.fileupload.FileUploadException;
import org.apache.commons.fileupload.disk.DiskFileItemFactory;
import org.apache.commons.fileupload.servlet.ServletFileUpload;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.servlet.http.HttpServletRequest;
import java.io.File;
import java.util.List;

/**
 * Created by Иван on 20.06.2016.
 */
public class AddImagePreparationCommand implements ActionCommand {
    private static final String ID_PREPARATION = "idPreparation";
    private static final String PROPERTY_NAME = "java.io.tmpdir";
    private static final String UPLOAD_SUBDIRECTORY = "img";
    private static final String UPLOAD_DIRECTORY = "preparation";
    private static final int MEMORY_THRESHOLD = 3 * 1024 * 1024; // 3 MB
    private static final int MAX_FILE_SIZE = 10 * 1024 * 1024; // 10 MB
    private static final int MAX_REQUEST_SIZE = 20 * 1024 * 1024; // 20 MB
    private static Logger logger = LogManager.getLogger(AddImagePreparationCommand.class);

    @Override
    public String execute(HttpServletRequest request) {
        String page = null;
        String idPreparation = (String) request.getSession().getAttribute(ID_PREPARATION);
        request.getSession().removeAttribute(ID_PREPARATION);
        String imageName = uploadImage(request);
        if ((imageName != null) && (idPreparation != null)) {
            try {

                PreparationService.addNewPreparationImage(idPreparation, imageName);
                page = ConfigurationManager.getProperty("path.link.show.list.preparation");

            } catch (ServiceException e) {
                e.printStackTrace();
            }
        } else {
            page = request.getHeader("referer");
        }
    return page;
    }

    @Override
    public boolean isRedirect() {
        return false;
    }

    private String uploadImage(HttpServletRequest request) {
        String path = null;

        DiskFileItemFactory factory = new DiskFileItemFactory();
        factory.setSizeThreshold(MEMORY_THRESHOLD);
        String s = System.getProperty(PROPERTY_NAME);
        factory.setRepository(new File(System.getProperty(PROPERTY_NAME)));

        ServletFileUpload upload = new ServletFileUpload(factory);
        upload.setFileSizeMax(MAX_FILE_SIZE);
        upload.setSizeMax(MAX_REQUEST_SIZE);

        try {
            List fileItems = upload.parseRequest(request);
            for (Object item : fileItems) {
                FileItem fileItem = (FileItem) item;
                if (!fileItem.isFormField()) {
                    String uploadPath = request.getServletContext().getRealPath("") + UPLOAD_SUBDIRECTORY + File.separator;
                    File uploadDir = new File(uploadPath);
                    if (!uploadDir.exists()) {
                        uploadDir.mkdir();
                    }
                    uploadPath += UPLOAD_DIRECTORY + File.separator;
                    uploadDir = new File(uploadPath);
                    if (!uploadDir.exists()) {
                        uploadDir.mkdir();
                    }

                    String fileName = fileItem.getName();
                    File file = new File(uploadDir + File.separator + fileName);
                    fileItem.write(file);
                    path = fileName ;

                }
            }
        } catch (FileUploadException ex) {
            logger.error("File upload fail", ex);
        } catch (Exception ex) {
            logger.error("FileItem write error", ex);
        }

        return path;
    }
}
