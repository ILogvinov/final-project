package by.bsuir.pharma.command;

import by.bsuir.pharma.command.cookie.CartCookieCommand;
import by.bsuir.pharma.entity.Role;
import by.bsuir.pharma.entity.User;
import by.bsuir.pharma.resource.ConfigurationManager;
import by.bsuir.pharma.resource.MessageManager;
import by.bsuir.pharma.service.BasketService;
import by.bsuir.pharma.service.ServiceException;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;

/**
 * Created by Иван on 12.05.2016.
 */
public class UpdateProductCartCommand extends CartCookieCommand {
    private boolean isRedirect;

    private static final String PRODUCT_ID_REGEXP = "^[\\d]{9}$";
    private static final String PRODUCT_COUNT_REGEXP = "^[\\d]{1,3}(\\.[\\d]{1})?$";

    private static final String PARAM_ID_NAME = "idProduct";
    private static final String PARAM_QUANTITY_NAME = "quantity";
    private static final String ATTRIBUTE_ROLE_NAME = "role";
    private static final String ATTRIBUTE_USER_NAME = "user";

    private static final String ATTRIBUTE_ERROR_MESSAGE = "errorMessage";

    @Override
    public String execute(HttpServletRequest request) {
        String page;

        String paramIdProduct = request.getParameter(PARAM_ID_NAME);
        String paramCount = request.getParameter(PARAM_QUANTITY_NAME);
        if(validate(paramIdProduct,paramCount)){
            Integer idProduct = Integer.valueOf(paramIdProduct);
            Float count = Float.valueOf(paramCount);
            try {
                switch ((Role)request.getSession().getAttribute(ATTRIBUTE_ROLE_NAME)) {
                    case GUEST:
                        Cookie[] cookies = request.getCookies();
                        cookie = BasketService.updateProduct(count,idProduct,cookies);
                        break;
                    case CLIENT:
                        User user = (User)request.getSession().getAttribute(ATTRIBUTE_USER_NAME);
                        String login = user.getLogin();
                        BasketService.updateProduct(login,count,idProduct);
                        break;
                }
                isRedirect = true;
                page = request.getHeader("referer");
            } catch (ServiceException e) {
                request.setAttribute(ATTRIBUTE_ERROR_MESSAGE, MessageManager.getProperty("message.invalid.link",request.getLocale()));
                page = ConfigurationManager.getProperty("path.page.fail.result");
            }
        }else {
            request.setAttribute(ATTRIBUTE_ERROR_MESSAGE, MessageManager.getProperty("message.invalid.link",request.getLocale()));
            page = ConfigurationManager.getProperty("path.page.fail.result");
        }

        return page;
    }

    @Override
    public boolean isRedirect() {
        return isRedirect;
    }

    private boolean validate(String idProduct,String count){
        return !(idProduct.isEmpty() || !checkRegexp(idProduct, PRODUCT_ID_REGEXP)) && !(count.isEmpty() || !checkRegexp(count, PRODUCT_COUNT_REGEXP));
    }



}
