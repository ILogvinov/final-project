package by.bsuir.pharma.command;

import by.bsuir.pharma.entity.Preparation;
import by.bsuir.pharma.resource.ConfigurationManager;
import by.bsuir.pharma.resource.MessageManager;
import by.bsuir.pharma.service.PreparationService;
import by.bsuir.pharma.service.ServiceException;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

/**
 * Created by Иван on 15.05.2016.
 */
public class ShowListPreparationCommand implements ActionCommand {
    private static final String ATTRIBUTE_ERROR_MESSAGE = "errorMessage";
    private static final String ATTRIBUTE_PREPARATIONS = "preparations";

    @Override
    public String execute(HttpServletRequest request) {
        String page;

        try {
            List<Preparation> preparations = PreparationService.findAll();
            request.setAttribute(ATTRIBUTE_PREPARATIONS,preparations);
            page = ConfigurationManager.getProperty("path.page.list.preparation");
        } catch (ServiceException e) {

            request.setAttribute(ATTRIBUTE_ERROR_MESSAGE, MessageManager.getProperty("message.error.show.list.preparation", request.getLocale()));
            page = ConfigurationManager.getProperty("path.page.fail.result");
        }


        return page;
    }

    @Override
    public boolean isRedirect() {
        return false;
    }
}
