package by.bsuir.pharma.command;

import by.bsuir.pharma.entity.Recipe;
import by.bsuir.pharma.entity.User;
import by.bsuir.pharma.resource.ConfigurationManager;
import by.bsuir.pharma.resource.MessageManager;
import by.bsuir.pharma.service.RecipeService;
import by.bsuir.pharma.service.ServiceException;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.servlet.http.HttpServletRequest;
import java.sql.Date;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;

/**
 * Created by Иван on 04.06.2016.
 */
public class AddNewRecipeCommand implements ActionCommand {
    private boolean isRedirect;

    private static Logger logger = LogManager.getLogger(AddNewRecipeCommand.class);

    private static final String RECIPE_ID_REGEXP = "^\\d{8,10}$";
    private static final String RECIPE_DOSAGE_REGEXP = "^[\\d]{1,2}(\\.[\\d]{1,3})?$";
    private static final String RECIPE_DISCOUNT_REGEXP = "^[\\d]{1,2}$";
    private static final String RECIPE_AMOUNT_REGEXP = "^[\\d]{1,2}(\\.[\\d]{1})?$";
    private static final String RECIPE_PREPARATION_NAME_REGEXP = "^[a-zA-Z0-9А-Яа-я\\s\\-]+$";

    private static final String PARAMETER_ID_RECIPE_NAME = "idRecipe";
    private static final String PARAMETER_AMOUNT_NAME = "amount";
    private static final String PARAMETER_CLIENT_LOGIN_NAME = "clientLogin";
    private static final String PARAMETER_DISCOUNT_NAME = "discount";
    private static final String PARAMETER_DOSAGE_NAME = "dosage";
    private static final String PARAMETER_FINISH_DATE_NAME = "finishDate";
    private static final String ATTRIBUTE_USER = "user";
    private static final String PARAMETER_NAME_PREPARATION = "namePreparation";

    private static final String ATTRIBUTE_SUCCESS_MESSAGE = "successMessage";
    private static final String ATTRIBUTE_ERROR_MESSAGE = "errorMessage";



    /**
     * Returns a string which contains relative path new preparation image page. Validates recipe
     * information which contains in HTTP request object ({@link HttpServletRequest}). If validation failed returns a
     * string which contains relative path to new recipe page file. Else the Recipe object filled and send to RecipeService.

     *
     * @param request the HTTP request object which contains information about order
     * @return a string which contains relative path  new preparation recipe page
     * @see Recipe
     * @see RecipeService
     */


    @Override
    public String execute(HttpServletRequest request) {
        Recipe recipe = new Recipe();
        User user = (User) request.getSession().getAttribute(ATTRIBUTE_USER);
        recipe.setDoctorLogin(user.getLogin());
        RecipeService service = new RecipeService();
        if (validate(request, recipe)) {
            try {
                service.registryNewRecipe(recipe);
                request.setAttribute(ATTRIBUTE_SUCCESS_MESSAGE, MessageManager.getProperty("message.success.add.recipe"));
                isRedirect = true;
            } catch (ServiceException e) {
                request.setAttribute(ATTRIBUTE_ERROR_MESSAGE, MessageManager.getProperty("message.error.add.recipe"));
                logger.error(e);
            }
        }else {
            logger.info("Parameters of recipe aren't validate");
        }
        return ConfigurationManager.getProperty("path.page.doctor.new.recipe");
    }

    @Override
    public boolean isRedirect() {
        return isRedirect;
    }

    private boolean validate(HttpServletRequest request, Recipe recipe) {
        boolean result = true;
        String param = request.getParameter(PARAMETER_ID_RECIPE_NAME);
        if (!param.isEmpty() && checkRegexp(param, RECIPE_ID_REGEXP)) {
            recipe.setId(Integer.valueOf(param));
        } else {
            result = false;
            request.setAttribute("invalid_id", MessageManager.getProperty("message.wrong.format.id.recipe", request.getLocale()));
        }

        param = request.getParameter(PARAMETER_AMOUNT_NAME);
        if (!param.isEmpty() && checkRegexp(param, RECIPE_AMOUNT_REGEXP)) {
            recipe.setAmount(Float.valueOf(param));
        } else {
            result = false;
            request.setAttribute("invalid_amount", MessageManager.getProperty("message.wrong.amount", request.getLocale()));
        }

        param = request.getParameter(PARAMETER_CLIENT_LOGIN_NAME);
        if (!param.isEmpty()) {
            recipe.setClientLogin(param);
        } else {
            result = false;
            request.setAttribute("invalid_client_login", MessageManager.getProperty("message.wrong.format.login", request.getLocale()));
        }

        param = request.getParameter(PARAMETER_DISCOUNT_NAME);
        if (!param.isEmpty() && checkRegexp(param, RECIPE_DISCOUNT_REGEXP)) {
            recipe.setDiscount(Float.valueOf(param));
        } else {
            result = false;
            request.setAttribute("invalid_discount", MessageManager.getProperty("message.wrong.discount", request.getLocale()));
        }

        param = request.getParameter(PARAMETER_DOSAGE_NAME);
        if (!param.isEmpty() && checkRegexp(param, RECIPE_DOSAGE_REGEXP)) {
            recipe.setDosage(Float.valueOf(param));
        } else {
            result = false;
            request.setAttribute("invalid_dosage", MessageManager.getProperty("message.wrong.fomat.dosage", request.getLocale()));
        }

        param = request.getParameter(PARAMETER_FINISH_DATE_NAME);
        if (!param.isEmpty()) {
            SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
            try {
                recipe.setFinishDate(new Date(format.parse(param).getTime()));
                Date today = new Date(Calendar.getInstance().getTime().getTime());
                if (!recipe.getFinishDate().after(today)) {
                    result = false;
                    request.setAttribute("invalid_finish_date", MessageManager.getProperty("message.wrong.finish.date", request.getLocale()));
                }
            } catch (ParseException e) {
                result = false;
                request.setAttribute("invalid_finish_date", MessageManager.getProperty("message.wrong.finish.date", request.getLocale()));

            }
        } else {
            result = false;
            request.setAttribute("invalid_finish_date", MessageManager.getProperty("message.wrong.finish.date", request.getLocale()));
        }

        param = request.getParameter(PARAMETER_NAME_PREPARATION);
        if (!param.isEmpty() && checkRegexp(param, RECIPE_PREPARATION_NAME_REGEXP)) {
            recipe.setNameOfPreparation(param);
        } else {
            result = false;
            request.setAttribute("invalid_name_preparation", MessageManager.getProperty("message.wrong.parametr.name", request.getLocale()));
        }
        return result;
    }


}
