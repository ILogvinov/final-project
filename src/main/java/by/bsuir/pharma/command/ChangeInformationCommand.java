package by.bsuir.pharma.command;

import by.bsuir.pharma.entity.User;
import by.bsuir.pharma.resource.ConfigurationManager;
import by.bsuir.pharma.resource.MessageManager;
import by.bsuir.pharma.service.ServiceException;
import by.bsuir.pharma.service.UpdateUserLogic;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

/**
 * Created by Иван on 25.04.2016.
 */
public class ChangeInformationCommand implements ActionCommand {
    private static final String PARAM_NAME_LOGIN = "login";
    private static final String PARAM_NAME_PASSWORD = "password";
    private static final String PARAM_EMAIL = "email";
    private static final String PARAM_PHONE_NUMBER = "phone_number";

    private static Logger logger = LogManager.getLogger(ChangeInformationCommand.class);

    @Override
    public String execute(HttpServletRequest request) {
        String page;

        User checkUser = new User();
        checkUser.setLogin(request.getParameter(PARAM_NAME_LOGIN));
        checkUser.setPassword(request.getParameter(PARAM_NAME_PASSWORD));
        checkUser.setPhoneNumber(request.getParameter(PARAM_PHONE_NUMBER));
        checkUser.setEmail(request.getParameter(PARAM_EMAIL));
        checkUser.setName(request.getParameter("name"));
        checkUser.setSurname(request.getParameter("surname"));


        try {
            if (UpdateUserLogic.changeUserData(checkUser)) {
                HttpSession session = request.getSession();
                session.setAttribute("user", checkUser);
                session.setAttribute("role", checkUser.getRole().getValue());
                page = ConfigurationManager.getProperty("path.page.main" + "_" + checkUser.getRole().toString().toLowerCase());
            } else {
                logger.warn("Invalidate password and login");
                request.setAttribute("errorLoginPassMessage", MessageManager.getProperty("message.loginerror", request.getLocale()));
                page = ConfigurationManager.getProperty("path.page.registrate");
            }

        } catch (ServiceException e) {
            logger.warn("Invalidate password and login", e);
            request.setAttribute("errorLoginPassMessage", MessageManager.getProperty("message.loginerror", request.getLocale()));
            page = ConfigurationManager.getProperty("path.page.registrate");
        }
        return page;
    }

    @Override
    public boolean isRedirect() {
        return false;
    }
}

