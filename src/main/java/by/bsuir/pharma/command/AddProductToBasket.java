package by.bsuir.pharma.command;

import by.bsuir.pharma.command.cookie.CartCookieCommand;
import by.bsuir.pharma.entity.Basket;
import by.bsuir.pharma.entity.Role;
import by.bsuir.pharma.entity.User;
import by.bsuir.pharma.resource.ConfigurationManager;
import by.bsuir.pharma.resource.MessageManager;
import by.bsuir.pharma.service.BasketService;
import by.bsuir.pharma.service.ServiceException;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;

/**
 * Created by Иван on 09.05.2016.
 */
public class AddProductToBasket extends CartCookieCommand {
    private final String COUNT_PRODUCT_CART_NAME = "countProductCart";
    private static final String PRODUCT_LOT_NUMBER_REGEXP = "^[\\d]{9}$";

    private static Logger logger = LogManager.getLogger(AddProductToBasket.class);

    private static final String PARAMETER_ID_PRODUCT = "id";
    private static final String ATTRIBUTE_ROLE = "role";
    private static final String ATTRIBUTE_USER = "user";

    private boolean isRedirect;


    /**
     * Returns a string which contains relative path catalog of preparation. Validates product
     * information which contains in HTTP request object ({@link HttpServletRequest}). If validation failed returns a
     * string which contains relative path failed page. Else the id product  filled and send to ProductService.

     *
     * @param request the HTTP request object which contains information about order
     * @return a string which contains relative path  new preparation recipe page
     * @see Basket
     * @see BasketService
     */
    @Override
    public String execute(HttpServletRequest request) {
        String page;
        Integer countProductInCart;
        Cookie[] cookies = request.getCookies();
        BasketService service = new BasketService();
        String idProduct = request.getParameter(PARAMETER_ID_PRODUCT);

        if (checkRegexp(idProduct, PRODUCT_LOT_NUMBER_REGEXP)) {
            try {
                switch ((Role) request.getSession().getAttribute(ATTRIBUTE_ROLE)) {
                    case GUEST:
                        cookie = service.addProductToBasket(cookies, Integer.valueOf(idProduct));
                        if (cookie != null) {
                            countProductInCart = (Integer) request.getSession().getAttribute(COUNT_PRODUCT_CART_NAME);
                            countProductInCart++;
                            request.getSession().setAttribute(COUNT_PRODUCT_CART_NAME, countProductInCart);
                        }
                        break;
                    case CLIENT:
                        User user = (User) request.getSession().getAttribute(ATTRIBUTE_USER);
                        String login = user.getLogin();
                        if (service.addProductToBasket(Integer.valueOf(idProduct), login)) {
                            Integer countProduct = (Integer) request.getSession().getAttribute(COUNT_PRODUCT_CART_NAME);
                            countProduct++;
                            request.getSession().setAttribute(COUNT_PRODUCT_CART_NAME, countProduct);
                        }
                        break;
                }
                page = request.getHeader("referer");
                isRedirect = true;
            } catch (ServiceException e) {
                request.setAttribute("errorMessage", MessageManager.getProperty("message.error.add.cart", request.getLocale()));
                page = ConfigurationManager.getProperty("path.page.fail.result");
            }
        } else {
            page = ConfigurationManager.getProperty("path.page.fail.result");
            request.setAttribute("errorMessage", MessageManager.getProperty("message.error.add.cart", request.getLocale()));
            logger.info("Lot number isn't valid");
        }

        return page;
    }

    @Override
    public boolean isRedirect() {
        return isRedirect;
    }


}
