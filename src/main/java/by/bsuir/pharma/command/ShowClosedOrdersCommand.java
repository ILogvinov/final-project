package by.bsuir.pharma.command;

import by.bsuir.pharma.entity.Order;
import by.bsuir.pharma.resource.ConfigurationManager;
import by.bsuir.pharma.resource.MessageManager;
import by.bsuir.pharma.service.OrderService;
import by.bsuir.pharma.service.ServiceException;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

/**
 * Created by Иван on 27.06.2016.
 */
public class ShowClosedOrdersCommand implements ActionCommand {
    private static final String ORDER_STATUS_DELIVERED = "DELIVERED";
    private static final String ATTRIBUTE_ORDERS = "orders";
    private static final String ATTRIBUTE_ERROR_MESSAGE = "errorMessage";

    @Override
    public String execute(HttpServletRequest request) {
        String page;
        try {
            List<Order> orders = OrderService.findOrderByStatus(ORDER_STATUS_DELIVERED);
            request.setAttribute(ATTRIBUTE_ORDERS, orders);
            page = ConfigurationManager.getProperty("path.page.pharmacist.closed.orders");
        } catch (ServiceException e) {
            request.setAttribute(ATTRIBUTE_ERROR_MESSAGE, MessageManager.getProperty("message.error.show.order", request.getLocale()));
            page = ConfigurationManager.getProperty("path.page.fail.result");
        }
        return page;
    }

    @Override
    public boolean isRedirect() {
        return false;
    }
}
