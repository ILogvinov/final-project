package by.bsuir.pharma.command;

import by.bsuir.pharma.entity.Preparation;
import by.bsuir.pharma.resource.ConfigurationManager;
import by.bsuir.pharma.resource.MessageManager;
import by.bsuir.pharma.service.ProductService;
import by.bsuir.pharma.service.ServiceException;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

/**
 * Created by Иван on 20.06.2016.
 */
public class ShowListProductCommand implements ActionCommand {
    private static final String ATTRIBUTE_ERROR_MESSAGE = "errorMessage";
    private static final String ATTRIBUTE_PREPARATIONS = "preparations";
    @Override
    public String execute(HttpServletRequest request) {
        String page;

        try {
            List<Preparation> preparations = ProductService.giveCatalogOfPreparation();
            request.setAttribute(ATTRIBUTE_PREPARATIONS, preparations);
            page = ConfigurationManager.getProperty("path.page.list.product");
        } catch (ServiceException e) {
            request.setAttribute(ATTRIBUTE_ERROR_MESSAGE, MessageManager.getProperty("message.error.show.list.product", request.getLocale()));
            page = ConfigurationManager.getProperty("path.page.fail.result");
        }
        return page;
    }

    @Override
    public boolean isRedirect() {
        return false;
    }
}
