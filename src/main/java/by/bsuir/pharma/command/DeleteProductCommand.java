package by.bsuir.pharma.command;


import by.bsuir.pharma.resource.ConfigurationManager;
import by.bsuir.pharma.resource.MessageManager;
import by.bsuir.pharma.service.ProductService;
import by.bsuir.pharma.service.ServiceException;

import javax.servlet.http.HttpServletRequest;

/**
 * Created by Иван on 15.05.2016.
 */
public class DeleteProductCommand implements ActionCommand {
    private boolean isRedirect;
    private static final String PRODUCT_LOT_NUMBER_REGEXP = "^[\\d]{9}$";
    private static final String PARAM_LOT_NUMBER = "idProduct";

    @Override
    public String execute(HttpServletRequest request) {
        String page;
        String param = request.getParameter(PARAM_LOT_NUMBER);
        if (!param.isEmpty() && checkRegexp(param, PRODUCT_LOT_NUMBER_REGEXP)) {
            Integer idProduct = Integer.valueOf(param);
            try {
                ProductService.removeProduct(idProduct);
                page = request.getHeader("referer");
                isRedirect = true;
            } catch (ServiceException e) {
                request.setAttribute("errorMessage", MessageManager.getProperty("message.error.delete.product", request.getLocale()));
                page = request.getHeader("referer");
            }
        } else {
            request.setAttribute("errorMessage", MessageManager.getProperty(" message.invalidate.data.product.delete", request.getLocale()));
            page = ConfigurationManager.getProperty("path.page.fail.result");
        }
        return page;
    }

    @Override
    public boolean isRedirect() {
        return isRedirect;
    }
}
