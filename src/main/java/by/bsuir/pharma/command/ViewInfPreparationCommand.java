package by.bsuir.pharma.command;

import by.bsuir.pharma.entity.Preparation;
import by.bsuir.pharma.resource.ConfigurationManager;
import by.bsuir.pharma.resource.MessageManager;
import by.bsuir.pharma.service.PreparationService;
import by.bsuir.pharma.service.ServiceException;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.servlet.http.HttpServletRequest;

/**
 * Created by Иван on 04.05.2016.
 */
public class ViewInfPreparationCommand implements ActionCommand {
    private boolean isRedirect;
    private static final String ID_PREPARATION_REGEXP = "^[\\w\\d]{10}$";
    private static final String ATTRIBUTE_ERROR_MESSAGE = "errorMessage";
    private static final String ATTRIBUTE_PREPARATION = "preparation";
    private static Logger logger = LogManager.getLogger(ViewInfPreparationCommand.class);

    @Override
    public String execute(HttpServletRequest request) {
        String page =null;
        String idPreparation = request.getParameter("id_preparation");
        try {
            if(!idPreparation.isEmpty()&&checkRegexp(idPreparation,ID_PREPARATION_REGEXP)){
                PreparationService service = new PreparationService();
                Preparation preparation = service.findById(idPreparation);
                request.setAttribute(ATTRIBUTE_PREPARATION,preparation);
                page = ConfigurationManager.getProperty("path.page.detail.preparation");
            }else {
                logger.warn("Invalidate format of id preparation");
                page = ConfigurationManager.getProperty("path.link.show.catalog");
                isRedirect =true;
            }

        } catch (ServiceException e) {
            request.setAttribute(ATTRIBUTE_ERROR_MESSAGE, MessageManager.getProperty("message.preparation.inf.not.found",request.getLocale()));
            page = ConfigurationManager.getProperty("path.page.fail.result");
        }
        return page;
    }

    @Override
    public boolean isRedirect() {
        return isRedirect;
    }


}
