package by.bsuir.pharma.command;

import by.bsuir.pharma.entity.Role;
import by.bsuir.pharma.entity.User;
import by.bsuir.pharma.resource.ConfigurationManager;
import by.bsuir.pharma.resource.MessageManager;
import by.bsuir.pharma.service.RegistrateService;
import by.bsuir.pharma.service.ServiceException;

import javax.servlet.http.HttpServletRequest;
import java.util.Locale;

/**
 * Created by Иван on 13.05.2016.
 */
public class RegistryWorkerCommand implements ActionCommand {
    private boolean isRedirect;
    private static final String PARAM_NAME_LOGIN = "login";
    private static final String PARAM_NAME_PASSWORD = "password";
    private static final String PARAM_EMAIL = "email";
    private static final String PARAM_PHONE_NUMBER = "phone_number";
    private static final String PARAM_NAME = "name";
    private static final String PARAM_SURNAME = "surname";
    private static final String PARAM_ROLE = "role";

    private static final String ATTRIBUTE_ROLE = "role";
    private static final String ATTRIBUTE_ERROR_MESSAGE = "errorMessage";

    private static final String NAME_REGEXP = "^[A-Za-zА-Яа-я]{2,15}$";
    private static final String SURNAME_REGEXP = "^[A-Za-zА-Яа-я\\-]{2,25}$";
    private static final String PASSWORD_REGEXP = "^[\\w\\d]{7,20}";
    private static final String ROLE_REGEXP = "^DOCTOR|PHARMACIST$";
    private static final String LOGIN_REGEXP = "^[\\w\\d\\_]{7,20}$";
    private static final String EMAIL_REGEXP = "^[A-Za-z0-9._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,6}$";
    private static final String PHONE_REGEXP = "\\+?[\\d]{10,14}";



    @Override
    public String execute(HttpServletRequest request) {
        User checkUser = new User();
        String page;
        Locale locale = (Locale) request.getSession().getAttribute("Locale");

        if(validate(request,checkUser)){
            try {
                if (RegistrateService.registreNewUser(checkUser)) {
                    page = ConfigurationManager.getProperty("path.page.new.worker");
                    isRedirect = true;
                } else {

                    request.setAttribute(ATTRIBUTE_ERROR_MESSAGE, MessageManager.getProperty("message.loginerror", locale));
                    page = ConfigurationManager.getProperty("path.page.new.worker");
                }

            } catch (ServiceException e) {
                request.setAttribute(ATTRIBUTE_ERROR_MESSAGE, MessageManager.getProperty("message.loginerror", locale));
                page = ConfigurationManager.getProperty("path.page.new.worker");
            }
        }else {
            page = ConfigurationManager.getProperty("path.page.new.worker");
        }

        return page;
    }

    @Override
    public boolean isRedirect() {
        return isRedirect;
    }

    private boolean validate(HttpServletRequest request, User user){
        boolean result = true;
        Locale locale = (Locale) request.getSession().getAttribute("Locale");
        String param = request.getParameter(PARAM_NAME_LOGIN);
        if(!param.isEmpty()&&checkRegexp(param,LOGIN_REGEXP)){
            user.setLogin(param);
        }else {
            request.setAttribute("invalid_login",MessageManager.getProperty("message.wrong.format.login",locale));
            result = false;
        }

        param = request.getParameter(PARAM_NAME_PASSWORD);
        if(!param.isEmpty()&&checkRegexp(param,PASSWORD_REGEXP)){
            user.setPassword(param);
        }else {
            request.setAttribute("invalid_password",MessageManager.getProperty("message.wrong.format.password",locale));
            result = false;
        }

        param = request.getParameter(PARAM_PHONE_NUMBER);
        if(!param.isEmpty()&&checkRegexp(param,PHONE_REGEXP)){
            user.setPhoneNumber(param);
        }else {
            request.setAttribute("invalid_phone",MessageManager.getProperty("message.wrong.format.phone",locale));
            result = false;
        }

        param = request.getParameter(PARAM_EMAIL);
        if(!param.isEmpty()&&checkRegexp(param,EMAIL_REGEXP)){
            user.setEmail(param);
        }else {
            request.setAttribute("invalid_email",MessageManager.getProperty("message.wrong.format.email",locale));
            result = false;
        }

        param = request.getParameter(PARAM_NAME);
        if(!param.isEmpty()&&checkRegexp(param,NAME_REGEXP)){
            user.setName(param);
        }else {
            request.setAttribute("invalid_name",MessageManager.getProperty("message.wrong.format.name",locale));
            result = false;
        }

        param = request.getParameter(PARAM_SURNAME);
        if(!param.isEmpty()&&checkRegexp(param,SURNAME_REGEXP)){
            user.setSurname(param);
        }else {
            request.setAttribute("invalid_surname",MessageManager.getProperty("message.wrong.format.surname",locale));
            result = false;
        }

        param = request.getParameter(PARAM_ROLE);
        if(!param.isEmpty()&&checkRegexp(param,ROLE_REGEXP)){
            user.setRole(Role.valueOf(param));
        }else {
            request.setAttribute("invalid_role",MessageManager.getProperty("message.wrong.role",locale));
            result = false;
        }
        return result;
    }
}

