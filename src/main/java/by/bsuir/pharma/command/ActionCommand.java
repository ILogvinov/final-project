package by.bsuir.pharma.command;

import javax.servlet.http.HttpServletRequest;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by Иван on 27.03.2016.
 */
public interface ActionCommand {
    /**
     * Executes specified command.
     *
     * @param request the {@link HttpServletRequest} object
     * @return a string which contains page path
     */
    String  execute(HttpServletRequest request);

    boolean isRedirect();

    /**
     * Check parameter of HttpServletRequest
     *
     * @param param a string which contain parameter of HttpServletRequest
     * @param regex a string which contain regular expression
     * @return result, the validity of the parameter or not
     */
    default  boolean checkRegexp(String param, String regex) {
        Pattern pattern = Pattern.compile(regex);
        Matcher matcher = pattern.matcher(param);
        return matcher.matches();
    }
}
