package by.bsuir.pharma.command;

import by.bsuir.pharma.resource.ConfigurationManager;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.util.Locale;

/**
 * Created by Иван on 30.03.2016.
 */
public class LocaleCommand implements ActionCommand {
    @Override
    public String execute(HttpServletRequest request) {
        String page;
        String language = request.getParameter("language");
        HttpSession session = request.getSession(true);
        if (language.equals("English")) {
            session.setAttribute("locale", "en_US");
            session.setAttribute("Locale", new Locale("en", "US"));
        } else {
            session.setAttribute("locale", "ru_RU");
            session.setAttribute("Locale", new Locale("ru", "RU"));


        }


        page = ConfigurationManager.getProperty("path.page.start");
        return page;
    }

    @Override
    public boolean isRedirect() {
        return false;
    }
}
