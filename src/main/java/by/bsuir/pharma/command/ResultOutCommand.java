package by.bsuir.pharma.command;

import by.bsuir.pharma.resource.ConfigurationManager;

import javax.servlet.http.HttpServletRequest;

/**
 * Created by Иван on 10.04.2016.
 */
public class ResultOutCommand implements ActionCommand {
    @Override
    public String execute(HttpServletRequest request) {
        return ConfigurationManager.getProperty("path.page.main");
    }

    @Override
    public boolean isRedirect() {
        return false;
    }
}
