package by.bsuir.pharma.command;

import by.bsuir.pharma.resource.ConfigurationManager;
import by.bsuir.pharma.service.RequestToRecipeService;
import by.bsuir.pharma.service.ServiceException;

import javax.servlet.http.HttpServletRequest;

/**
 * Created by Иван on 04.06.2016.
 */
public class RegisterRequestToRecipe implements ActionCommand  {
    private boolean isRedirect;

    private static final String PARAM_ID_RECIPE = "idRecipe";
    @Override
    public String execute(HttpServletRequest request) {
        String page;
        Integer idRecipe = Integer.valueOf(request.getParameter(PARAM_ID_RECIPE));

        RequestToRecipeService requestToRecipeService = new RequestToRecipeService();
        try {
            if(requestToRecipeService.registryNewRequest(idRecipe)){
                isRedirect = true;
                page = request.getHeader("referer");
            }else {
                page = ConfigurationManager.getProperty("path.page.registrate");
            }
        } catch (ServiceException e) {
            page = ConfigurationManager.getProperty("path.page.registrate");
        }
        return page;
    }

    @Override
    public boolean isRedirect() {
        return isRedirect;
    }
}
