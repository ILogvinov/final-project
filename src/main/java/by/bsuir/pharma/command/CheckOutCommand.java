package by.bsuir.pharma.command;

import by.bsuir.pharma.entity.Preparation;
import by.bsuir.pharma.entity.Recipe;
import by.bsuir.pharma.entity.Role;
import by.bsuir.pharma.entity.User;
import by.bsuir.pharma.resource.ConfigurationManager;
import by.bsuir.pharma.resource.MessageManager;
import by.bsuir.pharma.service.BasketService;
import by.bsuir.pharma.service.RecipeService;
import by.bsuir.pharma.service.ServiceException;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

/**
 * Created by Иван on 13.05.2016.
 */
public class CheckOutCommand implements ActionCommand {
    private boolean isRedirect;

    @Override
    public String execute(HttpServletRequest request) {
        String page = null;
        List<Preparation> preparations = null;
        try {
            switch ((Role) request.getSession().getAttribute("role")) {
                case GUEST:
                    request.setAttribute("errorMessage", MessageManager.getProperty("message.require.autorization",request.getLocale()));
                    page = ConfigurationManager.getProperty("path.page.cart");
                    break;
                case CLIENT:
                    User user = (User) request.getSession().getAttribute("user");
                    String login = user.getLogin();
                    if (BasketService.findPreparationWithoutRecipe(login) == 0) {
                        preparations = BasketService.findProductsOfClient(login);
                        if (preparations.size() > 0) {
                            Float totalValue = 0f;
                            for (Preparation preparation : preparations) {
                                totalValue += preparation.getPrice() * preparation.getCount()*((100-preparation.getDiscount())/100);
                            }
                            request.setAttribute("preparations", preparations);
                            request.setAttribute("totalValue", totalValue);
                            page = ConfigurationManager.getProperty("path.page.checkout");
                        } else {

                            request.setAttribute("errorMessage", MessageManager.getProperty("message.error.empty.cart",request.getLocale()));
                            page = ConfigurationManager.getProperty("path.page.cart");
                            isRedirect = false;
                        }

                    }else {
                        preparations = BasketService.findProductsOfClient(login);
                        List<Recipe> recipes = RecipeService.findRecipeClient(login);
                        request.setAttribute("recipes",recipes);
                        request.setAttribute("preparations",preparations);
                        request.setAttribute("errorMessage", MessageManager.getProperty("message.preparation.without.recipe",request.getLocale()));
                        page = ConfigurationManager.getProperty("path.page.cart");
                    }

                    break;
            }
        } catch (ServiceException e) {
            request.setAttribute("errorMessage", MessageManager.getProperty("message.error.checkout", request.getLocale()));
            page = ConfigurationManager.getProperty("path.page.fail.result");
        }
        return page;
    }

    @Override
    public boolean isRedirect() {
        return isRedirect;
    }
}
