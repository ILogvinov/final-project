package by.bsuir.pharma.command;

import by.bsuir.pharma.resource.MessageManager;
import by.bsuir.pharma.service.ProductService;
import by.bsuir.pharma.service.ServiceException;

import javax.servlet.http.HttpServletRequest;

/**
 * Created by Иван on 15.05.2016.
 */
public class ChangeCountPreparationCommand implements ActionCommand {
    private boolean isRedirect;

    private static final String PARAM_ID_PRODUCT = "idProduct";
    private static final String PARAM_QUANTITY = "quantity";

    private static final String PRODUCT_LOT_NUMBER_REGEXP = "^[\\d]{1,9}$";
    private static final String PRODUCT_COUNT_REGEXP = "^[\\d]{1,3}$";



    @Override
    public String execute(HttpServletRequest request) {
        String page;
        String paramIdProduct = request.getParameter(PARAM_ID_PRODUCT);
        String paramQuantity = request.getParameter(PARAM_QUANTITY);

        if(validate(paramIdProduct,paramQuantity)){
            try {
                Integer idProduct = Integer.valueOf(paramIdProduct);
                Float count = Float.valueOf(paramQuantity);
                ProductService.updateCountPreparation(idProduct, count);
                page = request.getHeader("referer");
                isRedirect = true;
            } catch (ServiceException e) {
                request.setAttribute("errorMessage", MessageManager.getProperty("message.error.update.product", request.getLocale()));
                page = request.getHeader("referer");
            }
        }else {
            request.setAttribute("errorMessage", MessageManager.getProperty("message.error.update.product", request.getLocale()));
            page = request.getHeader("referer");
            isRedirect = true;
        }

        return page;
    }

    @Override
    public boolean isRedirect() {
        return isRedirect;
    }

    private boolean validate(String paramIdProduct,String paramQuantity) {
        return !(paramIdProduct.isEmpty() && paramIdProduct.isEmpty()) && checkRegexp(paramIdProduct, PRODUCT_LOT_NUMBER_REGEXP) && checkRegexp(paramQuantity, PRODUCT_COUNT_REGEXP);
    }
}
