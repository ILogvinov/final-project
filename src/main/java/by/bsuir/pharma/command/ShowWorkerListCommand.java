package by.bsuir.pharma.command;

import by.bsuir.pharma.entity.User;
import by.bsuir.pharma.resource.ConfigurationManager;
import by.bsuir.pharma.resource.MessageManager;
import by.bsuir.pharma.service.ServiceException;
import by.bsuir.pharma.service.WorkerService;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

/**
 * Created by Иван on 21.06.2016.
 */
public class ShowWorkerListCommand implements ActionCommand {
    private static final String ATTRIBUTE_WORKERS = "workers";
    private static final String ATTRIBUTE_ERROR_MESSAGE = "errorMessage";
    @Override
    public String execute(HttpServletRequest request) {
        String page;
        try {
            List<User> workers = WorkerService.findWorkers();
            request.setAttribute(ATTRIBUTE_WORKERS, workers);
            page = ConfigurationManager.getProperty("path.page.list.workers");
        } catch (ServiceException e) {
            request.setAttribute(ATTRIBUTE_ERROR_MESSAGE, MessageManager.getProperty("message.error.show.workers", request.getLocale()));
            page = ConfigurationManager.getProperty("path.page.fail.result");
        }
        return page;
    }

    @Override
    public boolean isRedirect() {
        return false;
    }
}
