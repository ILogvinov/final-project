package by.bsuir.pharma.command;

import by.bsuir.pharma.entity.Product;
import by.bsuir.pharma.resource.ConfigurationManager;
import by.bsuir.pharma.resource.MessageManager;
import by.bsuir.pharma.service.ProductService;
import by.bsuir.pharma.service.ServiceException;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.servlet.http.HttpServletRequest;
import java.sql.Date;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;

/**
 * Created by Иван on 13.05.2016.
 */
public class AddNewProductCommand implements ActionCommand {

    private static final String ID_PREPARATION_REGEXP = "^[\\w\\d]{9,10}$";
    private static final String PRODUCT_LOT_NUMBER_REGEXP = "^[\\d]{9}$";
    private static final String PRODUCT_PRICE_REGEXP = "^[\\d]{1,2}(\\.[\\d]{1,3})?$";
    private static final String PRODUCT_COUNT_REGEXP = "^[\\d]{1,3}(\\.[\\d]{1})?$";
    private static final String PRODUCT_FINISH_DATE = "^(?:(?:31(\\/|-|\\.)(?:0?[13578]|1[02]))\\1|(?:(?:29|30)(\\/|-|\\.)(?:0?[1,3-9]|1[0-2])\\2))(?:(?:1[6-9]|[2-9]\\d)?\\d{2})$|^(?:29(\\/|-|\\.)0?2\\3(?:(?:(?:1[6-9]|[2-9]\\d)?(?:0[48]|[2468][048]|[13579][26])|(?:(?:16|[2468][048]|[3579][26])00))))$|^(?:0?[1-9]|1\\d|2[0-8])(\\/|-|\\.)(?:(?:0?[1-9])|(?:1[0-2]))\\4(?:(?:1[6-9]|[2-9]\\d)?\\d{2})$";

    private static final String PARAMETER_NAME_LOT = "lotNumber";
    private static final String PARAMETER_PREPARATION_ID = "id";
    private static final String PARAMETER_NAME_PRICE = "price";
    private static final String PARAMETER_NAME_COUNT = "count";
    private static final String PARAMETER_NAME_FINISH_DATE = "endShelfLife";

    private static final String ATTRIBUTE_ERROR_MESSAGE = "errorMessage";

    private boolean isRedirect;
    private static Logger logger = LogManager.getLogger(AddNewProductCommand.class);

    /**
     * Returns a string which contains relative path new preparation image page. Validates preparation
     * information which contains in HTTP request object ({@link HttpServletRequest}). If validation failed returns a
     * string which contains relative path to new preparation page file. Else the Preparation object filled and send to PreparationService.
     * If save image failed created message page with
     * message about fail.
     *
     * @param request the HTTP request object which contains information about order
     * @return a string which contains relative path  new preparation image page
     * @see Product
     * @see ProductService
     */
    @Override
    public String execute(HttpServletRequest request) {
        String page;
        Product product = new Product();
        try {
            if (validate(request, product)) {
                ProductService.addNewProduct(product);
                page = ConfigurationManager.getProperty("path.page.pharmacist.new.product");
                isRedirect = true;
            } else {
                page = ConfigurationManager.getProperty("path.page.pharmacist.new.product");
                logger.warn("Invalidate parameters of new preparation");
            }

        } catch (ServiceException e) {
            page = ConfigurationManager.getProperty("path.page.pharmacist.fail");
            request.setAttribute(ATTRIBUTE_ERROR_MESSAGE, MessageManager.getProperty("message.fail.add.product", request.getLocale()));
            logger.error("Error of adding new product", e);
        }
        return page;
    }

    @Override
    public boolean isRedirect() {
        return isRedirect;
    }


    private boolean validate(HttpServletRequest request, Product product) {
        boolean result = true;
        String param = request.getParameter(PARAMETER_NAME_LOT);
        if (!param.isEmpty() && checkRegexp(param, PRODUCT_LOT_NUMBER_REGEXP)) {
            product.setLotNumber(Integer.valueOf(param));
        } else {
            result = false;
            request.setAttribute("invalid_lot", MessageManager.getProperty("message.wrong.format.lot", request.getLocale()));
        }

        param = request.getParameter(PARAMETER_PREPARATION_ID);
        if (!param.isEmpty() && checkRegexp(param, ID_PREPARATION_REGEXP)) {
            product.setPreparationId(param);
        } else {
            result = false;
            request.setAttribute("invalid_id", MessageManager.getProperty("message.wrong.preparation.id", request.getLocale()));
        }

        param = request.getParameter(PARAMETER_NAME_PRICE);
        if (!param.isEmpty() && checkRegexp(param, PRODUCT_PRICE_REGEXP)) {
            product.setPrice(Float.valueOf(param));
        } else {
            result = false;
            request.setAttribute("invalid_price", MessageManager.getProperty("message.wrong.format.price", request.getLocale()));
        }

        param = request.getParameter(PARAMETER_NAME_COUNT);
        if (!param.isEmpty() && checkRegexp(param, PRODUCT_COUNT_REGEXP)) {
            product.setCountOnSparePart(Integer.valueOf(param));
        } else {
            result = false;
            request.setAttribute("invalid_count", MessageManager.getProperty("message.wrong.format.count", request.getLocale()));
        }

        param = request.getParameter(PARAMETER_NAME_FINISH_DATE);
        if (!param.isEmpty()) {
            try {
                SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
                product.setEndShelfLife(new Date(format.parse(param).getTime()));
                Date today = new Date(Calendar.getInstance().getTime().getTime());
                if (!product.getEndShelfLife().after(today)) {
                    result = false;
                    request.setAttribute("invalid_finish_date", MessageManager.getProperty("message.wrong.finish.date", request.getLocale()));
                }
            } catch (ParseException e) {
                result = false;
                request.setAttribute("invalid_finish_date", MessageManager.getProperty("message.wrong.finish.date", request.getLocale()));
            }
        } else {
            result = false;
            request.setAttribute("invalid_finish_date", MessageManager.getProperty("message.wrong.finish.date", request.getLocale()));
        }
        return result;
    }


}
