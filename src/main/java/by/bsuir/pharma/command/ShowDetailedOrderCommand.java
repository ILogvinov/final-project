package by.bsuir.pharma.command;

import by.bsuir.pharma.entity.Preparation;
import by.bsuir.pharma.resource.ConfigurationManager;
import by.bsuir.pharma.resource.MessageManager;
import by.bsuir.pharma.service.BasketService;
import by.bsuir.pharma.service.ServiceException;

import javax.servlet.http.HttpServletRequest;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Иван on 13.05.2016.
 */
public class ShowDetailedOrderCommand implements ActionCommand {
    private final static String PARAMETER_ID_ORDER = "idOrder";

    private final static String ATTRIBUTE_PREPARATIONS = "preparations";
    private final static String ATTRIBUTE_ERROR_MESSAGE = "errorMessage";
    private final static String ATTRIBUTE_ID_ORDER = "idOrder";

    @Override
    public String execute(HttpServletRequest request) {
        String page;
        List<Preparation> preparations;
        Integer idOrder = Integer.valueOf(request.getParameter(PARAMETER_ID_ORDER));
        try {
            preparations = BasketService.findProductsOfClient(idOrder);
            request.setAttribute(ATTRIBUTE_PREPARATIONS,preparations);
            request.setAttribute(ATTRIBUTE_ID_ORDER,idOrder);
            page = ConfigurationManager.getProperty("path.page.detailed.order");
        } catch (ServiceException e) {
            request.setAttribute(ATTRIBUTE_ERROR_MESSAGE, MessageManager.getProperty("message.error.show.detail.order", request.getLocale()));
            page = ConfigurationManager.getProperty("path.page.fail.result");
        }
        return page;
    }

    @Override
    public boolean isRedirect() {
        return false;
    }
}

