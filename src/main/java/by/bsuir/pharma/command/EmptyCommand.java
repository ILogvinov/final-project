package by.bsuir.pharma.command;

import by.bsuir.pharma.resource.ConfigurationManager;

import javax.servlet.http.HttpServletRequest;

/**
 * Created by Иван on 27.03.2016.
 */
public class EmptyCommand implements ActionCommand {
    @Override
    public String execute(HttpServletRequest request) {
        return ConfigurationManager.getProperty("path.page.login");
    }

    @Override
    public boolean isRedirect() {
        return false;
    }
}
