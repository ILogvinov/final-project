package by.bsuir.pharma.command;

import by.bsuir.pharma.entity.Preparation;
import by.bsuir.pharma.entity.Recipe;
import by.bsuir.pharma.entity.Role;
import by.bsuir.pharma.entity.User;
import by.bsuir.pharma.resource.ConfigurationManager;
import by.bsuir.pharma.resource.MessageManager;
import by.bsuir.pharma.service.BasketService;
import by.bsuir.pharma.service.RecipeService;
import by.bsuir.pharma.service.ServiceException;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

/**
 * Created by Иван on 12.05.2016.
 */
public class ShowCartCommand implements ActionCommand {
    private final static String ATTRIBUTE_PREPARATIONS = "preparations";
    private static final String ATTRIBUTE_ERROR_MESSAGE = "errorMessage";
    private final static String ATTRIBUTE_RECIPES = "recipes";

    @Override
    public String execute(HttpServletRequest request) {
        String page;
        BasketService service = new BasketService();
        List<Preparation> preparations = null;
        try {
            switch ( (Role)request.getSession().getAttribute("role")) {
                case GUEST:
                    preparations = BasketService.findProductsOfGuest(request.getCookies());
                    break;
                case CLIENT:
                    User user = (User)request.getSession().getAttribute("user");
                    String login = user.getLogin();
                    preparations = BasketService.findProductsOfClient(login);
                    List<Recipe> recipes = RecipeService.findRecipeClient(login);
                    request.setAttribute(ATTRIBUTE_RECIPES,recipes);
                    break;
            }
            request.setAttribute(ATTRIBUTE_PREPARATIONS,preparations);
            page = ConfigurationManager.getProperty("path.page.cart");
        } catch (ServiceException e) {
            request.setAttribute(ATTRIBUTE_ERROR_MESSAGE, MessageManager.getProperty("message.error.show.cart", request.getLocale()));
            page = ConfigurationManager.getProperty("path.page.fail.result");
        }
        return page;
    }

    @Override
    public boolean isRedirect() {
        return false;
    }
}
